#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

class DynamicLibraryPrivate
{
public:
	DynamicLibraryPrivate( fs::path const& file_path )
	    : mPath( file_path )
	{
	}

	~DynamicLibraryPrivate() { close(); }

	fs::path get_path() const { return mPath; }

	bool open()
	{
		A_CLASS_CALL1( mPath.u8string() );

		MOJO_ASSERT( !mDynamicLibrary );

		mDynamicLibrary = std::make_unique<juce::DynamicLibrary>();

		const juce::String path( juce::CharPointer_UTF8( mPath.u8string().c_str() ) );
		const auto open = mDynamicLibrary->open( path );

		if( !open )
		{
			mDynamicLibrary.reset();
		}
		return is_open();
	}

	bool is_open()
	{
		A_CLASS_CALL1( mPath.u8string() );

		return mDynamicLibrary != nullptr;
	}

	bool close()
	{
		A_CLASS_CALL1( mPath.u8string() );

		if( mDynamicLibrary )
		{
			mDynamicLibrary->close();
			mDynamicLibrary.reset();
		}
		return true;
	}

	DynamicLibrary::FunctionPointer resolve( const std::string& symbol_name )
	{
		A_CLASS_CALL1( symbol_name );

		if( !mDynamicLibrary )
		{
			MOJO_ASSERT(
			    "Dynamic library must be open before a symbol can be resolved" );
			return nullptr;
		}

		DynamicLibrary::FunctionPointer function =
		    static_cast<DynamicLibrary::FunctionPointer>(
		        mDynamicLibrary->getFunction( symbol_name ) );

		return function;
	}

	static bool is_library( fs::path const& path )
	{
		const auto extension = path.extension().u8string();

		for( const auto& dynamic_library_extension : GetExtensions() )
		{
			if( extension == dynamic_library_extension )
			{
				return true;
			}
		}
		return false;
	}

	static std::vector<std::string> GetExtensions()
	{
#if MOJO_WINDOWS
		return { ".dll", ".DLL" };
#elif MOJO_MAC
		return { ".dylib", ".bundle", ".so" };
#else
		return { ".so" };
#endif
	}

private:
	fs::path mPath;
	std::unique_ptr<juce::DynamicLibrary> mDynamicLibrary;

private:
	A_DECLARE_CLASS_MEMBERS( DynamicLibraryPrivate )
};

A_DEFINE_CLASS_AS_MEMBERS( DynamicLibraryPrivate, "mojo::DynamicLibrary" )

DynamicLibrary::DynamicLibrary( fs::path const& path )
    : d_ptr( new DynamicLibraryPrivate( path ) )
{
}

DynamicLibrary::~DynamicLibrary() {}

fs::path
DynamicLibrary::get_path() const
{
	return d_ptr->get_path();
}

bool
DynamicLibrary::open()
{
	return d_ptr->open();
}

bool
DynamicLibrary::is_open()
{
	return d_ptr->is_open();
}

bool
DynamicLibrary::close()
{
	return d_ptr->close();
}

DynamicLibrary::FunctionPointer
DynamicLibrary::resolve( std::string const& symbol_name )
{
	return d_ptr->resolve( symbol_name );
}

bool
DynamicLibrary::is_library( fs::path const& path )
{
	return DynamicLibraryPrivate::is_library( path );
}

MOJO_END_NAMESPACE
