#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class TypeID final
{
public:
	TypeID( const char* const ) noexcept;

	TypeID( const std::string& str ) noexcept;

	TypeID( const TypeID& other ) noexcept = default;

	inline bool operator==( const TypeID& other ) const
	{
		return m_name.data() == other.m_name.data();
	}

	inline bool operator!=( const TypeID& other ) const
	{
		return m_name.data() != other.m_name.data();
	}

	inline bool operator<( const TypeID& other ) const
	{
		return m_name.data() < other.m_name.data();
	}

	std::string_view get_str() { return m_name; }

private:
	std::string_view m_name;
};

MOJO_END_NAMESPACE
