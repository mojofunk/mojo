#include "mojo_test_includes.hpp"

TEST( DynamicLibrary, i_can_check_if_a_file_name_is_a_dynamic_library )
{
#if MOJO_WINDOWS
	ASSERT_TRUE( DynamicLibrary::is_library( "module.dll" ) );
	ASSERT_TRUE( DynamicLibrary::is_library( "module.DLL" ) );
#elif MOJO_MAC
	ASSERT_TRUE( DynamicLibrary::is_library( "module.dylib" ) );
	ASSERT_TRUE( DynamicLibrary::is_library( "module.bundle" ) );
	ASSERT_TRUE( DynamicLibrary::is_library( "module.so" ) );
#else
	ASSERT_TRUE( DynamicLibrary::is_library( "module.so" ) );
#endif
}

TEST( DynamicLibrary, i_can_check_if_a_file_path_is_a_dynamic_library )
{
#if MOJO_WINDOWS
	auto path = find_path( { "C:/Windows/System32" }, "winmm.dll" );

	ASSERT_TRUE( !path.empty() );

	ASSERT_TRUE( DynamicLibrary::is_library( path ) );
#elif MOJO_MAC
	// TODO
	ASSERT_TRUE( false );
#else
	auto path = find_path( { "/usr/lib", "/usr/lib64" }, "libdbus-glib-1.so" );

	ASSERT_TRUE( !path.empty() );

	ASSERT_TRUE( DynamicLibrary::is_library( path ) );
#endif
}

TEST( DynamicLibrary, i_can_open_a_dynamic_library_using_a_name )
{
#if MOJO_WINDOWS
	const char* library_name = "wininet";
#elif MOJO_MAC
	// TODO
	ASSERT_TRUE( false );
#else
	const char* library_name = "glib-2.0";
#endif

	// open using generic library name
	DynamicLibrary lib( library_name );

	ASSERT_FALSE( lib.is_open() );

	ASSERT_TRUE( lib.open() );

	ASSERT_TRUE( lib.is_open() );

	ASSERT_TRUE( lib.close() );

	ASSERT_FALSE( lib.is_open() );

	DynamicLibrary lib_same( library_name );

	ASSERT_FALSE( lib_same.is_open() );
}

TEST( DynamicLibrary, i_can_open_a_dynamic_library_using_a_path )
{
#if MOJO_WINDOWS
	auto path = find_path( { "C:/Windows/System32" }, "shell32.dll" );

#elif MOJO_MAC
	// TODO
	ASSERT_TRUE( false );
#else

	auto path = find_path( { "/usr/lib", "/usr/lib64" }, "libdbus-glib-1.so" );
#endif

	ASSERT_TRUE( !path.empty() );

	DynamicLibrary lib( path.string() );

	ASSERT_FALSE( lib.is_open() );

	ASSERT_TRUE( lib.open() );

	ASSERT_TRUE( lib.is_open() );

	ASSERT_TRUE( lib.close() );
}

TEST( DynamicLibrary, i_can_resolve_a_function )
{
#if MOJO_WINDOWS
	DynamicLibrary lib( "winmm" );

	ASSERT_TRUE( lib.open() );

	DynamicLibrary::FunctionPointer fptr = nullptr;

	fptr = lib.resolve( "timeBeginPeriod" );

	ASSERT_TRUE( fptr != nullptr );
#elif MOJO_MAC
	// TODO
	ASSERT_TRUE( false );
#else

	Library lib( "glib-2.0" );

	ASSERT_TRUE( lib.open() );

	Library::FunctionPointer fptr = nullptr;

	fptr = lib.resolve( "g_main_loop_new" );

	ASSERT_TRUE( fptr != nullptr );

#endif
}
