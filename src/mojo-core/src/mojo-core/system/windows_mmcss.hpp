#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class MOJO_API Mmcss
{
public:
	using WINDOWS_HANDLE = void*;

	enum AvrtPriority
	{
		VERYLOW = -2,
		LOW,
		NORMAL,
		HIGH,
		CRITICAL
	};

	/// Windows supports these task "names" by default
	///
	/// Audio
	/// Capture
	/// Distribution
	/// Games
	/// Playback
	/// Pro Audio
	/// Window Manager
	static result<void> set_thread_characteristics( const std::string& task_name,
	                                                WINDOWS_HANDLE* task_handle );

	static result<void>
	revert_thread_characteristics( WINDOWS_HANDLE task_handle );

	static result<void> set_thread_priority( WINDOWS_HANDLE, AvrtPriority );
};

enum class MmcssError
{
	SUCCESS = 0,
	LOADING_DLL,
	RESOLVE_SYMBOL,
	INVALID_TASK_INDEX,
	INVALID_TASK_NAME,
	PRIVILEGE_NOT_HELD,
	UNKNOWN,
};

class MOJO_API MmcssErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override final;
	std::string message( int error_value ) const noexcept override final;
};

MOJO_API const std::error_category&
mmcss_error_category() noexcept;

MOJO_API std::error_condition make_error_condition( MmcssError ) noexcept;

inline std::error_code
make_error_code( MmcssError err )
{
	return { static_cast<int>( err ), mmcss_error_category() };
}

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::MmcssError> : true_type
{
};
} // namespace std