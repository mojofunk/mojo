cmake_minimum_required(VERSION 3.4)

project(mojo-dummy-audio-driver)

set(MOJO_DUMMY_AUDIO_DRIVER_TARGET
    mojo-dummy-audio-driver-${MOJO_VERSION_MAJOR})

add_library(${MOJO_DUMMY_AUDIO_DRIVER_TARGET} SHARED
            src/mojo-dummy-audio-driver.cpp)

add_library(${MOJO_NS}::${MOJO_DUMMY_AUDIO_DRIVER_TARGET} ALIAS
            ${MOJO_DUMMY_AUDIO_DRIVER_TARGET})

target_include_directories(
  ${MOJO_DUMMY_AUDIO_DRIVER_TARGET}
  PUBLIC $<INSTALL_INTERFACE:include/${MOJO_NS}>
         $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>)

target_compile_definitions(${MOJO_DUMMY_AUDIO_DRIVER_TARGET}
                           PRIVATE "MOJO_BUILDING_DLL")

target_link_libraries(
  ${MOJO_DUMMY_AUDIO_DRIVER_TARGET}
  PRIVATE ${MOJO_NS}::mojo-core-${MOJO_VERSION_MAJOR} adt-0::adt-0)

mojo_set_target_cxx_properties(${MOJO_DUMMY_AUDIO_DRIVER_TARGET})

target_compile_options(${MOJO_DUMMY_AUDIO_DRIVER_TARGET}
                       PRIVATE ${MOJO_WARNINGS})

include(GNUInstallDirs)

install(
  TARGETS ${MOJO_DUMMY_AUDIO_DRIVER_TARGET}
  EXPORT ${MOJO_DUMMY_AUDIO_DRIVER_TARGET}-targets
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})

if(MOJO_INSTALL_HEADERS)
  install(
    DIRECTORY src/
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${MOJO_NS}
    FILES_MATCHING
    PATTERN "*.h*"
    PATTERN "*_p.h*" EXCLUDE)
endif()
