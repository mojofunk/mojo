MOJO_BEGIN_NAMESPACE

A_DEFINE_CLASS_MEMBERS( PortaudioAudioDevice )

PortaudioAudioDevice::PortaudioAudioDevice( PaDeviceIndex index )
    : m_device_index( index )
    , m_stream( 0 )
    , m_user_data( 0 )
{
}

PortaudioAudioDevice::~PortaudioAudioDevice() {}

std::string
PortaudioAudioDevice::get_name() const
{
	return get_device_info()->name;
}

int
PortaudioAudioDevice::portaudio_callback( const void* input_buffer,
                                          void* output_buffer,
                                          unsigned long samples_per_buffer,
                                          const PaStreamCallbackTimeInfo*,
                                          PaStreamCallbackFlags,
                                          void* user_data )
{
	A_CLASS_STATIC_CALL1( (uint32_t)samples_per_buffer );

	PortaudioAudioDevice* device = static_cast<PortaudioAudioDevice*>( user_data );

	CallbackResult result = device->m_callback( (float*)input_buffer,
	                                            (float*)output_buffer,
	                                            samples_per_buffer,
	                                            device->m_user_data );

	A_CLASS_STATIC_DATA1( result );

	if( result == CONTINUE ) return 0;
	return 1;
}

result<void>
PortaudioAudioDevice::open( uint32_t input_channels,
                            uint32_t output_channels,
                            double sample_rate,
                            uint32_t buffer_size,
                            Callback* cb,
                            void* user_data )
{
	A_CLASS_CALL4( input_channels, output_channels, sample_rate, buffer_size );

	PaStreamParameters input_params = get_default_input_params();
	PaStreamParameters output_params = get_default_output_params();

	input_params.channelCount = input_channels;
	output_params.channelCount = output_channels;

	m_callback = cb;
	m_user_data = user_data;

	PaError error = Pa_OpenStream( &m_stream,
	                               input_channels > 0 ? &input_params : NULL,
	                               output_channels > 0 ? &output_params : NULL,
	                               sample_rate,
	                               buffer_size,
	                               paDitherOff | paClipOff,
	                               portaudio_callback,
	                               this );
	A_CLASS_DATA1( error );

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}
	return success();
}

result<bool>
PortaudioAudioDevice::is_open() const
{
	return m_stream != NULL;
}

result<void>
PortaudioAudioDevice::start()
{
	A_CLASS_CALL();

	PaError error = Pa_StartStream( m_stream );

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}
	return success();
}

result<bool>
PortaudioAudioDevice::is_active() const
{
	if( m_stream == NULL ) return false;

	PaError error = Pa_IsStreamActive( m_stream );

	// value of 1 indicates stream is active
	if( error == 1 ) return true;

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}
	return false;
}

result<void>
PortaudioAudioDevice::stop()
{
	A_CLASS_CALL();

	PaError error = Pa_StopStream( m_stream );

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}
	return success();
}

result<bool>
PortaudioAudioDevice::is_stopped() const
{
	PaError error = Pa_IsStreamStopped( m_stream );

	// value of 1 indicates stream is stopped
	if( error == 1 ) return true;

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}
	return false;
}

result<void>
PortaudioAudioDevice::close()
{
	A_CLASS_CALL();

	PaError error = Pa_CloseStream( m_stream );

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}

	m_stream = NULL;
	return success();
}

result<void>
PortaudioAudioDevice::abort()
{
	A_CLASS_CALL();

	PaError error = Pa_AbortStream( m_stream );

	if( error )
	{
		return failure( static_cast<PortaudioError>( error ) );
	}
	m_stream = NULL;
	return success();
}

PaDeviceInfo const*
PortaudioAudioDevice::get_device_info() const
{
	return Pa_GetDeviceInfo( m_device_index );
}

PaStreamParameters
PortaudioAudioDevice::get_default_input_params() const
{
	PaStreamParameters input_params;
	input_params.device = m_device_index;
	input_params.channelCount = max_input_channels();
	input_params.sampleFormat = paFloat32;
	input_params.suggestedLatency = 0;
	input_params.hostApiSpecificStreamInfo = 0;
	return input_params;
}

PaStreamParameters
PortaudioAudioDevice::get_default_output_params() const
{
	PaStreamParameters output_params;
	output_params.device = m_device_index;
	output_params.channelCount = max_output_channels();
	output_params.sampleFormat = paFloat32;
	output_params.suggestedLatency = 0;
	output_params.hostApiSpecificStreamInfo = 0;
	return output_params;
}

uint32_t
PortaudioAudioDevice::max_input_channels() const
{
	return get_device_info()->maxInputChannels;
}

uint32_t
PortaudioAudioDevice::max_output_channels() const
{
	return get_device_info()->maxOutputChannels;
}

double
PortaudioAudioDevice::get_default_sample_rate() const
{
	return get_device_info()->defaultSampleRate;
}

std::vector<double>
PortaudioAudioDevice::get_supported_sample_rates() const
{
	std::vector<double> supported_rates;

	PaStreamParameters input_params = get_default_input_params();
	PaStreamParameters output_params = get_default_output_params();

	for( auto const& rate : get_common_sample_rates() )
	{
		if( paFormatIsSupported ==
		    Pa_IsFormatSupported( max_input_channels() > 0 ? &input_params : NULL,
		                          max_output_channels() > 0 ? &output_params : NULL,
		                          rate ) )
		{
			supported_rates.push_back( rate );
		}
	}
	return supported_rates;
}

double
PortaudioAudioDevice::get_input_latency() const
{
	if( m_stream == NULL ) return 0.0;

	const PaStreamInfo* info = Pa_GetStreamInfo( m_stream );

	if( info == NULL ) return 0.0;

	return info->inputLatency;
}

double
PortaudioAudioDevice::get_output_latency() const
{
	if( m_stream == NULL ) return 0.0;

	const PaStreamInfo* info = Pa_GetStreamInfo( m_stream );

	if( info == NULL ) return 0.0;

	return info->outputLatency;
}

double
PortaudioAudioDevice::get_current_sample_rate() const
{
	if( m_stream == NULL ) return 0.0;

	const PaStreamInfo* info = Pa_GetStreamInfo( m_stream );

	if( info == NULL ) return 0.0;

	return info->sampleRate;
}

double
PortaudioAudioDevice::get_cpu_load() const
{
	if( m_stream == NULL ) return 0.0;

	return Pa_GetStreamCpuLoad( m_stream );
}

MOJO_END_NAMESPACE
