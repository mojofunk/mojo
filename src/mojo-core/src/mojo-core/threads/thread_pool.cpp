#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

// The pool name should be used to identify thread in logging and for thread
// name registration.
class ThreadPoolPrivate
{
public:
	ThreadPoolPrivate( const std::string& pool_name,
	                   std::size_t count,
	                   ThreadPool::ThreadFunction thread_function,
	                   ThreadPool::ThreadStartedCallback thread_started_callback )
	    : m_name( pool_name )
	    , m_thread_count( count )
	    , m_thread_function( thread_function )
	    , m_thread_started_callback( thread_started_callback )
	{
	}

	~ThreadPoolPrivate() { stop(); }

	bool is_running() const { return m_running; }

	bool start( ThreadPool::StartOption start_option )
	{
		A_CLASS_CALL();

		if( m_running )
		{
			A_CLASS_MSG( A_FMT( "ThreadPool with name {} is already running", m_name ) );
			return false;
		}

		m_running = true;
		MOJO_ASSERT( m_running_count == 0 );

		for( std::size_t i = 0; i != m_thread_count; ++i )
		{
			m_threads.emplace_back(
			    std::thread( &ThreadPoolPrivate::thread_main, this ) );
		}

		if( start_option == ThreadPool::StartOption::NO_WAIT )
		{
			return true;
		}

		auto const start = std::chrono::system_clock::now();
		auto const timeout = std::chrono::seconds( 5 );

		while( true )
		{

			std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

			auto const elapsed = std::chrono::system_clock::now() - start;

			if( m_running_count == m_thread_count )
			{
				return true;
			}
			if( elapsed > timeout )
			{
				// @todo Should be returning an error here to indicate
				// that any threads that were started should be
				// signalled to finished and then stop() called.
				return false;
			}
		}
	}

	bool stop()
	{
		A_CLASS_CALL();

		if( !m_running )
		{
			return false;
		}

		for( auto& t : m_threads )
		{
			t.join();
		}
		m_threads.clear();
		m_running_count = 0;
		m_running = false;
		// Not sure if there is any use in a return value...
		return true;
	}

	std::size_t thread_count() const { return m_thread_count; }

private:
	void thread_main()
	{
		m_running_count++;

		if( m_thread_started_callback )
		{
			m_thread_started_callback();
		}

		{
			A_CLASS_CALL1( m_name );

			MOJO_ASSERT( m_thread_function );
			m_thread_function();
		}
	}

private: // member data
	std::string const m_name;
	std::size_t const m_thread_count{ 0 };
	ThreadPool::ThreadFunction const m_thread_function;
	ThreadPool::ThreadStartedCallback const m_thread_started_callback;

	std::atomic<bool> m_running{ false };
	std::atomic<std::size_t> m_running_count{ 0 };
	std::vector<std::thread> m_threads;

private:
	A_DECLARE_CLASS_MEMBERS( ThreadPoolPrivate )
};

A_DEFINE_CLASS_MEMBERS( mojo::ThreadPoolPrivate )

ThreadPool::ThreadPool( const std::string& pool_name,
                        std::size_t count,
                        ThreadFunction thread_function,
                        ThreadStartedCallback thread_started_callback )
    : d_ptr( new ThreadPoolPrivate(
          pool_name, count, thread_function, thread_started_callback ) )
{
}

ThreadPool::~ThreadPool() {}

bool
ThreadPool::is_running() const
{
	return d_ptr->is_running();
}

bool
ThreadPool::start( StartOption start_option )
{
	return d_ptr->start( start_option );
}

bool
ThreadPool::stop()
{
	return d_ptr->stop();
}

std::size_t
ThreadPool::thread_count() const
{
	return d_ptr->thread_count();
}

MOJO_END_NAMESPACE
