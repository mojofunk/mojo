#include "mojo_test_includes.hpp"

TEST( StringConversionError, ConvertsToErrorCode )
{
	std::error_code ec = StringConversionError::ILLEGAL_CHAR;

	EXPECT_EQ( "Illegal Character", ec.message() );
}
