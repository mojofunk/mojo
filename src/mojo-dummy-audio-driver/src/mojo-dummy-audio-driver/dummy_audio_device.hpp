MOJO_BEGIN_NAMESPACE

class DummyAudioDevice : public AudioDevice
{
public: // Constructors
	DummyAudioDevice();
	~DummyAudioDevice();

public: // AudioDevice interface
	std::string get_name() const override;

	result<void> open( uint32_t input_channels,
	                   uint32_t output_channels,
	                   double sample_rate,
	                   uint32_t buffer_size,
	                   Callback* cb,
	                   void* user_data ) override;

	result<bool> is_open() const override;

	result<void> start() override;

	result<bool> is_active() const override;

	result<void> stop() override;

	result<bool> is_stopped() const override;

	result<void> abort() override;

	result<void> close() override;

	uint32_t max_input_channels() const override;

	uint32_t max_output_channels() const override;

	double get_default_sample_rate() const override;

	std::vector<double> get_supported_sample_rates() const override;

	double get_input_latency() const override;

	double get_output_latency() const override;

	double get_current_sample_rate() const override;

	double get_cpu_load() const override;

private:
	bool mIsOpen = false;
	bool mIsActive = false;

private:
	A_DECLARE_CLASS_MEMBERS( DummyAudioDevice )
};

MOJO_END_NAMESPACE
