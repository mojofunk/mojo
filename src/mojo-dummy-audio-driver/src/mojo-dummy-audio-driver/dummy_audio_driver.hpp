MOJO_BEGIN_NAMESPACE

class DummyAudioDriver : public AudioDriver
{
public: // constructors
	DummyAudioDriver();
	~DummyAudioDriver();

public: // AudioDriver interface
	AudioDeviceSPSet get_devices() const;

private:
	A_DECLARE_CLASS_MEMBERS( DummyAudioDriver )
};

MOJO_END_NAMESPACE