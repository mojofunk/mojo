MOJO_BEGIN_NAMESPACE

class PortaudioAudioDevice : public AudioDevice
{
public: // Constructors
	PortaudioAudioDevice( PaDeviceIndex );
	~PortaudioAudioDevice() override;

public: // AudioDevice interface
	std::string get_name() const override;

	result<void> open( uint32_t input_channels,
	                   uint32_t output_channels,
	                   double sample_rate,
	                   uint32_t buffer_size,
	                   Callback* cb,
	                   void* user_data ) override;

	result<bool> is_open() const override;

	result<void> start() override;

	result<bool> is_active() const override;

	result<void> stop() override;

	result<bool> is_stopped() const override;

	result<void> abort() override;

	result<void> close() override;

	uint32_t max_input_channels() const override;

	uint32_t max_output_channels() const override;

	double get_default_sample_rate() const override;

	std::vector<double> get_supported_sample_rates() const override;

	double get_input_latency() const override;

	double get_output_latency() const override;

	double get_current_sample_rate() const override;

	double get_cpu_load() const override;

private: // methods
	PaDeviceInfo const* get_device_info() const;

	PaStreamParameters get_default_input_params() const;

	PaStreamParameters get_default_output_params() const;

	static int portaudio_callback( const void* input_buffer,
	                               void* output_buffer,
	                               unsigned long samples_per_buffer,
	                               const PaStreamCallbackTimeInfo* time_info,
	                               PaStreamCallbackFlags status_flags,
	                               void* user_data );

	static std::vector<double> get_common_sample_rates()
	{
		return { 8000.0,  22050.0, 24000.0,  44100.0, 48000.0,
			        88200.0, 96000.0, 176400.0, 192000.0 };
	}

private: // member data
	PaDeviceIndex m_device_index;
	PaStream* m_stream;

	Callback* m_callback;
	void* m_user_data;

private:
	A_DECLARE_CLASS_MEMBERS( PortaudioAudioDevice )
};

MOJO_END_NAMESPACE
