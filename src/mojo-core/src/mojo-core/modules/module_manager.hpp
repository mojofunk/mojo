#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class MOJO_API ModuleManager
{
public:
	static std::vector<fs::path> get_default_directory_paths();

	static void set_directory_paths( std::vector<fs::path> const& );

	static std::vector<fs::path> get_directory_paths();

	static void refresh();

	static ModuleSPVec get_modules();

	template <class T>
	static std::set<std::shared_ptr<T>> get_modules_of_type()
	{
		std::set<std::shared_ptr<T>> modules_of_type;

		for( auto const module : get_modules() )
		{
			std::shared_ptr<T> module_of_type = std::dynamic_pointer_cast<T>( module );
			if( module_of_type )
			{
				modules_of_type.insert( module_of_type );
			}
		}
		return modules_of_type;
	}

private:
	ModuleManager() = delete;
};

MOJO_END_NAMESPACE
