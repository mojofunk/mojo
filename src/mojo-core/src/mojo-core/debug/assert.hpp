#pragma once

#include "mojo-core.hpp"

MOJO_CAPI void
mojo_assertion_failure( const char* expression,
                        const char* file,
                        int line,
                        const char* message );

MOJO_CAPI void
mojo_debug_break();

#if !defined( NDEBUG ) || defined( MOJO_ASSERT_ENABLED )

#define MOJO_ASSERT_INTERNAL( expression, message, ... )                       \
	( ( expression ) ? ( (void)0 )                                                \
	                 : mojo_assertion_failure(                                    \
	                       #expression, __FILE__, __LINE__, ( message ) ) )

#else

#define MOJO_ASSERT_INTERNAL( expression, message, ... ) ( (void)0 )

#endif

// MSVC doesn't expand __VA_ARGS__ as you would expect, so this is a workaround,
// see
// https://stackoverflow.com/questions/5134523/msvc-doesnt-expand-va-args-correctly
#define MOJO_EXPAND( x ) ( x )

#define MOJO_ASSERT( ... )                                                     \
	MOJO_EXPAND( MOJO_ASSERT_INTERNAL( __VA_ARGS__, 0, 0 ) )