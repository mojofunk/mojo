#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( Mmcss, "mojo::Mmcss" )
}

typedef HANDLE( WINAPI* AvSetMmThreadCharacteristicsA_t )( LPCSTR TaskName,
                                                           LPDWORD TaskIndex );

typedef BOOL( WINAPI* AvRevertMmThreadCharacteristics_t )( HANDLE AvrtHandle );

typedef BOOL( WINAPI* AvSetMmThreadPriority_t )(
    HANDLE AvrtHandle, mojo::Mmcss::AvrtPriority Priority );

static HMODULE avrt_dll = NULL;

static AvSetMmThreadCharacteristicsA_t AvSetMmThreadCharacteristicsA = NULL;
static AvRevertMmThreadCharacteristics_t AvRevertMmThreadCharacteristics = NULL;
static AvSetMmThreadPriority_t AvSetMmThreadPriority = NULL;

class MmcssPrivate
{
public:
	M_SINGLETON( MmcssPrivate );

	MmcssPrivate()
	    : m_init_result( initialize() )
	{
	}

	~MmcssPrivate() { m_init_result = deinitialize(); }

	result<void> get_init_result() { return m_init_result; }

private:
	result<void> initialize()
	{
		A_LOG_CLASS_CALL( log::Mmcss );

		// MOJO_ASSERT(avrt_dll == NULL);

		avrt_dll = LoadLibraryA( "avrt.dll" );

		if( avrt_dll == NULL )
		{
			return failure( MmcssError::LOADING_DLL );
		}

		AvSetMmThreadCharacteristicsA =
		    (AvSetMmThreadCharacteristicsA_t)GetProcAddress(
		        avrt_dll, "AvSetMmThreadCharacteristicsA" );

		AvRevertMmThreadCharacteristics =
		    (AvRevertMmThreadCharacteristics_t)GetProcAddress(
		        avrt_dll, "AvRevertMmThreadCharacteristics" );

		AvSetMmThreadPriority = (AvSetMmThreadPriority_t)GetProcAddress(
		    avrt_dll, "AvSetMmThreadPriority" );

		if( AvSetMmThreadCharacteristicsA == NULL ||
		    AvRevertMmThreadCharacteristics == NULL || AvSetMmThreadPriority == NULL )
		{
			auto result = deinitialize();

			// deinitialize() should always be successful, this is just
			// silence unused result warning. todo add MOJO_UNUSED macro
			if( !result )
			{
				return result;
			}
			return failure( MmcssError::RESOLVE_SYMBOL );
		}

		return success();
	}

	result<void> deinitialize()
	{
		// MOJO_ASSERT(avrt_dll != NULL);

		if( FreeLibrary( avrt_dll ) == 0 )
		{
			return MmcssError::LOADING_DLL;
		}

		avrt_dll = NULL;

		AvSetMmThreadCharacteristicsA = NULL;
		AvRevertMmThreadCharacteristics = NULL;
		AvSetMmThreadPriority = NULL;

		return success();
	}

private: // Data
	result<void> m_init_result;
};

// Singleton interface
MmcssPrivate*
MmcssPrivate::get_instance()
{
	return Singleton<MmcssPrivate>::get();
}

// take handle by reference?
result<void>
Mmcss::set_thread_characteristics( const std::string& task_name,
                                   WINDOWS_HANDLE* task_handle )
{
	A_LOG_CALL( log::Mmcss );

	auto result = MmcssPrivate::get_instance()->get_init_result();

	if( !result )
	{
		return result;
	}

	DWORD task_index_dummy = 0;

	*task_handle =
	    AvSetMmThreadCharacteristicsA( task_name.c_str(), &task_index_dummy );

	if( *task_handle == 0 )
	{
		DWORD error = GetLastError();

		switch( error )
		{
		case ERROR_INVALID_TASK_INDEX:
			return failure( MmcssError::INVALID_TASK_INDEX );
			break;
		case ERROR_INVALID_TASK_NAME:
			return failure( MmcssError::INVALID_TASK_NAME );
			break;
		case ERROR_PRIVILEGE_NOT_HELD:
			return failure( MmcssError::PRIVILEGE_NOT_HELD );
			break;
		default:
			return failure( MmcssError::UNKNOWN );
			break;
		}
	}

	return success();
}

result<void>
Mmcss::revert_thread_characteristics( WINDOWS_HANDLE task_handle )
{
	A_LOG_CALL( log::Mmcss );

	auto result = MmcssPrivate::get_instance()->get_init_result();

	if( !result )
	{
		return result;
	}

	if( AvRevertMmThreadCharacteristics( task_handle ) == 0 )
	{
		return failure( MmcssError::UNKNOWN );
	}

	return success();
}

result<void>
Mmcss::set_thread_priority( WINDOWS_HANDLE task_handle, AvrtPriority priority )
{
	A_LOG_CALL( log::Mmcss );

	auto result = MmcssPrivate::get_instance()->get_init_result();

	if( !result )
	{
		return result;
	}

	if( AvSetMmThreadPriority( task_handle, priority ) == 0 )
	{
		return failure( MmcssError::UNKNOWN );
	}

	return success();
}

const char*
MmcssErrorCategory::name() const noexcept
{
	return "MmcssError";
}

std::string
MmcssErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<MmcssError>( error_value ) )
	{
	case MmcssError::SUCCESS:
		return "MMCSS: Operation Successful";
	case MmcssError::LOADING_DLL:
		return "MMCSS: Error Loading/Unloading dll";
	case MmcssError::RESOLVE_SYMBOL:
		return "MMCSS: Error resolving symbol";
	case MmcssError::INVALID_TASK_INDEX:
		return "MMCSS: Invalid Task Index";
	case MmcssError::INVALID_TASK_NAME:
		return "MMCSS: Invalid Task Name";
	case MmcssError::PRIVILEGE_NOT_HELD:
		return "MMCSS: Privilege not held";
	default:
		return "MMCSS: Unknown error";
	}
}

const std::error_category&
mmcss_error_category() noexcept
{
	static MmcssErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( MmcssError error ) noexcept
{
	return std::error_condition( static_cast<int>( error ),
	                             mmcss_error_category() );
}

MOJO_END_NAMESPACE