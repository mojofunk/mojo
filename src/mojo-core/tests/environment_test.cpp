#include "mojo_test_includes.hpp"

TEST( Environment, i_can_check_if_the_environment_has_a_variable_set )
{
	ASSERT_TRUE( Environment::has_variable( "PATH" ) );
	ASSERT_FALSE( Environment::has_variable( "BLAHBLAHCARS" ) );
}
