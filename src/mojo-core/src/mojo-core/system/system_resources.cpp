#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( SystemResources, "mojo::SystemResources" )
}

const char*
SystemResources::ErrorCategory::name() const noexcept
{
	return "SystemResources::Error";
}

std::string
SystemResources::ErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<SystemResourcesError>( error_value ) )
	{
	case SystemResourcesError::SUCCESS:
		return "Success";
	case SystemResourcesError::BAD_ADDRESS:
		return "Bad Address";
	case SystemResourcesError::NO_PERMISSION:
		return "Not Permitted";
	case SystemResourcesError::INVALID_VALUE:
		return "Invalid Value";
	case SystemResourcesError::NO_PROCESS:
		return "No Process";
	case SystemResourcesError::UNSUPPORTED:
		return "Unsupported";
	default:
		return "Unknown SystemResources error";
	}
}

const std::error_category&
SystemResources::error_category() noexcept
{
	static SystemResources::ErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( SystemResourcesError error ) noexcept
{
	return std::error_condition( static_cast<int>( error ),
	                             SystemResources::error_category() );
}

class SystemResourcesPrivate
{
public:
	static result<void> get_open_files_limit( SystemResources::Limits& limits )
	{
#ifdef MOJO_WINDOWS
		limits.current_limit = _getmaxstdio();
		limits.max_limit = 2048;
		return success();
#else
		struct rlimit rl;
		if( getrlimit( RLIMIT_NOFILE, &rl ) == 0 )
		{
			limits.current_limit = rl.rlim_cur;
			limits.max_limit = rl.rlim_max;
			return success();
		}
		return failure( SystemResourcesError::NO_PERMISSION );
#endif
	}

	static result<void>
	set_open_files_limit( const SystemResources::Limits& limits )
	{
#ifdef MOJO_WINDOWS
		// no soft and hard limits on windows
		int new_max = _setmaxstdio( static_cast<int>( limits.current_limit ) );

		if( new_max != -1 && new_max == limits.current_limit )
		{
			return success();
		}
		return failure( SystemResourcesError::NO_PERMISSION );
#else
		struct rlimit rl;
		rl.rlim_cur = limit.current_limit;
		rl.rlim_max = limit.max_limit;
		if( auto result = setrlimit( RLIMIT_NOFILE, &rl ) )
		{
			if( result == 0 )
			{
				return success();
			}
			return failure( SystemResourcesError::NO_PERMISSION );
		}

		return failure( SystemResourcesError::UNSUPPORTED );
#endif
	}

	static result<void> get_mem_lock_limit( SystemResources::Limits& limits )
	{
		(void)limits;

#ifdef MOJO_LINUX
		struct rlimit rl;

		if( getrlimit( RLIMIT_MEMLOCK, &rl ) != 0 )
		{
			return failure( SystemResourcesError::UNSUPPORTED );
		}

		if( rl.rlim_cur != RLIM_INFINITY )
		{
			return std::numeric_limits<int64_t>::max();
		}

		limit.current_limit = rl.rlim_cur;
		limit.max_limit = rl.rlim_max;
#endif
		return failure( SystemResourcesError::UNSUPPORTED );
	}

	static result<void> set_mem_lock_limit( const SystemResources::Limits& limits )
	{
		(void)limits;

#ifdef MOJO_LINUX
		/// TODO setrlimit
#elif MOJO_WINDOWS
		/// TODO SetProcessWorkingSetSize() ?
#endif
		return failure( SystemResourcesError::UNSUPPORTED );
	}
};

result<void>
SystemResources::get_limit( SystemResources::Type resource_type,
                            SystemResources::Limits& limits )
{
	switch( resource_type )
	{
	case SystemResources::Type::OPEN_FILES:
		return SystemResourcesPrivate::get_open_files_limit( limits );
	case SystemResources::Type::MEM_LOCK:
		return SystemResourcesPrivate::get_mem_lock_limit( limits );
	default:
		break;
	}
	return failure( SystemResourcesError::UNSUPPORTED );
}

result<void>
SystemResources::set_limit( Type resource_type,
                            const SystemResources::Limits& limits )
{
	switch( resource_type )
	{
	case SystemResources::Type::OPEN_FILES:
		return SystemResourcesPrivate::set_open_files_limit( limits );
	case SystemResources::Type::MEM_LOCK:
		return SystemResourcesPrivate::set_mem_lock_limit( limits );
	default:
		break;
	}
	return failure( SystemResourcesError::UNSUPPORTED );
}

result<int64_t>
SystemResources::physical_memory_size()
{
#if MOJO_LINUX
	int64_t pages;
	int64_t page_size;

	if( ( page_size = sysconf( _SC_PAGESIZE ) ) < 0 ||
	    ( pages = sysconf( _SC_PHYS_PAGES ) ) < 0 )
	{
		return failure( SystemResourcesError::UNSUPPORTED );
	}
	return (int64_t)pages * (int64_t)page_size;
#elif MOJO_WINDOWS
	SYSTEM_INFO system_info;
	GetSystemInfo( &system_info );
	return system_info.dwPageSize;
#endif
}

MOJO_END_NAMESPACE
