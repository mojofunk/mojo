option(MOJO_BUILD_SHARED_LIBS "Build shared library." ${BUILD_SHARED_LIBS})
option(MOJO_BUILD_UNITY "Build library as single source file." ON)
option(MOJO_BUILD_TESTS "Build tests." ${MOJO_MASTER_PROJECT})
option(MOJO_BUILD_SINGLE_TESTS "Build single test executables." OFF)
option(MOJO_WARNINGS_AS_ERRORS "Treat compiler warnings as errors" ON)
option(MOJO_INSTALL_HEADERS "Install header files." ${MOJO_MASTER_PROJECT})
option(MOJO_INSTALL_PKGCONFIG "Install pkg-config related files."
       ${MOJO_MASTER_PROJECT})
option(MOJO_INSTALL_CMAKECONFIG "Install CMake export target files."
       ${MOJO_MASTER_PROJECT})
option(MOJO_ENABLE_COVERAGE "Enable code coverage of tests." OFF)
