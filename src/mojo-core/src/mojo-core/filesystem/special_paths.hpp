#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

enum SpecialPathType
{
	UserHomeDirectory,
	UserTempDirectory,
	UserDataDirectory,
	UserConfigDirectory,
	UserDesktopDirectory,
	UserDocumentsDirectory,
	UserMusicDirectory,
	UserMoviesDirectory,
	UserPicturesDirectory,
	CommonTempDirectory,
	CommonDataDirectories,
	CommonConfigDirectories,
	CommonAppDirectories,
	ModuleFile,
	ExecutableFile
};

MOJO_API fs::path
get_special_path( SpecialPathType const );

MOJO_API std::vector<fs::path>
get_special_paths( SpecialPathType const );

MOJO_END_NAMESPACE