#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

M_DECLARE_CLASS_AND_POINTER_ALIASES( DynamicLibraryPrivate );

class MOJO_API DynamicLibrary
{
public:
	using FunctionPointer = void ( * )();

public: // ctors
	/**
	 * @param path A filename or absolute path to shared library.
	 *
	 * If a filename is given then the system locations for shared
	 * libraries are used to locate the shared object file.
	 *
	 * A generic name for the shared library can be given without the
	 * platform specific prefixes like "lib" and file extensions such as
	 * ".so" and ".dll".
	 */
	DynamicLibrary( fs::path const& path );

	/**
	 * Closes the library if open.
	 */
	~DynamicLibrary();

public: // member API
	fs::path get_path() const;

	bool open();

	bool is_open();

	bool close();

	FunctionPointer resolve( std::string const& symbol_name );

public: // static API
	/**
	 * @return true if path has a valid extension for a loadable
	 * library. path can be a filename or absolute path.
	 */
	static bool is_library( fs::path const& path );

private: // data members
	M_DECLARE_PRIVATE( DynamicLibrary );

	DynamicLibraryPrivateUP d_ptr;
};

MOJO_DEFINE_POINTER_ALIASES( DynamicLibrary );

MOJO_END_NAMESPACE