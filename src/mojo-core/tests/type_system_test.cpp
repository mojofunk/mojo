#include "mojo_test_includes.hpp"

template <class T>
bool
test_type_id( const TypeID& type_id )
{
	return ( TypeSystem::get_type_id( typeid( T ) ) == type_id );
}

TEST( TypeSystem, BuiltinTypes )
{
	ASSERT_FALSE( test_type_id<int>( "int" ) );
	ASSERT_FALSE( test_type_id<long int>( "long int" ) );

	ASSERT_FALSE( test_type_id<int8_t>( "int8_t" ) );
	ASSERT_FALSE( test_type_id<int16_t>( "int16_t" ) );

	ASSERT_TRUE( test_type_id<int32_t>( "int32_t" ) );
	ASSERT_TRUE( test_type_id<uint32_t>( "uint32_t" ) );
	ASSERT_TRUE( test_type_id<int64_t>( "int64_t" ) );
	ASSERT_TRUE( test_type_id<uint64_t>( "uint64_t" ) );
	ASSERT_TRUE( test_type_id<float>( "float" ) );
	ASSERT_TRUE( test_type_id<double>( "double" ) );
	ASSERT_TRUE( test_type_id<std::string>( "std::string" ) );
}

TEST( TypeSystem, RegisterTypeFactory )
{
	class RegisterTypeFactoryTest
	{
	};

	const char* const class_name = "RegisterTypeFactorysTest";

	ASSERT_FALSE( test_type_id<RegisterTypeFactoryTest>( class_name ) );

	TypeSystem::register_default_type_factory<RegisterTypeFactoryTest>(
	    "RegisterTypeFactorysTest" );

	//	TypeSystem::register_type_factory(TypeFactorySP(
	//    new TemplateTypeFactory<RegisterTypeFactoryTest>(class_name)));

	ASSERT_TRUE( test_type_id<RegisterTypeFactoryTest>( class_name ) );
}

template <class T>
void
test_type_factory( const TypeID& type_id )
{
	T* instance = std::any_cast<T*>( TypeSystem::create( type_id ) );

	ASSERT_TRUE( instance );

	delete instance;
}

TEST( TypeSystem, TypeFactory )
{
	ASSERT_ANY_THROW( test_type_factory<int8_t>( "int8_t" ) );
	ASSERT_ANY_THROW( test_type_factory<int16_t>( "int16_t" ) );

	ASSERT_NO_THROW( test_type_factory<int32_t>( "int32_t" ) );
	ASSERT_NO_THROW( test_type_factory<uint32_t>( "uint32_t" ) );

	ASSERT_NO_THROW( test_type_factory<int64_t>( "int64_t" ) );
	ASSERT_NO_THROW( test_type_factory<uint64_t>( "uint64_t" ) );

	ASSERT_NO_THROW( test_type_factory<float>( "float" ) );
	ASSERT_NO_THROW( test_type_factory<double>( "double" ) );

	ASSERT_NO_THROW( test_type_factory<std::string>( "std::string" ) );
}

enum FooEnum
{
	FOO_1,
	FOO_2,
};

TEST( TypeSystem, i_can_compare_the_type_id_of_two_values_from_the_same_enum )
{
	FooEnum foo1 = FOO_1;
	FooEnum foo2 = FOO_2;

	TypeID id_1 = TypeSystem::get_type_id( foo1 );
	TypeID id_2 = TypeSystem::get_type_id( foo2 );
	ASSERT_TRUE( id_1 == id_2 );
}
