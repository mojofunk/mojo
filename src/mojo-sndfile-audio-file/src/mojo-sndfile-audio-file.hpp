#ifndef MOJO_SNDFILE_AUDIO_FILE_H
#define MOJO_SNDFILE_AUDIO_FILE_H

#include <mojo-core.hpp>

#include <adt.hpp>

#if MOJO_WINDOWS
#include <windows.h>
#define ENABLE_SNDFILE_WINDOWS_PROTOTYPES 1
#endif

#include <sndfile.h>

#include <mojo-sndfile-audio-file/sndfile_audio_file.hpp>
#include <mojo-sndfile-audio-file/sndfile_audio_file_format.hpp>
#include <mojo-sndfile-audio-file/sndfile_audio_file_module.hpp>
#include <mojo-sndfile-audio-file/sndfile_error.hpp>

#endif // MOJO_SNDFILE_AUDIO_FILE_H