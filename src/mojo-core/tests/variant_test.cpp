#include "mojo_test_includes.hpp"

class VariantTestObject : public Object
{
};

template <class T>
T
variant_roundtrip( const T& v1 )
{
	Variant var_type( v1 );
	return std::get<T>( var_type );
}

template <typename T>
bool
isEqual( std::vector<T> const& v1, std::vector<T> const& v2 )
{
	return ( v1.size() == v2.size() &&
	         std::equal( v1.begin(), v1.end(), v2.begin() ) );
}

template <typename T>
std::vector<std::shared_ptr<T>>
ConvertToSPtrVec( const std::vector<std::weak_ptr<T>>& weak_ptr_vec )
{
	std::vector<std::shared_ptr<T>> shared_ptr_vec;

	for( auto& wptr : weak_ptr_vec )
	{
		std::shared_ptr<T> sptr = wptr.lock();

		if( sptr )
		{
			shared_ptr_vec.push_back( std::move( sptr ) );
		}
	}
	return shared_ptr_vec;
}

TEST( Variant, RoundTrip )
{
	int32_t const int32_val = -42;
	ASSERT_TRUE( int32_val == variant_roundtrip( int32_val ) );

	uint32_t const uint32_val = 12;
	ASSERT_TRUE( uint32_val == variant_roundtrip( uint32_val ) );

	int64_t const int64_val = std::numeric_limits<int64_t>::min();
	ASSERT_TRUE( int64_val == variant_roundtrip( int64_val ) );

	uint64_t const uint64_val = std::numeric_limits<int64_t>::max();
	ASSERT_TRUE( uint64_val == variant_roundtrip( uint64_val ) );

	auto object_sptr = std::make_shared<VariantTestObject>();
	std::weak_ptr<Object> object_wptr = object_sptr;

	auto wptr = variant_roundtrip( object_wptr );

	std::shared_ptr<Object> sptr = wptr.lock();

	EXPECT_EQ( object_sptr, sptr );

	ObjectWPVec object_list;

	object_list.push_back( wptr );

	auto object_list_copy = variant_roundtrip( object_list );

	auto object_list_sptr = ConvertToSPtrVec( object_list );

	auto object_list_copy_sptr = ConvertToSPtrVec( object_list_copy );

	EXPECT_TRUE( isEqual( object_list_sptr, object_list_copy_sptr ) );
}