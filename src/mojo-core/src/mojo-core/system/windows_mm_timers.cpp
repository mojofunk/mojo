#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( MmTimers, "mojo::MmTimers" )
}

class MmTimersPrivate
{
public:
	static UINT& timer_resolution()
	{
		static UINT timer_res_ms = 0;
		return timer_res_ms;
	}
};

result<uint32_t>
MmTimers::get_min_resolution()
{
	A_LOG_CALL( log::MmTimers );

	TIMECAPS caps;

	const auto error = timeGetDevCaps( &caps, sizeof( TIMECAPS ) );

	if( error != TIMERR_NOERROR )
	{
		return failure( MmTimersError::UNKNOWN );
	}

	return caps.wPeriodMin;
}

result<void>
MmTimers::set_min_resolution()
{
	A_LOG_CALL( log::MmTimers );

	auto result = get_min_resolution();

	if( !result )
	{
		return failure( result.error() );
	}

	return set_resolution( result.value() );
}

result<void>
MmTimers::set_resolution( uint32_t timer_resolution_ms )
{
	A_LOG_CALL1( log::MmTimers, timer_resolution_ms );

	if( MmTimersPrivate::timer_resolution() != 0 )
	{
		return failure( MmTimersError::MISMATCHED_CALLS );
	}

	auto error = timeBeginPeriod( timer_resolution_ms );

	if( error == TIMERR_NOCANDO )
	{
		return failure( MmTimersError::OUT_OF_RANGE );
	}

	if( error != TIMERR_NOERROR )
	{
		return failure( MmTimersError::UNKNOWN );
	}

	MmTimersPrivate::timer_resolution() = timer_resolution_ms;

	return success();
}

result<void>
MmTimers::reset_resolution()
{
	A_LOG_CALL1( log::MmTimers, MmTimersPrivate::timer_resolution() );

	auto& resolution = MmTimersPrivate::timer_resolution();
	if( resolution == 0 )
	{
		return failure( MmTimersError::MISMATCHED_CALLS );
	}

	if( timeEndPeriod( resolution ) != TIMERR_NOERROR )
	{
		return failure( MmTimersError::UNKNOWN );
	}

	resolution = 0;

	return success();
}

uint32_t
MmTimers::get_time_ms()
{
	return timeGetTime();
}

const char*
MmTimersErrorCategory::name() const noexcept
{
	return "MmTimersError";
}

std::string
MmTimersErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<MmTimersError>( error_value ) )
	{
	case MmTimersError::SUCCESS:
		return "MMTimers: Operation Successful";
	case MmTimersError::OUT_OF_RANGE:
		return "MMTimers: Timer value out of supported range";
	case MmTimersError::MISMATCHED_CALLS:
		return "MMTimers: Mismatched calls to set/reset_resolution()";
	default:
		return "MMTimers: Unknown error";
	}
}

const std::error_category&
mm_timers_error_category() noexcept
{
	static MmTimersErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( MmTimersError error ) noexcept
{
	return std::error_condition( static_cast<int>( error ),
	                             mm_timers_error_category() );
}

MOJO_END_NAMESPACE