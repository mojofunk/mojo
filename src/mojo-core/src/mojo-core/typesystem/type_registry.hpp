#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class TypeRegistry
{
public:
	void set_type_id( const std::type_info& info, const TypeID& type_id )
	{
		m_type_names.insert( std::make_pair( &info, type_id ) );
	}

	TypeID get_type_id( const std::type_info& info ) const
	{
		TypeNameMap::const_iterator i = m_type_names.find( &info );

		if( i != m_type_names.end() )
		{
			return i->second;
		}

		return "unknown";
	}

private:
	struct TypeInfoComp
	{
		bool operator()( const std::type_info* lhs, const std::type_info* rhs ) const
		{
			return lhs->before( *rhs );
		}
	};

	using TypeNameMap = std::map<const std::type_info*, TypeID, TypeInfoComp>;

	TypeNameMap m_type_names;
};

MOJO_END_NAMESPACE
