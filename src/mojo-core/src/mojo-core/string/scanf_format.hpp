#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

template <typename T>
const char* const
scanf_format( const T& )
{
	// Force a compilation error
	return T::SCANF_FORMAT_SPECIFIER_UNDEFINED;
}

template <>
inline const char* const
scanf_format( const int16_t& )
{
	return "%" SCNi16;
}

template <>
inline const char* const
scanf_format( const int32_t& )
{
	return "%" SCNi32;
}

template <>
inline const char* const
scanf_format( const int64_t& )
{
	return "%" SCNi64;
}

template <>
inline const char* const
scanf_format( const uint16_t& )
{
	return "%" SCNu16;
}

template <>
inline const char* const
scanf_format( const uint32_t& )
{
	return "%" SCNu32;
}

template <>
inline const char* const
scanf_format( const uint64_t& )
{
	return "%" SCNu64;
}

template <>
inline const char* const
scanf_format( const float& )
{
	return "%f";
}

template <>
inline const char* const
scanf_format( const double& )
{
	return "%f";
}

MOJO_END_NAMESPACE