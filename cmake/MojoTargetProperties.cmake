function(mojo_set_target_cxx_properties IN_TARGET)

  if(NOT CMAKE_CXX_STANDARD)
    set_target_properties(
      ${IN_TARGET}
      PROPERTIES CXX_STANDARD 17
                 CXX_STANDARD_REQUIRED ON
                 CXX_EXTENSIONS OFF)
  endif()

endfunction()
