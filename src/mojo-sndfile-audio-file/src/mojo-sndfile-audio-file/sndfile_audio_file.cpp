MOJO_BEGIN_NAMESPACE

A_DEFINE_CLASS_MEMBERS( SndfileAudioFile )

SndfileAudioFile::SndfileAudioFile( fs::path const& path )
    : m_path( path )
    , m_sf( NULL )
{
}

SndfileAudioFile::~SndfileAudioFile()
{
	if( is_open() )
	{
		close();
	}
}

result<void>
SndfileAudioFile::open( OpenMode open_mode )
{
	assert( m_sf == NULL );

	std::error_code ec;
	if( !fs::exists( m_path, ec ) )
	{
		A_CLASS_MSG( A_FMT( "Unable to open audio file at path {}",
		                    m_path.generic_u8string() ) );
		// no sure why make_error_code needs to be explicit here...
		return ec ? ec : make_error_code( std::errc::no_such_file_or_directory );
	}

	auto const sf_mode = open_mode_to_sf_mode( open_mode );

	auto const native_c_str = m_path.c_str();

#if MOJO_WINDOWS
	m_sf = sf_wchar_open( native_c_str, sf_mode, &m_info );
#else
	m_sf = sf_open( native_c_str, sf_mode, &m_info );
#endif

	if( m_sf == NULL )
	{
		A_CLASS_MSG( A_FMT( "Unable to open sndfile audio file at path {}",
		                    m_path.generic_u8string() ) );
		return static_cast<SndfileError>( sf_error( m_sf ) );
	}

	m_format = AudioFileFormatSP( new SndfileAudioFileFormat( m_info.format ) );

	return success();
}

result<void>
SndfileAudioFile::create( AudioFileFormatSP format,
                          double sample_rate,
                          uint32_t channels )
{
	std::error_code ec;

	if( fs::exists( m_path, ec ) )
	{
		A_CLASS_MSG( A_FMT( "Unable to create audio file at path {}",
		                    m_path.generic_u8string() ) );
		return ec ? ec : make_error_code( std::errc::file_exists );
	}

	auto sndfile_format =
	    std::dynamic_pointer_cast<SndfileAudioFileFormat>( format );

	// assume at this stage it is a programming error if this occurs
	assert( sndfile_format );

	m_info.format = sndfile_format->format();
	m_info.samplerate = (int)sample_rate;
	m_info.channels = channels;

	assert( m_sf == NULL );

	// This is LPCWSTR on Windows and const char* otherwise.
	auto const native_c_str = m_path.c_str();

#if MOJO_WINDOWS
	m_sf = sf_wchar_open( native_c_str, SFM_RDWR, &m_info );
#else
	// Some formats (FLAC?) can only be opened in either read or write mode but not
	// both. Does this mean we should retry open with SFM_WRITE if RDWR fails?
	m_sf = sf_open( native_c_str, SFM_RDWR, &m_info );
#endif

	if( m_sf == NULL )
	{
		A_CLASS_MSG( A_FMT( "Unable to open sndfile audio file at path {}",
		                    m_path.generic_u8string() ) );
		return static_cast<SndfileError>( sf_error( m_sf ) );
	}

	m_format = AudioFileFormatSP( new SndfileAudioFileFormat( m_info.format ) );

	return success();
}

bool
SndfileAudioFile::is_open() const
{
	return m_sf != NULL;
}

bool
SndfileAudioFile::close()
{
	int success = sf_close( m_sf );
	m_sf = NULL;
	m_format.reset();
	return success == 0;
}

AudioFileFormatSP
SndfileAudioFile::format() const
{
	return m_format;
}

int64_t
SndfileAudioFile::read_samples( float* buf, int64_t sample_count )
{
	return sf_readf_float( m_sf, buf, sample_count );
}

int64_t
SndfileAudioFile::write_samples( float* buf, int64_t sample_count )
{
	return sf_writef_float( m_sf, buf, sample_count );
}

int64_t
SndfileAudioFile::seek( int64_t sample_offset )
{
	return sf_seek( m_sf, sample_offset, SEEK_SET );
}

int64_t
SndfileAudioFile::samples() const
{
	// read current sample offset
	sf_count_t sample_pos = sf_seek( m_sf, 0, SEEK_CUR );
	// seek to end
	sf_count_t samples = sf_seek( m_sf, 0, SEEK_END );
	// restore previous offset
	sf_seek( m_sf, sample_pos, SEEK_SET );

	return samples;
}

double
SndfileAudioFile::sample_rate() const
{
	return m_info.samplerate;
}

uint32_t
SndfileAudioFile::channels() const
{
	return m_info.channels;
}

int
SndfileAudioFile::open_mode_to_sf_mode( OpenMode open_mode )
{
	if( open_mode == OpenMode::READ )
	{
		return SFM_READ;
	}
	else if( open_mode == OpenMode::WRITE )
	{
		return SFM_WRITE;
	}
	return SFM_RDWR;
}

MOJO_END_NAMESPACE
