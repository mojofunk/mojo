#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class AudioFileFormat
{
public:
	virtual ~AudioFileFormat();

	virtual std::string name() const = 0;

	virtual std::string extension() const = 0;

protected:
	AudioFileFormat();
};

MOJO_DEFINE_ALL_ALIASES( AudioFileFormat );

MOJO_END_NAMESPACE