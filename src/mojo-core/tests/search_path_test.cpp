#include "mojo_test_includes.hpp"

TEST( Searchpath, Ctors )
{
	fs::path( "/usr/bin" );

#if MOJO_WINDOWS
	std::string windows_search_path_string = "C:/Program Files;D:/tmp";

	Searchpath search_path( windows_search_path_string );
#else
	std::string posix_search_path_string = "/usr/bin:/usr/local/bin";

	Searchpath search_path( posix_search_path_string );
#endif

	ASSERT_TRUE( search_path.size() == 2 );
}

TEST( Searchpath, Operators ) {}
