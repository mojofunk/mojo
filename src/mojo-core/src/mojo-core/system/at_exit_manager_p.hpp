#pragma once

#include "mojo-core-private.hpp"

MOJO_BEGIN_NAMESPACE

/**
 * This is a singleton class that handles the execution of functors
 * registered at exit, which will be called during static
 * deinitialization.
 *
 * Which means that extreme care must be taken if functions registered
 * with AtExitManager reference other statically allocated objects, if
 * not totally avoided.
 *
 * This class primarily exists to manage Singleton's allocated on the
 * heap upon exit, although it can be used for other things.
 */
class AtExitManager
{
public:
	/**
	 * Functors registered with on_exit called in the same order they are
	 * added.
	 */
	static void on_exit( std::function<void()> func );

	/**
	 * Call functors registered with on_exit now rather than automatically on
	 * process termination.
	 */
	static void execute_functors_now();

private:
	M_DISALLOW_COPY_AND_ASSIGN( AtExitManager );
	AtExitManager() = delete;
};

MOJO_END_NAMESPACE