cmake_minimum_required(VERSION 3.16)

message(
  "------------------------------------------------------------------------")
message("")
message("Configuring - mojo")
message("")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# Determine if adt is built as a subproject (using add_subdirectory) or if it is
# the master project.
set(MOJO_MASTER_PROJECT OFF)
if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
  set(MOJO_MASTER_PROJECT ON)
endif()

include(MojoOptions)

include(MojoVersion)

include(MojoCMakeNamespace)

if(MOJO_ENABLE_COVERAGE)
  include(CodeCoverage)
  append_coverage_compiler_flags()
endif()

include(cmake-common/CompilerWarnings)

get_target_warnings(MOJO_WARNINGS ${MOJO_WARNINGS_AS_ERRORS})

# required?
project(mojo)

add_subdirectory(src/mojo-core)

add_subdirectory(src/mojo-dummy-audio-driver)

set(MOJO_BUILD_PORTAUDIO_AUDIO_DRIVER TRUE)

if(MOJO_BUILD_PORTAUDIO_AUDIO_DRIVER)
  add_subdirectory(src/mojo-portaudio-audio-driver)
endif()

set(MOJO_BUILD_SNDFILE_AUDIO_FILE TRUE)

if(MOJO_BUILD_SNDFILE_AUDIO_FILE)
  add_subdirectory(src/mojo-sndfile-audio-file)
endif()

include(MojoConfigSummary)
