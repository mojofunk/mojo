MOJO_BEGIN_NAMESPACE

PortaudioAudioDriverModule::PortaudioAudioDriverModule(
    DynamicLibrarySP library )
    : AudioDriverModule( library )
{
}

PortaudioAudioDriverModule::~PortaudioAudioDriverModule() {}

std::string
PortaudioAudioDriverModule::get_author()
{
	return "Tim Mayberry";
}

std::string
PortaudioAudioDriverModule::get_description()
{
	return "Portaudio module";
}

std::string
PortaudioAudioDriverModule::get_version()
{
	return "0.0.1";
}

AudioDriverSP
PortaudioAudioDriverModule::create_driver() const
{
	return AudioDriverSP( new PortaudioAudioDriver );
}

MOJO_CAPI void*
mojo_module_factory( DynamicLibrarySP library )
{
	return new PortaudioAudioDriverModule( library );
}

MOJO_END_NAMESPACE
