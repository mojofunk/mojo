#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class AudioDriverModule : public Module
{
public: // ctors
	AudioDriverModule( DynamicLibrarySP );

	virtual ~AudioDriverModule();

public: // Module interface
	virtual std::string get_author() = 0;

	virtual std::string get_description() = 0;

	virtual std::string get_version() = 0;

public: // AudioDriverModule Interface
	virtual AudioDriverSP create_driver() const = 0;

protected: // ctors
	AudioDriverModule();
};

MOJO_DEFINE_ALL_ALIASES( AudioDriverModule );

MOJO_END_NAMESPACE