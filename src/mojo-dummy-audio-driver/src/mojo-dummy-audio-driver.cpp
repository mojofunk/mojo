#include "mojo-dummy-audio-driver.hpp"

#include <adt.hpp>

#include "mojo-dummy-audio-driver/dummy_audio_device.cpp"
#include "mojo-dummy-audio-driver/dummy_audio_driver.cpp"
#include "mojo-dummy-audio-driver/dummy_audio_driver_error.cpp"
#include "mojo-dummy-audio-driver/dummy_audio_driver_module.cpp"