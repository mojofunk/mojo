#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class AudioFile
{
public: // ctors
	virtual ~AudioFile();

public: // types
	enum class OpenMode
	{
		READ,
		WRITE,
		READ_WRITE
	};

public: // API
	virtual result<void> open( OpenMode mode ) = 0;

	/**
	 * Open with READ_WRITE and fail to create if file exists.
	 */
	virtual result<void>
	create( AudioFileFormatSP, double sample_rate, uint32_t channels ) = 0;

	virtual bool is_open() const = 0;

	virtual bool close() = 0;

	virtual AudioFileFormatSP format() const = 0;

	virtual int64_t read_samples( float* buf, int64_t sample_count ) = 0;

	virtual int64_t write_samples( float* buf, int64_t sample_count ) = 0;

	/**
	 * @return position in samples from the start of the file
	 * @param sample_offset A sample offset from the start of the file
	 */
	virtual int64_t seek( int64_t sample_offset ) = 0;

	/**
	 * @return The total number of samples in the audio file
	 */
	virtual int64_t samples() const = 0;

	virtual double sample_rate() const = 0;

	virtual uint32_t channels() const = 0;

protected:
	AudioFile();
};

MOJO_DEFINE_ALL_ALIASES( AudioFile );

MOJO_END_NAMESPACE
