if(WIN32)
  find_path(MOJO_WINMM_INCLUDE_DIR timeapi.h)
  find_library(MOJO_WINMM_LIBRARIES Winmm)
endif()

if(MOJO_BUILD_TESTS)
  find_package(GTest REQUIRED)
endif()

if(NOT TARGET juce::juce_core)
  find_package(JUCE CONFIG)

  if(NOT JUCE_FOUND)
    #include(MojoFetchContentJUCE)
  endif()
endif()