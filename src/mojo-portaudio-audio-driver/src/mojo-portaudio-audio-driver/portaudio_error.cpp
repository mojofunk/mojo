MOJO_BEGIN_NAMESPACE

const char*
PortaudioErrorCategory::name() const noexcept
{
	return "PortaudioError";
}

std::string
PortaudioErrorCategory::message( int portaudio_error_value ) const noexcept
{
	return Pa_GetErrorText( portaudio_error_value );
}

bool
PortaudioErrorCategory::equivalent(
    int portaudio_error_value,
    const std::error_condition& condition ) const noexcept
{
	switch( static_cast<PortaudioError>( portaudio_error_value ) )
	{
	case PortaudioError::DEVICE_UNVAILABLE:
		return condition ==
		       make_error_condition( AudioDriverError::DEVICE_NOT_AVAILABLE );
	case PortaudioError::BAD_IO_DEVICE_COMBINATION:
		return condition ==
		       make_error_condition( AudioDriverError::DEVICE_IO_NOT_SUPPORTED );
	case PortaudioError::INVALID_CHANNEL_COUNT:
		return condition ==
		       make_error_condition( AudioDriverError::CHANNEL_COUNT_NOT_SUPPORTED );
	case PortaudioError::INVALID_SAMPLE_RATE:
		return condition ==
		       make_error_condition( AudioDriverError::SAMPLE_RATE_NOT_SUPPORTED );
	case PortaudioError::BUFFER_TOO_BIG:
		return condition ==
		       make_error_condition( AudioDriverError::BUFFER_SIZE_NOT_SUPPORTED );
	case PortaudioError::BUFFER_TOO_SMALL:
		return condition ==
		       make_error_condition( AudioDriverError::BUFFER_SIZE_NOT_SUPPORTED );
	case PortaudioError::BUFFER_OUTPUT_UNDERFLOWED:
		return condition == make_error_condition( AudioDriverError::BUFFER_UNDERRUN );
	case PortaudioError::BUFFER_INPUT_OVERFLOWED:
		return condition == make_error_condition( AudioDriverError::BUFFER_OVERRUN );
	default:
		return false;
	}
}

const std::error_category&
portaudio_error_category() noexcept
{
	static PortaudioErrorCategory category;
	return category;
}

std::error_code
make_error_code( PortaudioError portaudio_error ) noexcept
{
	return std::error_code( static_cast<int>( portaudio_error ),
	                        portaudio_error_category() );
}

MOJO_END_NAMESPACE