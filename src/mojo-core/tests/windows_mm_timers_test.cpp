#include "mojo_test_includes.hpp"

#include <windows.h>

#undef min
#undef max

namespace test
{

struct Timing
{
	uint32_t min_elapsed = std::numeric_limits<uint32_t>::max();
	uint32_t max_elapsed = std::numeric_limits<uint32_t>::min();
	uint32_t avg_elapsed = 0;
};

static Timing
get_tgt_timing()
{
	Timing timing;

	uint32_t count = 64;
	uint32_t total_elapsed = 0;

	uint32_t last_time_ms = MmTimers::get_time_ms();
	for( uint32_t i = 0; i < count; )
	{
		uint32_t current_time_ms = MmTimers::get_time_ms();
		if( current_time_ms == last_time_ms ) continue;
		uint32_t elapsed = current_time_ms - last_time_ms;
		timing.min_elapsed = std::min( timing.min_elapsed, elapsed );
		timing.max_elapsed = std::max( timing.max_elapsed, elapsed );
		total_elapsed += elapsed;
		last_time_ms = current_time_ms;
		++i;
	}
	timing.avg_elapsed = total_elapsed / count;

	return timing;
}

static Timing
get_sleep_timing()
{
	Timing timing;

	uint32_t count = 64;
	uint32_t total_elapsed = 0;

	uint32_t last_time_ms = MmTimers::get_time_ms();
	for( uint32_t i = 0; i < count; ++i )
	{
		Sleep( 1 );
		uint32_t current_time_ms = MmTimers::get_time_ms();
		uint32_t elapsed = current_time_ms - last_time_ms;
		timing.min_elapsed = std::min( timing.min_elapsed, elapsed );
		timing.max_elapsed = std::max( timing.max_elapsed, elapsed );
		total_elapsed += elapsed;
		last_time_ms = current_time_ms;
	}
	// the rounding here doesn't matter, we aren't interested in
	// accurate measurements
	timing.avg_elapsed = total_elapsed / count;

	return timing;
}

} // namespace test

TEST( MmTimers, i_can_set_minimum_timer_resolution )
{
	auto min_resolution_result = MmTimers::get_min_resolution();

	ASSERT_TRUE( min_resolution_result );
	EXPECT_TRUE( min_resolution_result.value() == 1 );

	auto timing = test::get_tgt_timing();

	A_LOG_DATA3(
	    LOG::Test, timing.min_elapsed, timing.max_elapsed, timing.avg_elapsed );

	timing = test::get_sleep_timing();

	ASSERT_TRUE( MmTimers::set_min_resolution() );

	timing = test::get_tgt_timing();

	A_LOG_DATA3(
	    LOG::Test, timing.min_elapsed, timing.max_elapsed, timing.avg_elapsed );

	// test that it the avg granularity is the same as minimum resolution
	EXPECT_TRUE( timing.avg_elapsed == 1 );

	timing = test::get_sleep_timing();

	// In a heavily loaded system and without running this test with raised
	// scheduling priority we can't assume that the sleep granularity is the
	// same as the minimum timer resolution so give it 1ms of slack, if it is
	// greater than that then there likely is a problem that needs
	// investigating.
	EXPECT_TRUE( timing.avg_elapsed <= 2 );

	EXPECT_TRUE( MmTimers::reset_resolution() );

	// You can't test setting the max timer resolution because AFAIR Windows
	// will use the minimum requested resolution of all the applications on the
	// system.
}