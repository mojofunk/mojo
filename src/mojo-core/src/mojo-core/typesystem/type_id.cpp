#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

struct TypeIDPrivate
{
	using StringPoolType = StringPool<std::string>;

	static const char* const get_id_str_ptr( const char* const c_str )
	{
		static StringPoolType s_pool;
		static std::mutex s_pool_mutex;
		std::unique_lock lock( s_pool_mutex );
		return s_pool.emplace_string( c_str );
	}
};

TypeID::TypeID( const char* const str ) noexcept
    : m_name( TypeIDPrivate::get_id_str_ptr( str ) )
{
}

TypeID::TypeID( const std::string& str ) noexcept
    : m_name( TypeIDPrivate::get_id_str_ptr( str.c_str() ) )
{
}

MOJO_END_NAMESPACE
