#include "mojo-core.hpp"

#include "mojo-core-private.hpp"

#include "mojo-core/build/source_includes.hpp"

// Now include all source files

#include "mojo-core/debug/assert.cpp"

#include "mojo-core/threads/run_loop.cpp"
#include "mojo-core/threads/thread_pool.cpp"

#include "mojo-core/string/split_string.cpp"
#include "mojo-core/string/string_conversion.cpp"
#include "mojo-core/string/string_conversion_error.cpp"

#include "mojo-core/filesystem/file_utils.cpp"
#include "mojo-core/filesystem/search_path.cpp"
#include "mojo-core/filesystem/special_paths.cpp"

#include "mojo-core/system/environment.cpp"
#include "mojo-core/system/system_resources.cpp"

#include "mojo-core/modules/dynamic_library.cpp"
#include "mojo-core/modules/module.cpp"
#include "mojo-core/modules/module_manager.cpp"

#include "mojo-core/audio_driver/audio_device.cpp"
#include "mojo-core/audio_driver/audio_driver.cpp"
#include "mojo-core/audio_driver/audio_driver_error.cpp"
#include "mojo-core/audio_driver/audio_driver_module.cpp"

#include "mojo-core/audio_file/audio_file.cpp"
#include "mojo-core/audio_file/audio_file_error.cpp"
#include "mojo-core/audio_file/audio_file_format.cpp"
#include "mojo-core/audio_file/audio_file_module.cpp"

#include "mojo-core/object/property_id.cpp"

#include "mojo-core/typesystem/type_id.cpp"
#include "mojo-core/typesystem/type_system.cpp"

#ifdef MOJO_WINDOWS
#include "mojo-core/system/windows_mmcss.cpp"

#include "mojo-core/system/windows_mm_timers.cpp"

#include "mojo-core/system/windows_qpc_timer.cpp"
#endif

#if 0
#include "mojo-core/audio/utils.cpp"

#include "mojo-core/midi/midi_util.cpp"

include "mojo-core/init/initialize.cpp"

#include "mojo-core/time/time.cpp"
#include "mojo-core/time/timing.cpp"

#include "mojo-core/memory/garbage_collector.cpp"

#include "mojo-core/misc/functor_dispatcher.cpp"
#include "mojo-core/misc/sample_block.cpp"
#include "mojo-core/misc/semaphore.cpp"

#include "mojo-core/system/resource.cpp"
#include "mojo-core/system/utils.cpp"

#if defined( MOJO_WINDOWS )
#include "mojo-core/native/windows_source_includes.hpp"

#include "mojo-core/native/windows_cpu_id.cpp"
#include "mojo-core/native/windows_mmcss.cpp"
#include "mojo-core/native/windows_timer_utils.cpp"
#elif defined( MOJO_LINUX )
#include "mojo-core/native/linux_cpu_id.cpp"

#endif

#endif
