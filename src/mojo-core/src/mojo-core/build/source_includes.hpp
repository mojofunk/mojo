#pragma once

// Keep this at the top to ensure it doesn't depend on any of the
// includes being listed below.
#include "mojo-core-private.hpp"

// Standard C++ headers
#include <atomic>
#include <condition_variable>
#include <iomanip>
#include <mutex>
#include <sstream>

// Library Dependencies
#include <adt.hpp>

#include <juce_core/juce_core.h>

#if MOJO_WINDOWS

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <windows.h>

#include <timeapi.h>

#endif // MOJO_WINDOWS