#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class AudioFileModule : public Module
{
public: // constructors
	AudioFileModule( DynamicLibrarySP );
	~AudioFileModule();

public: // interface
	/**
	 * @return The audio file formats that are readable by the
	 * plugin.
	 */
	virtual AudioFileFormatSPSet get_readable_formats() const = 0;

	/**
	 * @return The audio file formats that are writable by the plugin
	 */
	virtual AudioFileFormatSPSet get_writable_formats() const = 0;

	/**
	 * @return new AudioFile or nullptr on failure
	 */
	virtual AudioFileSP make_audio_file( fs::path const& path ) = 0;
};

MOJO_DEFINE_ALL_ALIASES( AudioFileModule );

MOJO_END_NAMESPACE
