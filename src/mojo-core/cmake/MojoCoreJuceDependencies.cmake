if(NOT TARGET juce::juce_core)
  find_package(JUCE CONFIG)

  if(NOT JUCE_FOUND)
    message( FATAL "JUCE is required to build mojo-core")
    #include(MojoFetchContentJUCE)
  endif()
endif()

find_package(ZLIB)

if(ZLIB_FOUND)
  set(JUCE_INCLUDE_ZLIB_CODE 0)
  set(JUCE_ZLIB_INCLUDE_PATH ${ZLIB_INCLUDE_DIRS})
  message( "JUCE is using external zlib at ${ZLIB_INCLUDE_DIRS}")
else()
  set(JUCE_INCLUDE_ZLIB_CODE 1)
endif()
find_package(CURL)

if(CURL_FOUND)
  set(JUCE_USE_CURL TRUE)
endif()