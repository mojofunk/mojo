#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace
{

#ifdef MOJO_WINDOWS
const char path_delimiter = ';';
#else
const char path_delimiter = ':';
#endif
} // namespace

Searchpath::Searchpath() {}

Searchpath::Searchpath( const std::string& path )
{
	auto const paths = split_string( path, path_delimiter );

	for( auto const& p : paths )
	{
		add_directory( p );
	}
}

Searchpath::Searchpath( const fs::path& path )
{
	add_directory( path );
}

Searchpath::Searchpath( const std::vector<fs::path>& paths )
{
	std::copy( paths.begin(), paths.end(), std::back_inserter( m_dirs ) );
}

Searchpath::Searchpath( const std::vector<std::string>& paths )
{
	std::copy( paths.begin(), paths.end(), std::back_inserter( m_dirs ) );
}

Searchpath::Searchpath( const Searchpath& other )
    : m_dirs( other.m_dirs )
{
}

void
Searchpath::add_directory( const fs::path& directory_path )
{
	// test for existance and warn etc?
	m_dirs.push_back( directory_path );
}

const std::string
Searchpath::to_string() const
{
	std::string path;

	for( const_iterator i = m_dirs.begin(); i != m_dirs.end(); ++i )
	{
		path += ( *i ).string();
		path += path_delimiter;
	}

	path = path.substr( 0, path.length() - 1 ); // drop final separator

	return path;
}

Searchpath&
Searchpath::operator=( const Searchpath& path )
{
	m_dirs = path.m_dirs;
	return *this;
}

Searchpath&
Searchpath::operator+=( const Searchpath& spath )
{
	m_dirs.insert( m_dirs.end(), spath.m_dirs.begin(), spath.m_dirs.end() );
	return *this;
}

Searchpath&
Searchpath::operator+=( const fs::path& directory_path )
{
	add_directory( directory_path );
	return *this;
}

const Searchpath
Searchpath::operator+( const fs::path& directory_path )
{
	return Searchpath( *this ) += directory_path;
}

const Searchpath
Searchpath::operator+( const Searchpath& spath )
{
	return Searchpath( *this ) += spath;
}

Searchpath&
Searchpath::add_subdirectory_to_paths( const std::string& subdir )
{
	std::vector<fs::path> tmp;
	std::string directory_path;

	for( iterator i = m_dirs.begin(); i != m_dirs.end(); ++i )
	{
		// should these new paths just be added to the end of
		// the search path rather than replace?
		*i /= subdir;
	}

	return *this;
}

Searchpath&
Searchpath::operator/( const std::string& subdir )
{
	return add_subdirectory_to_paths( subdir );
}

MOJO_END_NAMESPACE
