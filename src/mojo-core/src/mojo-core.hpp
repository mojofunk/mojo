#ifndef MOJO_CORE_H
#define MOJO_CORE_H

#include "mojo-core/build/header_includes.hpp"

#include "mojo-core/debug/assert.hpp"

#include "mojo-core/outcome/outcome.hpp"

#include "mojo-core/macros/declare_class_and_aliases.hpp"
#include "mojo-core/macros/disallow_copy_and_assign.hpp"
#include "mojo-core/macros/namespace.hpp"
#include "mojo-core/macros/private.hpp"
#include "mojo-core/macros/smart_pointer_aliases.hpp"

#include "mojo-core/outcome/outcome_namespace.hpp"

#include "mojo-core/threads/run_loop.hpp"
#include "mojo-core/threads/thread_pool.hpp"

#include "mojo-core/filesystem/filesystem_alias.hpp"

#include "mojo-core/filesystem/file_utils.hpp"
#include "mojo-core/filesystem/search_path.hpp"
#include "mojo-core/filesystem/special_paths.hpp"

#include "mojo-core/string/printf_format.hpp"
#include "mojo-core/string/scanf_format.hpp"
#include "mojo-core/string/split_string.hpp"
#include "mojo-core/string/string_conversion.hpp"
#include "mojo-core/string/string_conversion_error.hpp"
#include "mojo-core/string/string_pool.hpp"

#include "mojo-core/system/environment.hpp"
#include "mojo-core/system/system_resources.hpp"

#include "mojo-core/modules/dynamic_library.hpp"
#include "mojo-core/modules/module.hpp"
#include "mojo-core/modules/module_manager.hpp"

#include "mojo-core/audio_driver/audio_driver_error.hpp"

#include "mojo-core/audio_driver/audio_device.hpp"
#include "mojo-core/audio_driver/audio_driver.hpp"
#include "mojo-core/audio_driver/audio_driver_module.hpp"

#include "mojo-core/audio_file/audio_file_format.hpp"

#include "mojo-core/audio_file/audio_file.hpp"
#include "mojo-core/audio_file/audio_file_error.hpp"
#include "mojo-core/audio_file/audio_file_module.hpp"

#include "mojo-core/object/CallerId.hpp"

#include "mojo-core/object/CallbackList.hpp"

#include "mojo-core/object/object.hpp"
#include "mojo-core/object/property_id.hpp"
#include "mojo-core/object/variant.hpp"

#include "mojo-core/typesystem/type_id.hpp"

#include "mojo-core/typesystem/type_factory.hpp"

#include "mojo-core/typesystem/template_type_factory.hpp"
#include "mojo-core/typesystem/type_registry.hpp"
#include "mojo-core/typesystem/type_system.hpp"

#ifdef MOJO_WINDOWS
#include "mojo-core/system/windows_mmcss.hpp"

#include "mojo-core/system/windows_mm_timers.hpp"
#include "mojo-core/system/windows_qpc_timer.hpp"
#endif

#if 0
#include "mojo-core/audio/cycle_timer.hpp"
#include "mojo-core/audio/timeline.hpp"
#include "mojo-core/audio/types.hpp"
#include "mojo-core/audio/utils.hpp"

#include "mojo-core/midi/midi_util.hpp"

#include "mojo-core/init/initialize.hpp"

#include "mojo-core/object/change.hpp"
#include "mojo-core/object/change_set.hpp"
#include "mojo-core/object/properties.hpp"
#include "mojo-core/object/property.hpp"
#include "mojo-core/object/signals.hpp"

#include "mojo-core/time/chrono_timestamp.hpp"
#include "mojo-core/time/elapsed_timer.hpp"
#include "mojo-core/time/time.hpp"
#include "mojo-core/time/timing.hpp"

#include "mojo-core/memory/fixed_size_object_pool.hpp"
#include "mojo-core/memory/fixed_size_pool.hpp"
#include "mojo-core/memory/fixed_size_pool_allocator.hpp"
#include "mojo-core/memory/garbage_collector.hpp"
#include "mojo-core/memory/null_deleter.hpp"

#include "mojo-core/lockfree/ring_buffer.hpp"

#include "mojo-core/misc/functor_dispatcher.hpp"
#include "mojo-core/misc/sample_block.hpp"
#include "mojo-core/misc/semaphore.hpp"
#include "mojo-core/misc/spinwait.hpp"

#include "mojo-core/system/cpu_id.hpp"

#ifdef MOJO_WINDOWS
#include "mojo-core/native/windows_timer_utils.hpp"
#endif

#endif

#endif // MOJO_CORE_H
