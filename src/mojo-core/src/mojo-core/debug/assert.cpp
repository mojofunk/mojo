#include "mojo-core/build/source_includes.hpp"

// If not overridden by the build system set all these as enabled.

#ifndef MOJO_ASSERT_PRINT_ENABLED
#define MOJO_ASSERT_PRINT_ENABLED 1
#endif

#ifndef MOJO_ASSERT_LOGGING_ENABLED
#define MOJO_ASSERT_LOGGING_ENABLED 1
#endif

#ifndef MOJO_ASSERT_BREAK_ENABLED
#define MOJO_ASSERT_BREAK_ENABLED 1
#endif

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( Assert, "mojo::Assert" )
}

MOJO_END_NAMESPACE

void
mojo_assertion_failure( const char* expression,
                        const char* file,
                        int line,
                        const char* message )
{
#if MOJO_ASSERT_PRINT_ENABLED || MOJO_ASSERT_LOGGING_ENABLED
	char buffer[1024];
	std::snprintf( buffer,
	               sizeof( buffer ),
	               "Assertion failure: '%s' at %s:%d%s%s\n",
	               expression,
	               file,
	               line,
	               message ? " with message: " : "",
	               message ? message : "" );
#endif

#if MOJO_ASSERT_PRINT_ENABLED
#if defined( MOJO_WINDOWS )
	OutputDebugStringA( buffer );
#else
	std::fputs( buffer, stderr );
#endif
#endif

#if MOJO_ASSERT_LOGGING_ENABLED
	A_LOG_MSG( mojo::log::Assert, buffer );
#endif

#if MOJO_ASSERT_BREAK_ENABLED
	mojo_debug_break();
#endif
}

void
mojo_debug_break()
{
	JUCE_BREAK_IN_DEBUGGER;
}