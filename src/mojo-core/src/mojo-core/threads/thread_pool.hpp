
#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

M_DECLARE_CLASS_AND_POINTER_ALIASES( ThreadPoolPrivate );

// The ThreadPool class is a thin wrapper around a vector of
// std::thread. The size of the pool is fixed and does not change at
// runtime.
//
// The threads in ThreadPool must be started explicitly using
// ThreadPool::start().
//
// The threads in ThreadPool can be stopped explicitly using
// ThreadPool::stop() but will also be stopped in the ThreadPool dtor
//
// is_running(), start() and stop() must be called by a single control thread.
//
// All threads in the ThreadPool should run at the same priority, which
// can be set in the ThreadStartedCallback using platform
// specific/native API.
class MOJO_API ThreadPool
{
public:
	using ThreadFunction = std::function<void()>;
	using ThreadStartedCallback = std::function<void()>;

	enum class StartOption
	{
		NO_WAIT,
		WAIT,
	};

public:
	ThreadPool( const std::string& pool_name,
	            std::size_t count,
	            ThreadFunction thread_function,
	            ThreadStartedCallback thread_started_callback );

	~ThreadPool();

	bool is_running() const;

	bool start( StartOption wait = StartOption::NO_WAIT );

	bool stop();

	std::size_t thread_count() const;

private:
	M_DECLARE_PRIVATE( ThreadPool );

	ThreadPoolPrivateUP d_ptr;
};

MOJO_END_NAMESPACE