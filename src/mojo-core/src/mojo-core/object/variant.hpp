#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

using Variant = std::variant<int32_t,
                             uint32_t,
                             int64_t,
                             uint64_t,
                             float,
                             double,
                             std::string,
                             std::weak_ptr<Object>,
                             std::vector<std::weak_ptr<Object>>>;

MOJO_END_NAMESPACE