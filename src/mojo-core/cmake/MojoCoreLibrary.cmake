set(MOJO_CORE_TARGET mojo-core-${MOJO_VERSION_MAJOR})

file(GLOB MOJO_CORE_PUBLIC_HEADER "src/mojo-core.hpp")
file(GLOB MOJO_CORE_PUBLIC_SUBHEADERS "src/mojo-core/*/*.h*")
file(GLOB MOJO_CORE_PUBLIC_WINDOWS_SUBHEADERS "src/mojo-core/*/windows_*.h*")

list(FILTER MOJO_CORE_PUBLIC_SUBHEADERS EXCLUDE REGEX "src/mojo-core/*/*_p.h*")
list(FILTER MOJO_CORE_PUBLIC_SUBHEADERS EXCLUDE REGEX
     "src/mojo-core/*/windows_*.h*")

file(GLOB MOJO_CORE_PRIVATE_HEADER "src/mojo-core/mojo-core-private.hpp")
file(GLOB MOJO_CORE_PRIVATE_SUBHEADERS "src/mojo-core/*/*_p.h*")

file(GLOB MOJO_CORE_PUBLIC_SOURCES "src/mojo-core/*/*.cpp")
file(GLOB MOJO_CORE_PUBLIC_WINDOWS_SOURCES "src/mojo-core/*/windows_*.cpp")

list(FILTER MOJO_CORE_PUBLIC_SOURCES EXCLUDE REGEX "src/mojo-core/*/*_p.cpp")
list(FILTER MOJO_CORE_PUBLIC_SOURCES EXCLUDE REGEX
     "src/mojo-core/*/windows_*.cpp")

file(GLOB MOJO_CORE_PRIVATE_SOURCES "src/mojo-core/*/*_p.cpp")

file(GLOB MOJO_CORE_UNITY_SOURCE "src/mojo-core.cpp")
file(GLOB MOJO_CORE_PRIVATE_UNITY_SOURCE "src/mojo-core-private.cpp")

set(MOJO_CORE_SOURCES
    "${MOJO_CORE_PUBLIC_HEADER}" "${MOJO_CORE_PUBLIC_SUBHEADERS}"
    "${MOJO_CORE_PRIVATE_HEADER}" "${MOJO_CORE_PRIVATE_SUBHEADERS}")

if(MOJO_BUILD_UNITY)
  list(APPEND MOJO_CORE_SOURCES "${MOJO_CORE_UNITY_SOURCE}"
       "${MOJO_CORE_PRIVATE_UNITY_SOURCE}")
else()
  list(APPEND MOJO_CORE_SOURCES "${MOJO_CORE_PUBLIC_SOURCES}"
       "${MOJO_CORE_PRIVATE_SOURCES}")
  if(WIN32)
    list(APPEND MOJO_CORE_SOURCES "${MOJO_CORE_PUBLIC_WINDOWS_SUBHEADERS}"
         "${MOJO_CORE_PUBLIC_WINDOWS_SOURCES}")
  endif()
endif()

add_library(${MOJO_CORE_TARGET} SHARED ${MOJO_CORE_SOURCES})

add_library(${MOJO_NS}::${MOJO_CORE_TARGET} ALIAS ${MOJO_CORE_TARGET})

mojo_set_target_cxx_properties(${MOJO_CORE_TARGET})

target_compile_options(${MOJO_CORE_TARGET} PRIVATE ${MOJO_WARNINGS})

if(MSVC)
  # Disable a compiler warning related to exporting a class when building a dll
  # on Windows so it can be used by clients.
  #
  # An example of the warning: Warning C4251 'mojo::Library::d_ptr': class
  # 'std::unique_ptr<mojo::LibraryPrivate,std::default_delete<_Ty>>' needs to
  # have dll-interface to be used by clients of class 'mojo::Library'
  #
  # In all cases these are private PIMPL members that will never be required to
  # be exported and accessed via external code.
  target_compile_options(${MOJO_CORE_TARGET} PUBLIC /wd4251)

  # Disable warning related to a non-dll-interface class being used as a base
  # class for a dll-interface class.
  #
  # An example of the warning: Warning C4275 non dll-interface class
  # 'std::error_category' used as base for dll-interface class
  # 'mojo::AudioDriverErrorCategory' Documentation on MSDN suggests ignore the
  # error for types defined in the standard C++ library:
  #
  # "C4275 can be ignored in Visual C++ if you are deriving from a type in the
  # Standard C++ Library, compiling a debug release (/MTd) and where the
  # compiler error message refers to _Container_base."
  #
  # Other suggested fixes are to inline the class.
  target_compile_options(${MOJO_CORE_TARGET} PUBLIC /wd4275)
endif()

set_target_properties(
  ${MOJO_CORE_TARGET} PROPERTIES VERSION ${MOJO_VERSION} SOVERSION
                                                         ${MOJO_VERSION_MAJOR})

target_include_directories(
  ${MOJO_CORE_TARGET}
  PUBLIC $<INSTALL_INTERFACE:include/${MOJO_NS}>
         $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
  PRIVATE # For config.h that isn't public, see below
          ${CMAKE_CURRENT_BINARY_DIR})

if(WIN32)
  target_include_directories(${MOJO_CORE_TARGET}
                             PRIVATE ${MOJO_WINMM_INCLUDE_DIR})
  target_link_libraries(${MOJO_CORE_TARGET} PRIVATE ${MOJO_WINMM_LIBRARIES})
endif()

target_compile_definitions(${MOJO_CORE_TARGET} PRIVATE "MOJO_BUILDING_DLL")

if(MSVC)
  message("MSVC detected, exporting all symbols from DLL")
  set_target_properties(${MOJO_CORE_TARGET}
                        PROPERTIES WINDOWS_EXPORT_ALL_SYMBOLS true)
endif()

# target_compile_definitions(${MOJO_CORE_TARGET} PRIVATE "HAVE_CONFIG_H")

if(CMAKE_COMPILER_IS_GNUCC AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 8)
  message("Explicit linking to stdc++fs is required for gcc version 8.")
  target_link_libraries(${MOJO_CORE_TARGET} PUBLIC stdc++fs)
endif()

target_link_libraries(${MOJO_CORE_TARGET} PRIVATE adt-0::adt-0
                                                  ${MOJO_CORE_JUCE_TARGET})
