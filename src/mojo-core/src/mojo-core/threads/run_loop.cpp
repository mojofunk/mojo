#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

class RunLoopPrivate
{
	M_DECLARE_PUBLIC( RunLoop );

public:
	/**
	 * Create a new RunLoop, run() must be called to start
	 * the main loop.
	 */
	RunLoopPrivate( RunLoop* q )
	    : q_ptr( q )
	    , m_running( false )
	    , m_iter_count( 0 )
	{
	}

	~RunLoopPrivate() {}

public: // member functions
	/**
	 * Start the loop, this will block until quit is called
	 */
	void run()
	{
		// DEBUG("RunLoopPrivate::run()\n");

		m_running = true;

		while( can_run() )
		{

			{
				std::unique_lock<std::mutex> lock( m_iter_mtx );

				while( m_iter_count < 1 )
				{
					m_cond.wait( lock );
				}

				// TODO just use atomic compare and exchange?
				while( --m_iter_count > 0 )
				{
				}
			}

			// DEBUG("RunLoopPrivate::run() : calling on_iteration()\n");

			q_ptr->on_iteration();

			{
				std::unique_lock<std::mutex> lock( m_iter_mtx );

				m_cond.notify_one();
			}
		}
	}

	void quit( bool block = false )
	{
		if( !m_running ) return;

		std::unique_lock<std::mutex> lock( m_iter_mtx );

		m_running = false;

		schedule_iteration();

		if( block )
		{
			// wait for the iteration to complete
			// which can only happen when m_iter_mtx
			// is released in the wait.
			// DEBUG("RunLoopPrivate::quit() : Waiting for thread to quit\n");
			m_cond.wait( lock );
		}
	}

	void wait_for_iteration()
	{
		//	DEBUG("RunLoop::wait_for_iteration\n");

		++m_iter_count;

		std::unique_lock<std::mutex> lock( m_iter_mtx );

		m_cond.notify_one();

		// wait for one iteration to complete
		m_cond.wait( lock );
	}

	void schedule_iteration()
	{
		//	ADT_DEBUG("RunLoop::schedule_iteration\n");

		++m_iter_count;

		m_cond.notify_one();
	}

	bool running() const { return m_running; }

private: // member functions
	/**
	 * \return true if thread is able to run.
	 */
	bool can_run()
	{
		std::unique_lock<std::mutex> lock( m_iter_mtx );

		if( !m_running )
		{
			m_cond.notify_one();
			return false;
		}
		return true;
	}

private: // member functions
	RunLoop* const q_ptr;

	std::mutex m_iter_mtx;

	std::condition_variable m_cond;

	std::atomic<bool> m_running;

	std::atomic<int> m_iter_count;

private:
	A_DECLARE_CLASS_MEMBERS( RunLoopPrivate )
};

A_DEFINE_CLASS_AS_MEMBERS( RunLoopPrivate, "mojo::RunLoop" )

RunLoop::RunLoop()
    : d_ptr( new RunLoopPrivate( this ) )
{
}

RunLoop::~RunLoop() {}

void
RunLoop::run()
{
	d_ptr->run();
}

void
RunLoop::quit( bool block )
{
	d_ptr->quit( block );
}

void
RunLoop::wait_for_iteration()
{
	d_ptr->wait_for_iteration();
}

void
RunLoop::schedule_iteration()
{
	d_ptr->schedule_iteration();
}

bool
RunLoop::running() const
{
	return d_ptr->running();
}

MOJO_END_NAMESPACE
