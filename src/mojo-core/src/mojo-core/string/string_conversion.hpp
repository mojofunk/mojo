#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

MOJO_API
result<void>
bool_to_string( bool value, std::string& str );

MOJO_API
result<void>
int16_to_string( int16_t value, std::string& str );

MOJO_API
result<void>
uint16_to_string( uint16_t value, std::string& str );

MOJO_API
result<void>
int32_to_string( int32_t value, std::string& str );

MOJO_API
result<void>
uint32_to_string( uint32_t value, std::string& str );

MOJO_API
result<void>
int64_to_string( int64_t value, std::string& str );

MOJO_API
result<void>
uint64_to_string( uint64_t value, std::string& str );

MOJO_API
result<void>
float_to_string( float value, std::string& str );

MOJO_API
result<void>
double_to_string( double value, std::string& str );

///
/// to_string with conversion error handling
///
template <class T>
inline result<void>
to_string( T value, std::string& str )
{
	// This will cause a compile time error if this function is ever
	// instantiated, which is useful to catch unintended conversions
	typename T::TO_STRING_TEMPLATE_NOT_DEFINED_FOR_THIS_TYPE invalid_type;
	return failure();
}

template <class T>
inline result<void>
to_string( bool value, std::string& str )
{
	return bool_to_string( value, str );
}

template <class T>
inline result<void>
to_string( int8_t value, std::string& str )
{
	return int16_to_string( value, str );
}

template <class T>
inline result<void>
to_string( uint8_t value, std::string& str )
{
	return uint16_to_string( value, str );
}

template <class T>
inline result<void>
to_string( int16_t value, std::string& str )
{
	return int16_to_string( value, str );
}

template <class T>
inline result<void>
to_string( uint16_t value, std::string& str )
{
	return uint16_to_string( value, str );
}

template <class T>
inline result<void>
to_string( int32_t value, std::string& str )
{
	return int32_to_string( value, str );
}

template <class T>
inline result<void>
to_string( uint32_t value, std::string& str )
{
	return uint32_to_string( value, str );
}

template <class T>
inline result<void>
to_string( int64_t value, std::string& str )
{
	return int64_to_string( value, str );
}

template <class T>
inline result<void>
to_string( uint64_t value, std::string& str )
{
	return uint64_to_string( value, str );
}

template <class T>
inline result<void>
to_string( float value, std::string& str )
{
	return float_to_string( value, str );
}

template <class T>
inline result<void>
to_string( double value, std::string& str )
{
	return double_to_string( value, str );
}

///
/// to_string variation that disregards conversion errors
///
template <class T>
inline std::string
to_string( T value )
{
	// This will cause a compile time error if this function is ever
	// instantiated, which is useful to catch unintended conversions
	typename T::TO_STRING_TEMPLATE_NOT_DEFINED_FOR_THIS_TYPE invalid_type;
	return std::string();
}

template <>
inline std::string
to_string( bool value )
{
	std::string tmp;
	auto result = bool_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( int8_t value )
{
	std::string tmp;
	auto result = int16_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( uint8_t value )
{
	std::string tmp;
	auto result = uint16_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( int16_t value )
{
	std::string tmp;
	auto result = int16_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( uint16_t value )
{
	std::string tmp;
	auto result = uint16_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( int32_t value )
{
	std::string tmp;
	auto result = int32_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( uint32_t value )
{
	std::string tmp;
	auto result = uint32_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( int64_t value )
{
	std::string tmp;
	auto result = int64_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( uint64_t value )
{
	std::string tmp;
	auto result = uint64_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( float value )
{
	std::string tmp;
	auto result = float_to_string( value, tmp );
	return tmp;
}

template <>
inline std::string
to_string( double value )
{
	std::string tmp;
	auto result = double_to_string( value, tmp );
	return tmp;
}

///
/// string_to_* API
///

MOJO_API result<void>
string_to_bool( const std::string& str, bool& val );

MOJO_API result<void>
string_to_int16( const std::string& str, int16_t& val );

MOJO_API result<void>
string_to_uint16( const std::string& str, uint16_t& val );

MOJO_API result<void>
string_to_int32( const std::string& str, int32_t& val );

MOJO_API result<void>
string_to_uint32( const std::string& str, uint32_t& val );

MOJO_API result<void>
string_to_int64( const std::string& str, int64_t& val );

MOJO_API result<void>
string_to_uint64( const std::string& str, uint64_t& val );

MOJO_API result<void>
string_to_float( const std::string& str, float& val );

MOJO_API result<void>
string_to_double( const std::string& str, double& val );

///
/// string_to with error handling
///
template <class T>
inline result<void>
string_to( const std::string& str, T& val )
{
	// This will cause a compile time error if this function is ever
	// instantiated, which is useful to catch unintended conversions
	typename T::TO_STRING_TEMPLATE_NOT_DEFINED_FOR_THIS_TYPE invalid_type;
	return failure();
}

template <class T>
inline result<void>
string_to( const std::string& str, bool& val )
{
	return string_to_bool( str, val );
}

template <class T>
inline result<void>
string_to( const std::string& str, int8_t& value )
{
	int16_t tmp = value;
	auto result = string_to_int16( str, tmp );
	if( !result )
	{
		return result;
	}
	value = tmp;
	return success();
}

template <class T>
inline result<void>
string_to( const std::string& str, uint8_t& value )
{
	uint16_t tmp = value;
	auto result = string_to_uint16( str, tmp );
	if( !result )
	{
		return result;
	}
	value = tmp;
	return success();
}

template <class T>
inline result<void>
string_to( const std::string& str, int16_t& value )
{
	return string_to_int16( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, uint16_t& value )
{
	return string_to_uint16( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, int32_t& value )
{
	return string_to_int32( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, uint32_t& value )
{
	return string_to_uint32( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, int64_t& value )
{
	return string_to_int64( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, uint64_t& value )
{
	return string_to_uint64( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, float& value )
{
	return string_to_float( str, value );
}

template <class T>
inline result<void>
string_to( const std::string& str, double& value )
{
	return string_to_double( str, value );
}

///
/// string_to variation that disregards conversion errors
///
template <class T>
inline T
string_to( const std::string& str )
{
	// This will cause a compile time error if this function is ever
	// instantiated, which is useful to catch unintended conversions
	typename T::STRING_TO_TEMPLATE_NOT_DEFINED_FOR_THIS_TYPE invalid_type;
	return T();
}

template <>
inline bool
string_to( const std::string& str )
{
	bool tmp;
	auto result = string_to_bool( str, tmp );
	return tmp;
}

template <>
inline int8_t
string_to( const std::string& str )
{
	int16_t tmp;
	auto result = string_to_int16( str, tmp );
	return static_cast<int8_t>( tmp );
}

template <>
inline uint8_t
string_to( const std::string& str )
{
	uint16_t tmp;
	auto result = string_to_uint16( str, tmp );
	return static_cast<uint8_t>( tmp );
}

template <>
inline int16_t
string_to( const std::string& str )
{
	int16_t tmp;
	auto result = string_to_int16( str, tmp );
	return tmp;
}

template <>
inline uint16_t
string_to( const std::string& str )
{
	uint16_t tmp;
	auto result = string_to_uint16( str, tmp );
	return tmp;
}

template <>
inline int32_t
string_to( const std::string& str )
{
	int32_t tmp;
	auto result = string_to_int32( str, tmp );
	return tmp;
}

template <>
inline uint32_t
string_to( const std::string& str )
{
	uint32_t tmp;
	auto result = string_to_uint32( str, tmp );
	return tmp;
}

template <>
inline int64_t
string_to( const std::string& str )
{
	int64_t tmp;
	auto result = string_to_int64( str, tmp );
	return tmp;
}

template <>
inline uint64_t
string_to( const std::string& str )
{
	uint64_t tmp;
	auto result = string_to_uint64( str, tmp );
	return tmp;
}

template <>
inline float
string_to( const std::string& str )
{
	float tmp;
	auto result = string_to_float( str, tmp );
	return tmp;
}

template <>
inline double
string_to( const std::string& str )
{
	double tmp;
	auto result = string_to_double( str, tmp );
	return tmp;
}

MOJO_END_NAMESPACE