#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class TypeSystem final
{
public:
	static void register_type_factory( TypeFactorySP factory );

	template <class T>
	static void register_default_type_factory( const char* const c_str )
	{
		register_type_factory( TypeFactorySP( new TemplateTypeFactory<T>( c_str ) ) );
	}

	/**
	 * because std::type_info.name() is not portable between compilers
	 * etc, use a standard mapping of names to types.
	 */
	static TypeID get_type_id( const std::type_info& info );

	template <class T>
	static TypeID get_type_id( const T& )
	{
		return get_type_id( typeid( T ) );
	}

	/**
	 * create an instance of the type .
	 */
	static std::any create( TypeID const& type_id );
};

MOJO_END_NAMESPACE
