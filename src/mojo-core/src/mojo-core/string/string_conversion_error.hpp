#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

enum class StringConversionError
{
	SUCCESS = 0,
	EMPTY_STRING = 1,
	ILLEGAL_CHAR = 2, // rename
	TOO_LONG = 3,     // rename
	VALUE_UNDERFLOW = 4,
	VALUE_OVERFLOW = 5,
	UNKNOWN = 6,
};

class MOJO_API StringConversionErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override final;
	std::string message( int error_value ) const noexcept override final;
};

MOJO_API const std::error_category&
string_conversion_error_category() noexcept;

MOJO_API std::error_condition
    make_error_condition( StringConversionError ) noexcept;

inline std::error_code
make_error_code( StringConversionError err )
{
	return { static_cast<int>( err ), string_conversion_error_category() };
}

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::StringConversionError> : true_type
{
};
} // namespace std
