#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

AudioFileModule::AudioFileModule( DynamicLibrarySP library )
    : Module( library )
{
}

AudioFileModule::~AudioFileModule() {}

MOJO_END_NAMESPACE