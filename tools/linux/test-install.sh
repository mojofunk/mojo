#! /bin/bash

. env.sh

# To install to a temporary directory for testing etc use
# $ DESTDIR=./tmp ./cmake-install.sh
# otherwise use sudo to install system wide
export DESTDIR=./tmp

cd $BUILD_DIR && ninja install
