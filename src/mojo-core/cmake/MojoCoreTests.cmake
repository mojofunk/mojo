enable_testing()

set(MOJO_COMMON_TEST_SOURCES tests/gtest_main.cpp)

if(MSVC)
  # remove dll-interface warnings related to gtest...which are hopefully not
  # relevant
  add_definitions(/wd4251 /wd4275)
endif()

set(MOJO_TEST_LIBRARIES GTest::GTest adt-test-0::adt-test-0
                        ${MOJO_NS}::${MOJO_CORE_TARGET})

add_library(MOJO_COMMON_TEST_OBJECTS OBJECT ${MOJO_COMMON_TEST_SOURCES})
target_link_libraries(MOJO_COMMON_TEST_OBJECTS PRIVATE ${MOJO_TEST_LIBRARIES})
target_compile_options(MOJO_COMMON_TEST_OBJECTS PRIVATE ${MOJO_WARNINGS})
target_precompile_headers(MOJO_COMMON_TEST_OBJECTS PRIVATE
                          tests/mojo_test_includes.hpp)

file(GLOB MOJO_TEST_SOURCES "tests/*_test.cpp")

if(NOT WIN32)
  list(FILTER MOJO_TEST_SOURCES EXCLUDE REGEX "tests/windows_*_test.cpp")
endif()

if(MOJO_BUILD_SINGLE_TESTS)
  foreach(TEST_SOURCE ${MOJO_TEST_SOURCES})
    get_filename_component(TEST_NAME ${TEST_SOURCE} NAME_WE)
    string(REPLACE "_" "-" TEST_NAME ${TEST_NAME})
    set(TEST_NAME mojo-${TEST_NAME})
    add_executable(${TEST_NAME} ${TEST_SOURCE}
                                $<TARGET_OBJECTS:MOJO_COMMON_TEST_OBJECTS>)
    target_link_libraries(${TEST_NAME} PRIVATE ${MOJO_TEST_LIBRARIES})
    target_compile_options(${TEST_NAME} PRIVATE ${MOJO_WARNINGS})
    target_precompile_headers(${TEST_NAME} REUSE_FROM MOJO_COMMON_TEST_OBJECTS)
    add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})
  endforeach()
endif()

set(MOJO_TESTS_TARGET "mojo-core-tests")
add_executable(${MOJO_TESTS_TARGET} ${MOJO_TEST_SOURCES}
                                    $<TARGET_OBJECTS:MOJO_COMMON_TEST_OBJECTS>)
target_link_libraries(${MOJO_TESTS_TARGET} PRIVATE ${MOJO_TEST_LIBRARIES})
target_compile_options(${MOJO_TESTS_TARGET} PRIVATE ${MOJO_WARNINGS})
target_precompile_headers(${MOJO_TESTS_TARGET} REUSE_FROM
                          MOJO_COMMON_TEST_OBJECTS)
add_test(NAME ${MOJO_TESTS_TARGET} COMMAND ${MOJO_TESTS_TARGET})

if(MOJO_ENABLE_COVERAGE)
  setup_target_for_coverage_gcovr(
    NAME ${MOJO_TESTS_TARGET}-coverage EXECUTABLE ${MOJO_TESTS_TARGET}
    DEPENDENCIES ${MOJO_TESTS_TARGET})
endif()
