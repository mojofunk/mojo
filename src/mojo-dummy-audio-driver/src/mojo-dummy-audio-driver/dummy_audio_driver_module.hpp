MOJO_BEGIN_NAMESPACE

class DummyAudioDriverModule : public AudioDriverModule
{
public: // constructors
	DummyAudioDriverModule( DynamicLibrarySP );
	~DummyAudioDriverModule();

public: // Module interface
	std::string get_author() override;

	std::string get_description() override;

	std::string get_version() override;

public: // AudioDriverModule interface
	AudioDriverSP create_driver() const;
};

MOJO_END_NAMESPACE