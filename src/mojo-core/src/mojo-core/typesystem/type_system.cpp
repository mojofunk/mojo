#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

class TypeSystemPrivate
{
public: // Instance Access
	// TODO use Singleton/macro
	static TypeSystemPrivate* get_instance()
	{
		static TypeSystemPrivate* s_instance = new TypeSystemPrivate();
		return s_instance;
	}

public: // API
	std::any create( const TypeID& type_id )
	{
		std::lock_guard<std::mutex> lg( m_mutex );

		auto const iter = m_type_factories.find( type_id );

		if( iter != m_type_factories.end() )
		{
			return ( *iter ).second->create();
		}
		return std::any();
	}

	void register_type_factory( TypeFactorySP tf )
	{
		std::lock_guard<std::mutex> lg( m_mutex );

		m_type_registry.set_type_id( tf->type_info(), tf->type_id() );
		m_type_factories.insert( std::make_pair( tf->type_id(), tf ) );
	}

	TypeID get_type_id( const std::type_info& info ) const
	{
		std::lock_guard<std::mutex> lg( m_mutex );

		return m_type_registry.get_type_id( info );
	}

private: // Ctors
	TypeSystemPrivate()
	{
		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<int32_t>( "int32_t" ) ) );

		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<uint32_t>( "uint32_t" ) ) );

		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<int64_t>( "int64_t" ) ) );

		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<uint64_t>( "uint64_t" ) ) );

		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<float>( "float" ) ) );

		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<double>( "double" ) ) );

		register_type_factory(
		    TypeFactorySP( new TemplateTypeFactory<std::string>( "std::string" ) ) );
	}

private: // Member Data
	TypeRegistry m_type_registry;

	using TypeFactories = std::map<mojo::TypeID, TypeFactorySP>;
	TypeFactories m_type_factories;
	mutable std::mutex m_mutex;
};

void
TypeSystem::register_type_factory( TypeFactorySP factory )
{
	TypeSystemPrivate::get_instance()->register_type_factory( factory );
}

TypeID
TypeSystem::get_type_id( const std::type_info& info )
{
	return TypeSystemPrivate::get_instance()->get_type_id( info );
}

std::any
TypeSystem::create( TypeID const& type_id )
{
	return TypeSystemPrivate::get_instance()->create( type_id );
}

MOJO_END_NAMESPACE