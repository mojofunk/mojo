#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

const char*
AudioFileErrorCategory::name() const noexcept
{
	return "AudioFileError";
}

std::string
AudioFileErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<AudioFileError>( error_value ) )
	{
	case AudioFileError::UNREGOGNISED_FORMAT:
		return "Unrecognised format";
	case AudioFileError::SYSTEM:
		return "System";
	case AudioFileError::MALFORMED_FILE:
		return "Malformed file";
	case AudioFileError::UNSUPPORTED_ENCODING:
		return "Unsupported encoding";
	default:
		return "Unknown AudioFile error";
	}
}

const std::error_category&
audio_file_error_category() noexcept
{
	static AudioFileErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( AudioFileError afe ) noexcept
{
	return std::error_condition( static_cast<int>( afe ),
	                             audio_file_error_category() );
}

MOJO_END_NAMESPACE
