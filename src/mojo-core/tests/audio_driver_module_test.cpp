#include "mojo_test_includes.hpp"

namespace
{
float left_phase( 0 );
float right_phase( 0 );
} // namespace

AudioDevice::CallbackResult
audio_callback( const float* input_buffer,
                const float* output_buffer,
                int64_t samples,
                void* )
{
	A_LOG_CALL1( LOG::Test, samples );

	float* out = (float*)output_buffer;

	unsigned int i;
	(void)input_buffer;
	for( i = 0; i < samples; i++ )
	{
		*out++ = left_phase;
		*out++ = right_phase;
		/* Generate simple sawtooth phaser that ranges between -1.0 and 1.0. */
		left_phase += 0.01f;
		/* When signal reaches top, drop back down. */
		if( left_phase >= 1.0f ) left_phase -= 2.0f;
		/* higher pitch so we can distinguish left and right. */
		right_phase += 0.03f;
		if( right_phase >= 1.0f ) right_phase -= 2.0f;
	}
	return AudioDevice::CONTINUE;
}

void
test_device( AudioDeviceSP dev )
{
	ASSERT_TRUE( dev );

	uint32_t buffer_size = 1024;

	if( dev->max_output_channels() < 2 )
	{
		A_LOG_MSG( LOG::Test,
		           A_FMT( "Device {} has {} outputs, at least 2 required for tests",
		                  dev->get_name(),
		                  dev->max_output_channels() ) );
		return;
	}

	auto open_result = dev->open( dev->max_input_channels(),
	                              dev->max_output_channels(),
	                              dev->get_default_sample_rate(),
	                              buffer_size,
	                              audio_callback,
	                              NULL );

	if( !open_result )
	{
		return;
	}

	EXPECT_TRUE( dev->is_open() );

	std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

	const auto start_result = dev->start();

	if( !start_result )
	{
		return;
	}

	std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

	EXPECT_TRUE( dev->is_active() );

	std::this_thread::sleep_for( std::chrono::seconds( 1 ) );

	A_LOG_DATA3( LOG::Test,
	             dev->get_input_latency(),
	             dev->get_output_latency(),
	             dev->get_cpu_load() );

	EXPECT_TRUE( dev->stop() );

	auto is_stopped_result = dev->is_stopped();

	EXPECT_TRUE( is_stopped_result );

	EXPECT_TRUE( is_stopped_result.value() );

	EXPECT_TRUE( dev->close() );

	auto is_open_result = dev->is_open();

	EXPECT_TRUE( is_open_result );

	const bool device_is_open = is_open_result.value();
	EXPECT_FALSE( device_is_open );
}

void
log_device_info( AudioDeviceSP dev )
{
	ASSERT_TRUE( dev );

	A_LOG_DATA4( LOG::Test,
	             dev->get_name(),
	             dev->max_input_channels(),
	             dev->max_output_channels(),
	             dev->get_default_sample_rate() );

	auto const rates = dev->get_supported_sample_rates();

	std::ostringstream sample_rates;
	for( auto const& rate : rates )
	{
		sample_rates << " " << rate;
	}
	A_LOG_MSG( LOG::Test,
	           A_FMT( "Supported Samplerates: {}", sample_rates.str() ) );
}

void
test_audio_driver_module( AudioDriverModuleSP mod )
{
	ASSERT_TRUE( mod );

	A_LOG_DATA3(
	    LOG::Test, mod->get_author(), mod->get_description(), mod->get_version() );

	AudioDriverSP driver = mod->create_driver();
	AudioDeviceSPSet devices = driver->get_devices();

	ASSERT_TRUE( !devices.empty() );

	for_each( devices.begin(), devices.end(), log_device_info );
	for_each( devices.begin(), devices.end(), test_device );
}

TEST( AudioDriverModule, ForEachOpenDefaultRate )
{
	auto modules = ModuleManager::get_modules_of_type<AudioDriverModule>();

	ASSERT_TRUE( !modules.empty() );

	std::for_each( modules.begin(), modules.end(), test_audio_driver_module );
}
