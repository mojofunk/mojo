message(
  "------------------------------------------------------------------------")
message("")
message("Configuration Summary - mojo")
message("")

if(MOJO_MASTER_PROJECT)
  include(FeatureSummary)
  feature_summary(
    WHAT ALL
    DESCRIPTION "mojo Features:"
    VAR allFeaturesText)
  message(STATUS "${allFeaturesText}")

  # cmake info
  message(STATUS "CMAKE_BINARY_DIR:        ${CMAKE_BINARY_DIR}")
  message(STATUS "CMAKE_INSTALL_PREFIX:    ${CMAKE_INSTALL_PREFIX}")
  message(STATUS "CMAKE_SYSTEM_NAME:       ${CMAKE_SYSTEM_NAME}")
  message(STATUS "CMAKE_SYSTEM_VERSION:    ${CMAKE_SYSTEM_VERSION}")
  message(STATUS "CMAKE_SYSTEM_PROCESSOR:  ${CMAKE_SYSTEM_PROCESSOR}")
  message(STATUS "CMAKE_C_COMPILER:        ${CMAKE_C_COMPILER}")
  message(STATUS "CMAKE_CXX_COMPILER:      ${CMAKE_CXX_COMPILER}")
  message(STATUS "CMAKE_BUILD_TYPE:        ${CMAKE_BUILD_TYPE}")
  message("")
endif()

# mojo specific
message(STATUS "MOJO_VERSION:                 ${MOJO_VERSION}")
message(STATUS "MOJO_BUILD_SHARED_LIBS:       ${MOJO_BUILD_SHARED_LIBS}")
message(STATUS "MOJO_BUILD_UNITY:             ${MOJO_BUILD_UNITY}")
message(STATUS "MOJO_BUILD_TESTS:             ${MOJO_BUILD_TESTS}")
message(STATUS "MOJO_SINGLE_TESTS:            ${MOJO_BUILD_SINGLE_TESTS}")
message(STATUS "MOJO_WARNINGS_AS_ERRORS       ${MOJO_WARNINGS_AS_ERRORS}")
message(STATUS "MOJO_INSTALL_HEADERS:         ${MOJO_INSTALL_HEADERS}")
message(STATUS "MOJO_INSTALL_PKGCONFIG:       ${MOJO_INSTALL_PKGCONFIG}")
message(STATUS "MOJO_INSTALL_CMAKECONFIG:     ${MOJO_INSTALL_CMAKECONFIG}")
message(STATUS "MOJO_ENABLE_COVERAGE:         ${MOJO_ENABLE_COVERAGE}")

message("")
message(
  "------------------------------------------------------------------------")
