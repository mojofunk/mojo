#include "mojo_test_includes.hpp"

TEST( StringPool, StdString )
{
	StringPool<std::string> string_pool;

	const char* const string_ptr1 = string_pool.emplace_string( "Hello World\n" );
	const char* const string_ptr2 = string_pool.emplace_string( "Hello World\n" );

	ASSERT_TRUE( string_ptr1 == string_ptr2 );

	const char* const string_ptr3 =
	    string_pool.emplace_string( std::string( "Hello World\n" ) );

	ASSERT_TRUE( string_ptr2 == string_ptr3 );

	std::string tmp = "Hello World\n";

	const char* const string_ptr4 = string_pool.emplace_string( tmp );

	ASSERT_TRUE( string_ptr3 == string_ptr4 );
}