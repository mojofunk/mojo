MOJO_BEGIN_NAMESPACE

class SndfileAudioFileModule : public AudioFileModule
{
public: // constructors
	SndfileAudioFileModule( DynamicLibrarySP );
	~SndfileAudioFileModule();

public: // Module interface
	std::string get_author() override;

	std::string get_description() override;

	std::string get_version() override;

public: // AudioFileModule interface
	AudioFileFormatSPSet get_readable_formats() const override;

	AudioFileFormatSPSet get_writable_formats() const override;

	/**
	 * Create an AudioFile instance.
	 */
	AudioFileSP make_audio_file( fs::path const& path ) override;

private: // member data
	AudioFileFormatSPSet m_readable_formats;

	AudioFileFormatSPSet m_writable_formats;

private: // member functions
	static void get_readable_formats( AudioFileFormatSPSet& );

	static void get_writable_formats( AudioFileFormatSPSet& );
};

MOJO_DEFINE_ALL_ALIASES( SndfileAudioFileModule );

MOJO_END_NAMESPACE