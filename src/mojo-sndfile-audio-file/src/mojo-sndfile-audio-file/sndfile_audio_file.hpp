MOJO_BEGIN_NAMESPACE

class SndfileAudioFile : public AudioFile
{
public:
	SndfileAudioFile( const fs::path& path );

	~SndfileAudioFile() override;

public:
	result<void> open( OpenMode ) override;

	result<void>
	create( AudioFileFormatSP, double sample_rate, uint32_t channels ) override;

	bool is_open() const override;

	bool close() override;

	AudioFileFormatSP format() const override;

	int64_t read_samples( float* buf, int64_t sample_count ) override;

	int64_t write_samples( float* buf, int64_t sample_count ) override;

	int64_t seek( int64_t sample_offset ) override;

	int64_t samples() const override;

	double sample_rate() const override;

	uint32_t channels() const override;

private:
	int open_mode_to_sf_mode( OpenMode open_mode );

private:
	const fs::path m_path;

	SNDFILE* m_sf;
	SF_INFO m_info;

	AudioFileFormatSP m_format;

private:
	A_DECLARE_CLASS_MEMBERS( SndfileAudioFile )
};

MOJO_DEFINE_ALL_ALIASES( SndfileAudioFile );

MOJO_END_NAMESPACE
