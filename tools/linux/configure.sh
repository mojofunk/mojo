#! /bin/bash

. env.sh

rm -rf $BUILD_DIR

mkdir $BUILD_DIR && cd $BUILD_DIR

cmake -GNinja $BUILD_OPTIONS "$@" $TOP_DIR
