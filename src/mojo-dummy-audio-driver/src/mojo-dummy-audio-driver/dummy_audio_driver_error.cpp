MOJO_BEGIN_NAMESPACE

const char*
DummyAudioDriverErrorCategory::name() const noexcept
{
	return "DummyAudioDriverError";
}

std::string
DummyAudioDriverErrorCategory::message( int dummy_error_value ) const noexcept
{
	switch( static_cast<DummyAudioDriverError>( dummy_error_value ) )
	{
	case DummyAudioDriverError::BUFFER_UNDERRUN:
		return AudioDriverErrorCategory::message(
		    static_cast<int>( AudioDriverError::BUFFER_UNDERRUN ) );
	case DummyAudioDriverError::BUFFER_OVERRUN:
		return AudioDriverErrorCategory::message(
		    static_cast<int>( AudioDriverError::BUFFER_OVERRUN ) );
	default:
		return {};
	}
}

bool
DummyAudioDriverErrorCategory::equivalent(
    int dummy_error_value,
    const std::error_condition& condition ) const noexcept
{
	switch( static_cast<DummyAudioDriverError>( dummy_error_value ) )
	{
	case DummyAudioDriverError::BUFFER_UNDERRUN:
		return condition == make_error_condition( AudioDriverError::BUFFER_UNDERRUN );
	case DummyAudioDriverError::BUFFER_OVERRUN:
		return condition == make_error_condition( AudioDriverError::BUFFER_OVERRUN );
	default:
		return false;
	}
}

const std::error_category&
dummy_audio_driver_error_category() noexcept
{
	static DummyAudioDriverErrorCategory category;
	return category;
}

std::error_code
make_error_code( DummyAudioDriverError dummy_error ) noexcept
{
	return std::error_code( static_cast<int>( dummy_error ),
	                        dummy_audio_driver_error_category() );
}

MOJO_END_NAMESPACE