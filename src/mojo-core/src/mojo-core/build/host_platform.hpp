#ifndef MOJO_CORE_HOST_PLATFORM_H
#define MOJO_CORE_HOST_PLATFORM_H

/**
 * Allow force override of host platform using MOJO_hostname define.
 */
#if defined( MOJO_WINDOWS ) || defined( _WIN32 )
#undef MOJO_WINDOWS
#define MOJO_WINDOWS 1
#ifdef _MSC_VER
#define MOJO_MSVC _MSC_VER
#endif
#elif defined( MOJO_LINUX ) || defined( __linux__ )
#undef MOJO_LINUX
#define MOJO_LINUX 1
#elif defined( MOJO_MAC ) || defined( __APPLE__ )
#undef MOJO_MAC
#define MOJO_MAC 1
#else
#warning "Unsupported target platform"
#endif

// bsd?
// android?

#endif // MOJO_CORE_HOST_PLATFORM_H
