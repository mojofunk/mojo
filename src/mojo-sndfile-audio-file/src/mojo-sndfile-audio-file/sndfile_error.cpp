MOJO_BEGIN_NAMESPACE

const char*
SndfileErrorCategory::name() const noexcept
{
	return "SndfileError";
}

std::string
SndfileErrorCategory::message( int sndfile_error_value ) const noexcept
{
	// The enum values defined in SndfileError are equivalent to the unnamed error
	// enum in sndfile so they can be past directly.
	return sf_error_number( sndfile_error_value );
}

bool
SndfileErrorCategory::equivalent(
    int sndfile_error_value,
    const std::error_condition& condition ) const noexcept
{
	switch( static_cast<SndfileError>( sndfile_error_value ) )
	{
	case SndfileError::UNREGOGNISED_FORMAT:
		return condition ==
		       make_error_condition( AudioFileError::UNREGOGNISED_FORMAT );
	case SndfileError::SYSTEM:
		return condition == make_error_condition( AudioFileError::SYSTEM );
	case SndfileError::MALFORMED_FILE:
		return condition == make_error_condition( AudioFileError::MALFORMED_FILE );
	case SndfileError::UNSUPPORTED_ENCODING:
		return condition ==
		       make_error_condition( AudioFileError::UNSUPPORTED_ENCODING );
	default:
		return false;
	}
}

const std::error_category&
sndfile_error_category() noexcept
{
	static SndfileErrorCategory category;
	return category;
}

std::error_code
make_error_code( SndfileError sndfile_error ) noexcept
{
	return std::error_code( static_cast<int>( sndfile_error ),
	                        sndfile_error_category() );
}

MOJO_END_NAMESPACE