#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class AudioDevice
{
public: // ctors
	virtual ~AudioDevice();

public: // interface
	// TODO enum class
	enum CallbackResult
	{
		CONTINUE = 0,
		ABORT
	};

	using Callback = CallbackResult( const float* input_buffer,
	                                 const float* output_buffer,
	                                 int64_t samples,
	                                 void* user_data );

	/// @return The name of the device. unique??
	virtual std::string get_name() const = 0;

	virtual result<void> open( uint32_t input_channels,
	                           uint32_t output_channels,
	                           double sample_rate,
	                           uint32_t buffer_size,
	                           Callback* cb,
	                           void* user_data ) = 0;

	virtual result<bool> is_open() const = 0;

	virtual result<void> start() = 0;

	virtual result<bool> is_active() const = 0;

	virtual result<void> stop() = 0;

	virtual result<bool> is_stopped() const = 0;

	virtual result<void> abort() = 0;

	virtual result<void> close() = 0;

	virtual uint32_t max_input_channels() const = 0;

	virtual uint32_t max_output_channels() const = 0;

	virtual double get_default_sample_rate() const = 0;

	virtual std::vector<double> get_supported_sample_rates() const = 0;

	// virtual uint32_t get_default_buffersize() const = 0;

	// virtual void get_supported_buffer_sizes(std::vector<uint32_t>&) const = 0;

	virtual double get_input_latency() const = 0;

	virtual double get_output_latency() const = 0;

	virtual double get_current_sample_rate() const = 0;

	virtual double get_cpu_load() const = 0;

	// virtual get_min_buffer_size () const;
	// virtual get_max_buffer_size () const;
};

MOJO_DEFINE_ALL_ALIASES( AudioDevice );

MOJO_END_NAMESPACE