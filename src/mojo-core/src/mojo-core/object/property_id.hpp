#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class PropertyID final
{
public:
	PropertyID( const char* const ) noexcept;

	PropertyID( const std::string& str ) noexcept;

	PropertyID( const PropertyID& other ) noexcept = default;

	inline bool operator==( const PropertyID& other ) const
	{
		return m_name.data() == other.m_name.data();
	}

	inline bool operator!=( const PropertyID& other ) const
	{
		return m_name.data() != other.m_name.data();
	}

	std::string_view get_str() { return m_name; }

private:
	std::string_view m_name;
};

MOJO_END_NAMESPACE