#include "mojo_test_includes.hpp"

TEST( ModuleManager, ModuleDirectoryPaths )
{
	auto const paths = ModuleManager::get_default_directory_paths();

	ASSERT_TRUE( !paths.empty() );

	for( auto const& p : paths )
	{
		A_LOG_DATA1( LOG::Test, p.u8string() );
		ASSERT_TRUE( fs::is_directory( p ) );
	}
}

TEST( ModuleManager, GetModules )
{
	auto const modules = ModuleManager::get_modules();

	for( auto module : modules )
	{
		ASSERT_TRUE( module );
		ASSERT_TRUE( !module->get_author().empty() );
		ASSERT_TRUE( !module->get_description().empty() );
		ASSERT_TRUE( !module->get_version().empty() );

		A_LOG_DATA1( LOG::Test, module->get_author() );
		A_LOG_DATA1( LOG::Test, module->get_description() );
		A_LOG_DATA1( LOG::Test, module->get_version() );
	}
}

TEST( ModuleManager, AudioDriverModule )
{
	auto audio_driver_modules =
	    ModuleManager::get_modules_of_type<AudioDriverModule>();

	for( auto& audio_driver_module : audio_driver_modules )
	{
		auto driver = audio_driver_module->create_driver();
		ASSERT_TRUE( driver );
	}
}
