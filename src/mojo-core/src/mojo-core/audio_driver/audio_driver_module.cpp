#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

AudioDriverModule::AudioDriverModule( DynamicLibrarySP library )
    : Module( library )
{
}

AudioDriverModule::~AudioDriverModule() {}

MOJO_END_NAMESPACE