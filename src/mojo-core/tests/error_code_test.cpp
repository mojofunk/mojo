#include "mojo_test_includes.hpp"

enum class MIDIFileError
{
	UNREGOGNISED_FORMAT = 1,
	SYSTEM,
	MALFORMED_FILE,
	UNSUPPORTED_ENCODING
};

namespace std
{
template <>
struct is_error_condition_enum<MIDIFileError> : true_type
{
};
} // namespace std

class MIDIFileErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override;
	std::string message( int error_value ) const noexcept override;
};

const std::error_category&
midi_file_error_category();

std::error_condition make_error_condition( MIDIFileError ) noexcept;

const char*
MIDIFileErrorCategory::name() const noexcept
{
	return "MIDIFile";
}

std::string
MIDIFileErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<MIDIFileError>( error_value ) )
	{
	case MIDIFileError::UNREGOGNISED_FORMAT:
		return "Unrecognised format";
	case MIDIFileError::SYSTEM:
		return "System";
	case MIDIFileError::MALFORMED_FILE:
		return "Malformed file";
	case MIDIFileError::UNSUPPORTED_ENCODING:
		return "Unsupported encoding";
	default:
		return "Unknown MIDIFile error";
	}
}

const std::error_category&
midi_file_error_category()
{
	static MIDIFileErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( MIDIFileError mfe ) noexcept
{
	return std::error_condition( static_cast<int>( mfe ),
	                             midi_file_error_category() );
}

class MIDIFile
{
protected:
	virtual ~MIDIFile(){};

public:
	virtual bool read( std::filesystem::path const&, std::error_code& ec ) = 0;
};

enum class CAError
{
	UNREGOGNISED_FORMAT = 10,
	SYSTEM,
	MALFORMED_FILE,
	UNSUPPORTED_ENCODING
};

namespace std
{
template <>
struct is_error_code_enum<CAError> : true_type
{
};
} // namespace std

const std::error_category&
ca_midi_file_error_category();

std::error_code
make_error_code( CAError ca_error ) noexcept
{
	return std::error_code( static_cast<int>( ca_error ),
	                        ca_midi_file_error_category() );
}

class CAMIDIFile : public MIDIFile
{
public:
	bool read( std::filesystem::path const&, std::error_code& ec ) override
	{
		// Fake failure for test
		ec = CAError::MALFORMED_FILE;
		return false;
	}
};

// Derive from MIDIFileErrorCategory to reuse the message strings
class CAMIDIFileErrorCategory : public MIDIFileErrorCategory
{
public:
	const char* name() const noexcept override { return "CAMIDIFileError"; }
	std::string message( int ca_error_value ) const noexcept override
	{
		// Defer messages to parent class.
		switch( static_cast<CAError>( ca_error_value ) )
		{
		case CAError::UNREGOGNISED_FORMAT:
			return MIDIFileErrorCategory::message(
			    static_cast<int>( MIDIFileError::UNREGOGNISED_FORMAT ) );
		case CAError::SYSTEM:
			return MIDIFileErrorCategory::message(
			    static_cast<int>( MIDIFileError::SYSTEM ) );
		case CAError::MALFORMED_FILE:
			return MIDIFileErrorCategory::message(
			    static_cast<int>( MIDIFileError::MALFORMED_FILE ) );
		case CAError::UNSUPPORTED_ENCODING:
			return MIDIFileErrorCategory::message(
			    static_cast<int>( MIDIFileError::UNSUPPORTED_ENCODING ) );
		default:
			return {};
		}
	}

	bool
	equivalent( int ca_error_value,
	            const std::error_condition& condition ) const noexcept override
	{
		switch( static_cast<CAError>( ca_error_value ) )
		{
		case CAError::UNREGOGNISED_FORMAT:
			return condition ==
			       make_error_condition( MIDIFileError::UNREGOGNISED_FORMAT );
		case CAError::SYSTEM:
			return condition == make_error_condition( MIDIFileError::SYSTEM );
		case CAError::MALFORMED_FILE:
			return condition == make_error_condition( MIDIFileError::MALFORMED_FILE );
		case CAError::UNSUPPORTED_ENCODING:
			return condition ==
			       make_error_condition( MIDIFileError::UNSUPPORTED_ENCODING );
		default:
			return false;
		}
	}
};

const std::error_category&
ca_midi_file_error_category()
{
	static CAMIDIFileErrorCategory category;
	return category;
}

enum class SMFError
{
	UNREGOGNISED_FORMAT = 100,
	SYSTEM,
	MALFORMED_FILE,
	UNSUPPORTED_ENCODING
};

namespace std
{
template <>
struct is_error_code_enum<SMFError> : true_type
{
};
} // namespace std

const std::error_category&
smf_midi_file_error_category();

std::error_code
make_error_code( SMFError smf_error ) noexcept
{
	return std::error_code( static_cast<int>( smf_error ),
	                        smf_midi_file_error_category() );
}

class SMFMIDIFile : public MIDIFile
{
public:
	virtual bool read( std::filesystem::path const&, std::error_code& ec ) override
	{
		// Fake failure for test
		ec = SMFError::MALFORMED_FILE;
		return false;
	}
};

class SMFMIDIFileErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override { return "SMFMIDIFileError"; }
	std::string message( int ) const noexcept override
	{
		// We may want to use more specific error messages provided by the library
		// rather than the generic ones defined in MIDIFileErrorCategory::message()
		// return smf_strerror(smf_error_value);
		return "SMFAPIERROR";
	}

	bool
	equivalent( int smf_error_value,
	            const std::error_condition& condition ) const noexcept override
	{
		switch( static_cast<SMFError>( smf_error_value ) )
		{
		case SMFError::UNREGOGNISED_FORMAT:
			return condition ==
			       make_error_condition( MIDIFileError::UNREGOGNISED_FORMAT );
		case SMFError::SYSTEM:
			return condition == make_error_condition( MIDIFileError::SYSTEM );
		case SMFError::MALFORMED_FILE:
			return condition == make_error_condition( MIDIFileError::MALFORMED_FILE );
		case SMFError::UNSUPPORTED_ENCODING:
			return condition ==
			       make_error_condition( MIDIFileError::UNSUPPORTED_ENCODING );
		default:
			return false;
		}
	}
};

const std::error_category&
smf_midi_file_error_category()
{
	static SMFMIDIFileErrorCategory category;
	return category;
}

TEST( ErrorCode, ErrorConditionEquivalence )
{
	CAMIDIFile ca_midi_file;
	SMFMIDIFile smf_midi_file;

	std::error_code ca_error_code;
	std::error_code smf_error_code;

	// Fake read failure with two different MIDI file API's
	ASSERT_FALSE( ca_midi_file.read( "dummy_path", ca_error_code ) );
	ASSERT_FALSE( smf_midi_file.read( "dummy_path", smf_error_code ) );

	// These are different error codes
	ASSERT_FALSE( ca_error_code == smf_error_code );

	// With different values
	ASSERT_TRUE( ca_error_code.value() != smf_error_code.value() );

	// But they both indicate the same error condition
	ASSERT_TRUE( ca_error_code == MIDIFileError::MALFORMED_FILE );
	ASSERT_TRUE( smf_error_code == MIDIFileError::MALFORMED_FILE );

	ASSERT_TRUE( ca_error_code.category().name() ==
	             ca_midi_file_error_category().name() );

	ASSERT_TRUE( smf_error_code.category().name() ==
	             smf_midi_file_error_category().name() );
}