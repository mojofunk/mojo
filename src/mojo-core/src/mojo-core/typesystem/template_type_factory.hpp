#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

template <class T>
class TemplateTypeFactory : public TypeFactory
{
public:
	TemplateTypeFactory( const TypeID type_id )
	    : m_type_id( type_id )
	{
	}

	virtual TypeID type_id() const override { return m_type_id; }

	virtual std::any create() const override { return std::any( new T ); }

	virtual std::type_info const& type_info() const override
	{
		return typeid( T );
	}

private:
	const TypeID m_type_id;
};

MOJO_END_NAMESPACE
