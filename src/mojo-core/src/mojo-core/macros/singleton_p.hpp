#pragma once

#include "mojo-core-private.hpp"

MOJO_BEGIN_NAMESPACE

template <class Type>
class Singleton
{
	friend Type* Type::get_instance();

	static Type* get()
	{
		static Type* instance = initialize();
		return instance;
	}

	static Type* initialize()
	{
		Type* new_type = new Type();
		AtExitManager::on_exit( &Singleton<Type>::on_exit );
		return new_type;
	}

	static void on_exit() { delete get(); }
};

MOJO_END_NAMESPACE

#define M_SINGLETON( ClassName )                                               \
	M_DISALLOW_COPY_AND_ASSIGN( ClassName );                                      \
	friend class mojo::Singleton<ClassName>;                                      \
	static ClassName* get_instance()
