#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

Module::Module( DynamicLibrarySP library )
    : m_library( library )
{
}

Module::~Module() {}

MOJO_END_NAMESPACE