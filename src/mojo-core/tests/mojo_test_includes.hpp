#ifndef MOJO_TEST_INCLUDES_H
#define MOJO_TEST_INCLUDES_H

#include <gtest/gtest.h>

#include <adt-test.hpp>

#include <mojo-core.hpp>

#include <chrono>
#include <thread>

namespace LOG
{
A_DECLARE_LOG_CATEGORY( Test )
}

using namespace mojo;

#endif
