# mojo Core

# Public Source files:

## Headers

1. Use #pragma as an include guard
2. Include the module header. e.g. mojo-core.hpp for mojo-core or in
   private headers, include the private module header e.g.
   mojo-core-private.hpp
3. Add the header to the module header that was included (e.g
   mojo-core.hpp)
4. Add MOJO_BEGIN/END_NAMESPACE macros and write new code in the
   namespace.

## Source

A public implementation/source file must:

1. Include the module source include header e.g
   mojo-core/build/source_includes.hpp or
   mojo-core/source_includes_p.hpp for private source files (not currently needed).
2. Add MOJO_BEGIN_NAMESPACE/MOJO_END_NAMESPACE macros and write implementation between them.

# mojo Modules

