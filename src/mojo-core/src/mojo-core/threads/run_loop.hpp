#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

M_DECLARE_CLASS_AND_POINTER_ALIASES( RunLoopPrivate );

class MOJO_API RunLoop
{
public:
	/**
	 * Create a new RunLoop, run() must be called to start
	 * the main loop.
	 */
	RunLoop();

	/**
	 * assert that quit() has been called.
	 */
	virtual ~RunLoop();

	/**
	 * Start the loop, this will block until quit is called
	 */
	void run();

	void quit( bool block = false );

	void wait_for_iteration();

	void schedule_iteration();

	bool running() const;

protected:
	virtual void on_iteration() = 0;

private:
	M_DECLARE_PRIVATE( RunLoop );

	RunLoopPrivateUP d_ptr;
};

MOJO_END_NAMESPACE
