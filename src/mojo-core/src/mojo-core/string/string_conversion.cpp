#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

///
/// *_to_string API, convert numeric values to strings
///

result<void>
bool_to_string( bool value, std::string& str )
{
	if( value )
	{
		str = "1";
	}
	else
	{
		str = "0";
	}
	return success();
}

namespace
{

constexpr size_t CONVERT_BUFFER_SIZE = 32;

template <class IntType>
result<void>
int_type_to_string( IntType value, std::string& str )
{
	// TODO template for buffer size?
	char buffer[CONVERT_BUFFER_SIZE];

	int retval =
	    snprintf( buffer, sizeof( buffer ), mojo::printf_format( value ), value );

	if( retval <= 0 || retval >= (int)sizeof( buffer ) )
	{
		return failure( mojo::StringConversionError::TOO_LONG );
	}
	str = buffer;
	return success();
}

template <class IntType>
result<void>
string_to_int_type( const std::string& str, IntType& value )
{
	if( sscanf( str.c_str(), scanf_format( value ), &value ) != 1 )
	{
		return failure( StringConversionError::ILLEGAL_CHAR );
	}
	return success();
}

} // namespace

result<void>
int16_to_string( int16_t value, std::string& str )
{
	return int_type_to_string( value, str );
}

result<void>
uint16_to_string( uint16_t value, std::string& str )
{
	return int_type_to_string( value, str );
}

result<void>
int32_to_string( int32_t value, std::string& str )
{
	return int_type_to_string( value, str );
}

result<void>
uint32_to_string( uint32_t value, std::string& str )
{
	return int_type_to_string( value, str );
}

result<void>
int64_to_string( int64_t value, std::string& str )
{
	return int_type_to_string( value, str );
}

result<void>
uint64_to_string( uint64_t value, std::string& str )
{
	return int_type_to_string( value, str );
}

namespace
{

template <class FloatType>
static bool
infinity_to_string( FloatType value, std::string& str )
{
	if( value == std::numeric_limits<FloatType>::infinity() )
	{
		str = "inf";
		return true;
	}
	else if( value == -std::numeric_limits<FloatType>::infinity() )
	{
		str = "-inf";
		return true;
	}
	return false;
}

template <class FloatType>
static bool
string_to_infinity( const std::string& str, FloatType& value )
{
	std::array<char const*, 4> const pos_infinity_strings{
		"inf", "INF", "infinity", "INFINITY"
	};

	std::array<char const*, 4> const neg_infinity_strings{
		"-inf", "-INF", "-infinity", "-INFINITY"
	};

	if( std::find( pos_infinity_strings.begin(),
	               pos_infinity_strings.end(),
	               str ) != pos_infinity_strings.end() )
	{
		value = std::numeric_limits<FloatType>::infinity();
		return true;
	}

	if( std::find( neg_infinity_strings.begin(),
	               neg_infinity_strings.end(),
	               str ) != neg_infinity_strings.end() )
	{
		value = -std::numeric_limits<FloatType>::infinity();
		return true;
	}

	return false;
}

template <class FloatType>
result<void>
float_type_to_string( FloatType value, std::string& str )
{
	if( infinity_to_string( value, str ) )
	{
		return success();
	}

	auto fpc = std::fpclassify( value );

	if( fpc == FP_NAN || fpc == FP_SUBNORMAL )
	{
		return failure( StringConversionError::VALUE_UNDERFLOW );
	}

	std::stringstream ss;
	ss.imbue( std::locale::classic() );

	//	ss << std::setprecision(std::numeric_limits<FloatType>::max_digits10);

	ss << std::setprecision( 17 );

	ss << value;

	if( ss.fail() )
	{
		return failure( StringConversionError::UNKNOWN );
	}

	str = ss.str();
	return success();
}

template <class FloatType>
result<void>
string_to_float_type( const std::string& str, FloatType& value )
{
	if( string_to_infinity( str, value ) )
	{
		return success();
	}

	value = 0.0;

	// special case zero as converstion can fail overflow
	if( str == "0.0" )
	{
		return success();
	}

	std::stringstream ss;
	ss.imbue( std::locale::classic() );

	ss << str;

	if( ss >> value || !ss.eof() )
	{
		if( ss.fail() )
		{
			return failure( StringConversionError::UNKNOWN );
		}
	}

	auto fpc = std::fpclassify( value );

	if( fpc == FP_NAN || fpc == FP_SUBNORMAL )
	{
		return failure( StringConversionError::VALUE_UNDERFLOW );
	}

	// The numeric value the string contained could not be represented.
	if( value == 0.0 )
	{
		return failure( StringConversionError::VALUE_OVERFLOW );
	}

	return success();
}

} // namespace

result<void>
float_to_string( float value, std::string& str )
{
	return float_type_to_string<float>( value, str );
}

result<void>
double_to_string( double value, std::string& str )
{
	return float_type_to_string<double>( value, str );
}

///
/// string_to_* API
///

result<void>
string_to_bool( const std::string& str, bool& value )
{
	if( str.empty() )
	{
		return failure( StringConversionError::EMPTY_STRING );
	}

	auto const to_lower = []( const unsigned char i ) {
		return static_cast<unsigned char>( std::tolower( static_cast<int>( i ) ) );
	};

	auto lower_case_str = str;
	std::transform( lower_case_str.begin(),
	                lower_case_str.end(),
	                lower_case_str.begin(),
	                to_lower );

	std::array<std::string, 4> true_strings = { "1", "y", "yes", "true" };
	std::array<std::string, 4> false_strings = { "0", "n", "no", "false" };

	if( std::find( true_strings.begin(), true_strings.end(), lower_case_str ) !=
	    true_strings.end() )
	{
		value = true;
		return success();
	}

	if( std::find( false_strings.begin(), false_strings.end(), lower_case_str ) !=
	    false_strings.end() )
	{
		value = false;
		return success();
	}

	return failure( StringConversionError::ILLEGAL_CHAR );
}

result<void>
string_to_int16( const std::string& str, int16_t& value )
{
	return string_to_int_type( str, value );
}

result<void>
string_to_uint16( const std::string& str, uint16_t& value )
{
	return string_to_int_type( str, value );
}

result<void>
string_to_int32( const std::string& str, int32_t& value )
{
	return string_to_int_type( str, value );
}

result<void>
string_to_uint32( const std::string& str, uint32_t& value )
{
	return string_to_int_type( str, value );
}

result<void>
string_to_int64( const std::string& str, int64_t& value )
{
	return string_to_int_type( str, value );
}

result<void>
string_to_uint64( const std::string& str, uint64_t& value )
{
	return string_to_int_type( str, value );
}

result<void>
string_to_float( const std::string& str, float& value )
{
	return string_to_float_type( str, value );
}

result<void>
string_to_double( const std::string& str, double& value )
{
	return string_to_float_type( str, value );
}

MOJO_END_NAMESPACE