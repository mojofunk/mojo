#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class MOJO_API MmTimers
{
public:
	/**
	 * Get the minimum Multimedia Timer resolution as supported by the
	 * system.
	 *
	 * @return true if getting min timer resolution was not successful
	 */
	static result<uint32_t> get_min_resolution();

	/**
	 * Set the minimum Multimedia Timer resolution as supported by the
	 * system.
	 *
	 * @return true if setting min timer resolution was successful
	 */
	static result<void> set_min_resolution();

	/**
	 * Set current Multimedia Timer resolution. If the timer resolution
	 * has already been set then reset_resolution() must be called
	 * before set_resolution will succeed.
	 *
	 * @return true if setting the timer value was successful, false if
	 * setting the timer resolution failed or the resolution has already
	 * been set.
	 */
	static result<void> set_resolution( uint32_t timer_resolution_ms );

	/**
	 * Reset Multimedia Timer resolution. In my testing, if the timer
	 * resolution is set below the default, then resetting the
	 * resolution will not reset the timer resolution back to 15ms. At
	 * least it does not reset immediately even after calling Sleep.
	 *
	 * @return true if setting the timer value was successful
	 */
	static result<void> reset_resolution();

	/**
	 * A wrapper around timeGetTime(), so including windows.h isn't
	 * required.
	 *
	 * The value returned by timeGetTime() is a 32bit value and wraps
	 * around to 0 every 2^32 milliseconds, which is about 49.71 days.
	 *
	 * @return the system time, in milliseconds.
	 */
	static uint32_t get_time_ms();
};

enum class MmTimersError
{
	SUCCESS = 0,
	OUT_OF_RANGE,
	MISMATCHED_CALLS, //< You must match calls to timeBegin/EndPeriod with the same
	                  //	resolution
	UNKNOWN,
};

class MOJO_API MmTimersErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override final;
	std::string message( int error_value ) const noexcept override final;
};

MOJO_API const std::error_category&
mm_timers_error_category() noexcept;

MOJO_API std::error_condition make_error_condition( MmTimersError ) noexcept;

inline std::error_code
make_error_code( MmTimersError err )
{
	return { static_cast<int>( err ), mm_timers_error_category() };
}

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::MmTimersError> : true_type
{
};
} // namespace std