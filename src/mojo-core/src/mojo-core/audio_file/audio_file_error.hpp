#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

enum class AudioFileError
{
	SUCCESS = 0,
	UNREGOGNISED_FORMAT,
	SYSTEM,
	MALFORMED_FILE,
	UNSUPPORTED_ENCODING
};

class MOJO_API AudioFileErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override;
	std::string message( int error_value ) const noexcept override;
};

MOJO_API const std::error_category&
audio_file_error_category() noexcept;

MOJO_API std::error_condition make_error_condition( AudioFileError ) noexcept;

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_condition_enum<mojo::AudioFileError> : true_type
{
};
} // namespace std