#include "mojo_test_includes.hpp"

TEST( WindowsMmcss, i_can_set_thread_characteristics_and_priority )
{
	Mmcss::WINDOWS_HANDLE task_handle = NULL;

	auto result = Mmcss::set_thread_characteristics( "Pro Audio", &task_handle );

	ASSERT_TRUE( result );
	EXPECT_TRUE( task_handle );

	result =
	    Mmcss::set_thread_priority( task_handle, Mmcss::AvrtPriority::NORMAL );

	ASSERT_TRUE( result );

	// probably not necessary
	std::this_thread::sleep_for( std::chrono::microseconds( 50 ) );

	// TODO check that it has taken effect

	result = Mmcss::revert_thread_characteristics( task_handle );

	ASSERT_TRUE( result );
}
