#include "mojo_test_includes.hpp"

namespace
{

class CLocaleGuard
{
public:
	// RAII class that sets the global C locale and then resets it to its
	// previous setting when going out of scope
	CLocaleGuard( const std::string& locale )
	{
		auto const original_locale_cstr = setlocale( LC_ALL, NULL );

		assert( original_locale_cstr != NULL );
		m_original_locale = original_locale_cstr;

		const char* new_locale = setlocale( LC_ALL, locale.c_str() );

		assert( new_locale != NULL );
	}

	~CLocaleGuard()
	{
		auto const old_locale = setlocale( LC_ALL, m_original_locale.c_str() );
		assert( old_locale != NULL );
	}

private:
	std::string m_original_locale;
};

static bool
check_decimal_mark_is_comma()
{
	char buf[32];
	double const dnum = 12345.67890;
	snprintf( buf, sizeof( buf ), "%.12g", dnum );
	bool found = ( strchr( buf, ',' ) != NULL );
	return found;
}

static std::vector<std::string>
get_locale_list()
{
	std::vector<std::string> locales;

	locales.push_back( "" ); // User preferred locale

	// Recent versions of Windows seem to also support the second group
	// of locale names, so just try them all.

	locales.push_back( "French_France.1252" ); // must be first
	locales.push_back( "Dutch_Netherlands.1252" );
	locales.push_back( "Italian_Italy.1252" );
	locales.push_back( "Farsi_Iran.1256" );
	locales.push_back( "Chinese_China.936" );
	locales.push_back( "Czech_Czech Republic.1250" );

	locales.push_back( "fr_FR" ); // French France
	locales.push_back( "fr_FR.UTF-8" );
	locales.push_back( "de_DE" ); // German Germany
	locales.push_back( "de_DE.UTF-8" );
	locales.push_back( "nl_NL" ); // Dutch - Netherlands
	locales.push_back( "nl_NL.UTF-8" );
	locales.push_back( "it_IT" ); // Italian
	locales.push_back( "fa_IR" ); // Farsi Iran
	locales.push_back( "zh_CN" ); // Chinese
	locales.push_back( "cs_CZ" ); // Czech

	return locales;
}

static std::vector<std::string>
get_supported_locales()
{
	std::string original_locale;
	{
		const char* original_locale_cstr = setlocale( LC_ALL, NULL );
		assert( original_locale_cstr != NULL );
		original_locale = original_locale;

		A_LOG_MSG( LOG::Test, A_FMT( "Original locale: {}", original_locale ) );
	}

	std::vector<std::string> locales = get_locale_list();
	std::vector<std::string> supported_locales;

	for( std::vector<std::string>::const_iterator it = locales.begin();
	     it != locales.end();
	     ++it )
	{

		const char* locale = it->c_str();
		const char* new_locale = setlocale( LC_ALL, locale );

		if( new_locale == NULL )
		{
			A_LOG_MSG(
			    LOG::Test,
			    A_FMT( "Unable to set locale to {}, may not be installed.", locale ) );
			continue;
		}

		if( *it != new_locale )
		{
			// Setting the locale may be successful but locale has a different
			// (or longer) name.
			if( it->empty() )
			{
				A_LOG_MSG( LOG::Test, A_FMT( "User preferred locale is {}", new_locale ) );
			}
			else
			{
				A_LOG_MSG( LOG::Test,
				           A_FMT( "Locale {} has name {}", locale, new_locale ) );
			}
		}

		A_LOG_MSG( LOG::Test,
		           A_FMT( "Adding locale {} to test locales", new_locale ) );

		std::cout << "Adding locale : " << new_locale << " to test locales"
		          << std::endl;

		supported_locales.push_back( *it );
	}

	if( setlocale( LC_ALL, original_locale.c_str() ) == NULL )
	{
		A_LOG_MSG(
		    LOG::Test,
		    A_FMT( "ERROR: Unable to restore original locale {}, further tests "
		           "may be invalid.",
		           original_locale ) );
		std::exit( EXIT_FAILURE );
	}

	return supported_locales;
}

static std::vector<std::string> const&
get_test_locales()
{
	static std::vector<std::string> locales = get_supported_locales();
	if( locales.empty() )
	{
		std::exit( EXIT_FAILURE );
	}
	return locales;
}

static std::string
get_locale_with_comma_decimal_mark()
{
	for( auto const& locale : get_test_locales() )
	{
		CLocaleGuard guard( locale );
		if( check_decimal_mark_is_comma() )
		{
			return locale;
		}
	}
	return {};
}

} // namespace

TEST( StringConversion, appropriate_locales_are_installed_for_testing )
{
	std::string locale_str = get_locale_with_comma_decimal_mark();
	ASSERT_FALSE( locale_str.empty() );
}

typedef void ( *TestFunctionType )( void );

void
for_each_test_locale( TestFunctionType test_func )
{
	for( auto const& locale_str : get_test_locales() )
	{
		CLocaleGuard guard( locale_str );
		test_func();
	}
}

static const std::string BOOL_TRUE_STR( "1" );
static const std::string BOOL_FALSE_STR( "0" );

TEST( StringConversion, bool_conversion )
{
	std::string str;

	// check the normal case for true/false
	EXPECT_TRUE( bool_to_string( true, str ) );
	EXPECT_EQ( BOOL_TRUE_STR, str );

	bool val = false;
	EXPECT_TRUE( string_to_bool( str, val ) );
	EXPECT_EQ( true, val );

	EXPECT_TRUE( bool_to_string( false, str ) );
	EXPECT_EQ( BOOL_FALSE_STR, str );

	val = true;
	EXPECT_TRUE( string_to_bool( str, val ) );
	EXPECT_EQ( false, val );

	// now check the other accepted values for true and false
	// when converting from a string to bool

	val = false;
	EXPECT_TRUE( string_to_bool( "1", val ) );
	EXPECT_EQ( true, val );

	val = true;
	EXPECT_TRUE( string_to_bool( "0", val ) );
	EXPECT_EQ( false, val );

	val = false;
	EXPECT_TRUE( string_to_bool( "Y", val ) );
	EXPECT_EQ( true, val );

	val = true;
	EXPECT_TRUE( string_to_bool( "N", val ) );
	EXPECT_EQ( false, val );

	val = false;
	EXPECT_TRUE( string_to_bool( "y", val ) );
	EXPECT_EQ( val, true );

	val = true;
	EXPECT_TRUE( string_to_bool( "n", val ) );
	EXPECT_EQ( val, false );

	// test some junk
	EXPECT_TRUE( !string_to_bool( "01234someYNtrueyesno junk0123", val ) );

	// test the string_to/to_string templates
	EXPECT_EQ( true, mojo::string_to<bool>( mojo::to_string( true ) ) );

	EXPECT_EQ( false, mojo::string_to<bool>( mojo::to_string( false ) ) );
}

template <class NumericType>
struct NumericStrings
{
	static const std::string max() { return "INVALID"; }
	static const std::string min() { return "INVALID"; }
};

template <class NumericType>
void
test_int_type_conversion()
{
	NumericType max = std::numeric_limits<NumericType>::max();
	EXPECT_EQ( max, mojo::string_to<NumericType>( mojo::to_string( max ) ) );

	NumericType min = std::numeric_limits<NumericType>::min();
	EXPECT_EQ( min, mojo::string_to<NumericType>( mojo::to_string( min ) ) );
}

template <>
struct NumericStrings<int16_t>
{
	static const std::string max() { return "32767"; }
	static const std::string min() { return "-32768"; }
};

TEST( StringConversion, int16_conversion_for_test_locales )
{
	for_each_test_locale( &test_int_type_conversion<int16_t> );
}

template <>
struct NumericStrings<uint16_t>
{
	static const std::string max() { return "65535"; }
	static const std::string min() { return "0"; }
};

TEST( StringConversion, uint16_conversion_for_test_locales )
{
	for_each_test_locale( &test_int_type_conversion<uint16_t> );
}

template <>
struct NumericStrings<int32_t>
{
	static const std::string max() { return "2147483647"; }
	static const std::string min() { return "-2147483648"; }
};

TEST( StringConversion, int32_conversion_for_test_locales )
{
	for_each_test_locale( &test_int_type_conversion<int32_t> );
}

template <>
struct NumericStrings<uint32_t>
{
	static const std::string max() { return "4294967295"; }
	static const std::string min() { return "0"; }
};

TEST( StringConversion, uint32_conversion_for_test_locales )
{
	for_each_test_locale( &test_int_type_conversion<uint32_t> );
}

template <>
struct NumericStrings<int64_t>
{
	static const std::string max() { return "9223372036854775807"; }
	static const std::string min() { return "-9223372036854775808"; }
};

TEST( StringConversion, int64_conversion_for_test_locales )
{
	for_each_test_locale( &test_int_type_conversion<int64_t> );
}

template <>
struct NumericStrings<uint64_t>
{
	static const std::string max() { return "18446744073709551615"; }
	static const std::string min() { return "0"; }
};

TEST( StringConversion, uint64_conversion_for_test_locales )
{
	for_each_test_locale( &test_int_type_conversion<uint64_t> );
}

static std::vector<std::string>
positive_infinity_strings()
{
	static std::array<char const*, 4> const strings{
		"inf", "INF", "infinity", "INFINITY"
	};
	return std::vector<std::string>( strings.begin(), strings.end() );
}

static std::vector<std::string>
negative_infinity_strings()
{
	static std::array<char const*, 4> const strings{
		"-inf", "-INF", "-infinity", "-INFINITY"
	};
	return std::vector<std::string>( strings.begin(), strings.end() );
}

template <class FloatType>
void
test_infinity_conversion()
{
	FloatType const pos_infinity = std::numeric_limits<FloatType>::infinity();
	FloatType const neg_infinity = -std::numeric_limits<FloatType>::infinity();

	// Check float -> string
	{
		std::string str;
		EXPECT_TRUE( to_string<FloatType>( pos_infinity, str ) );
		EXPECT_EQ( positive_infinity_strings().front(), str );

		EXPECT_TRUE( to_string<FloatType>( neg_infinity, str ) );
		EXPECT_EQ( negative_infinity_strings().front(), str );
	}

	for( auto const& str : positive_infinity_strings() )
	{
		FloatType pos_infinity_value;
		EXPECT_TRUE( string_to<FloatType>( str, pos_infinity_value ) );
		EXPECT_EQ( pos_infinity, pos_infinity_value );
	}

	for( auto const& str : negative_infinity_strings() )
	{
		FloatType neg_infinity_value;
		EXPECT_TRUE( string_to<FloatType>( str, neg_infinity_value ) );
		EXPECT_EQ( neg_infinity, neg_infinity_value );
	}

	// Check round-trip equality
	EXPECT_EQ( pos_infinity,
	           string_to<FloatType>( to_string<FloatType>( pos_infinity ) ) );
	EXPECT_EQ( neg_infinity,
	           string_to<FloatType>( to_string<FloatType>( neg_infinity ) ) );
}

TEST( StringConversion, float_infinity_conversion_for_test_locales )
{
	for_each_test_locale( &test_infinity_conversion<float> );
}

TEST( StringConversion, double_infinity_conversion_for_test_locales )
{
	for_each_test_locale( &test_infinity_conversion<double> );
}

static const std::string MAX_FLOAT_WIN( "3.4028234663852886e+038" );
static const std::string MIN_FLOAT_WIN( "1.1754943508222875e-038" );
static const std::string MAX_FLOAT_STR( "3.4028234663852886e+38" );
static const std::string MIN_FLOAT_STR( "1.1754943508222875e-38" );

void
test_float_conversion()
{
	// check float to string and back again for min and max float values
	std::string str;
	EXPECT_TRUE( float_to_string( std::numeric_limits<float>::max(), str ) );

	ASSERT_TRUE( MAX_FLOAT_WIN == str || MAX_FLOAT_STR == str );

	float val = 0.0f;
	EXPECT_TRUE( string_to_float( str, val ) );
	EXPECT_FLOAT_EQ( std::numeric_limits<float>::max(), val );

	EXPECT_TRUE( float_to_string( std::numeric_limits<float>::min(), str ) );

	ASSERT_TRUE( MIN_FLOAT_WIN == str || MIN_FLOAT_STR == str );

	EXPECT_TRUE( string_to_float( str, val ) );
	EXPECT_FLOAT_EQ( std::numeric_limits<float>::min(), val );

	// test the string_to/to_string templates
	float max = std::numeric_limits<float>::max();
	EXPECT_FLOAT_EQ( max, string_to<float>( to_string<float>( max ) ) );

	float min = std::numeric_limits<float>::min();
	EXPECT_FLOAT_EQ( min, string_to<float>( to_string<float>( min ) ) );

	// check that parsing the windows float string representation with the
	// difference in the exponent part parses correctly on other platforms
	// and vice versa

	EXPECT_TRUE( string_to_float( MAX_FLOAT_STR, val ) );
	EXPECT_FLOAT_EQ( std::numeric_limits<float>::max(), val );

	EXPECT_TRUE( string_to_float( MIN_FLOAT_STR, val ) );
	EXPECT_FLOAT_EQ( std::numeric_limits<float>::min(), val );

	EXPECT_TRUE( string_to_float( MAX_FLOAT_WIN, val ) );
	EXPECT_FLOAT_EQ( std::numeric_limits<float>::max(), val );

	EXPECT_TRUE( string_to_float( MIN_FLOAT_WIN, val ) );
	EXPECT_FLOAT_EQ( std::numeric_limits<float>::min(), val );

	test_infinity_conversion<float>();
}

TEST( StringConversion, float_conversion_for_test_locales )
{
	for_each_test_locale( &test_float_conversion );
}

static const std::string MAX_DOUBLE_STR( "1.7976931348623157e+308" );
static const std::string MIN_DOUBLE_STR( "2.2250738585072014e-308" );

void
test_double_conversion()
{
	std::string str;
	EXPECT_TRUE( double_to_string( std::numeric_limits<double>::max(), str ) );
	EXPECT_EQ( MAX_DOUBLE_STR, str );

	double val = 0.0;
	EXPECT_TRUE( string_to_double( str, val ) );
	EXPECT_DOUBLE_EQ( std::numeric_limits<double>::max(), val );

	EXPECT_TRUE( double_to_string( std::numeric_limits<double>::min(), str ) );
	EXPECT_EQ( MIN_DOUBLE_STR, str );

	EXPECT_TRUE( string_to_double( str, val ) );
	EXPECT_DOUBLE_EQ( std::numeric_limits<double>::min(), val );

	// test that overflow fails
	EXPECT_FALSE( string_to_double( "1.8e+308", val ) );
	// test that underflow fails
	EXPECT_FALSE( string_to_double( "2.4e-310", val ) );

	// test the string_to/to_string templates
	double max = std::numeric_limits<double>::max();
	EXPECT_DOUBLE_EQ( max, string_to<double>( to_string<double>( max ) ) );

	double min = std::numeric_limits<double>::min();
	EXPECT_DOUBLE_EQ( min, string_to<double>( to_string<double>( min ) ) );

	test_infinity_conversion<double>();
}

TEST( StringConversion, double_conversion_for_test_locales )
{
	for_each_test_locale( &test_double_conversion );
}

namespace
{

class UInt32ConversionTest : public adt::Test
{
	bool run() override
	{
		uint32_t num = adt::TestUtils::get_random_uint32();
		return ( num == string_to<uint32_t>( to_string( num ) ) );
	}
};

class FloatConversionTest : public adt::Test
{
	bool run() override
	{
		float num = adt::TestUtils::get_random_float();
		return ( num == string_to<float>( to_string<float>( num ) ) );
	}
};

class DoubleConversionTest : public adt::Test
{
	bool run() override
	{
		double num = adt::TestUtils::get_random_double();
		return ( num == string_to<double>( to_string<double>( num ) ) );
	}
};

class DecimalMarkIsCommaTest : public adt::Test
{
	bool run() override { return check_decimal_mark_is_comma(); }
};

} // namespace

// Perform the test in the French locale as the format for decimals is
// different and a comma is used as a decimal point. Test that this has no
// impact on the string conversions which are expected to be the same as in the
// C locale.
TEST( StringConversion, thread_safety )
{
	std::string locale_str = get_locale_with_comma_decimal_mark();

	ASSERT_FALSE( locale_str.empty() );

	CLocaleGuard guard( locale_str );

	adt::ConcurrencyTest test;

	test.add_default_count<UInt32ConversionTest>();
	test.add_default_count<FloatConversionTest>();
	test.add_default_count<DoubleConversionTest>();
	test.add_default_count<DecimalMarkIsCommaTest>();

	bool test_result = test.run();

	EXPECT_TRUE( test_result );
}