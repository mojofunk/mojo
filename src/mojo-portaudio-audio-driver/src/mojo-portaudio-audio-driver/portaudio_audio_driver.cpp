MOJO_BEGIN_NAMESPACE

A_DEFINE_CLASS_MEMBERS( PortaudioAudioDriver )

PortaudioAudioDriver::PortaudioAudioDriver()
    : m_initialized( false )
{
	initialize();
}

PortaudioAudioDriver::~PortaudioAudioDriver()
{
	terminate();
}

AudioDeviceSPSet
PortaudioAudioDriver::get_devices() const
{
	AudioDeviceSPSet devices;
	discover_devices( devices );
	return devices;
}

bool
PortaudioAudioDriver::initialize()
{
	A_CLASS_CALL();

	if( m_initialized ) return true;

	PaError error = Pa_Initialize();
	if( error == paNoError )
	{
		return m_initialized = true;
	}
	else
	{
		A_CLASS_MSG(
		    A_FMT( "Unable to Initialize portaudio: {}", Pa_GetErrorText( error ) ) );
	}
	return false;
}

bool
PortaudioAudioDriver::terminate()
{
	A_CLASS_CALL();

	if( !m_initialized ) return true;

	PaError error = Pa_Terminate();
	if( error == paNoError )
	{
		return m_initialized = false;
	}
	else
	{
		A_CLASS_MSG(
		    A_FMT( "Unable to Terminate portaudio: {}", Pa_GetErrorText( error ) ) );
	}
	return false;
}

void
PortaudioAudioDriver::discover_devices( AudioDeviceSPSet& devices )
{
	int device_count = Pa_GetDeviceCount();

	if( device_count < 0 )
	{
		A_CLASS_STATIC_MSG(
		    A_FMT( "Invalid device count: {}", Pa_GetErrorText( device_count ) ) );
		return;
	}

	for( PaDeviceIndex i = 0; i < device_count; ++i )
	{
		AudioDeviceSP device( new PortaudioAudioDevice( i ) );
		devices.insert( device );
	}
}

MOJO_END_NAMESPACE
