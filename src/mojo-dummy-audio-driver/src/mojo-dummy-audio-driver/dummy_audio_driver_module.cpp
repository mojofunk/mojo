MOJO_BEGIN_NAMESPACE

DummyAudioDriverModule::DummyAudioDriverModule( DynamicLibrarySP library )
    : AudioDriverModule( library )
{
}

DummyAudioDriverModule::~DummyAudioDriverModule() {}

std::string
DummyAudioDriverModule::get_author()
{
	return "Tim Mayberry";
}

std::string
DummyAudioDriverModule::get_description()
{
	return "Dummy module";
}

std::string
DummyAudioDriverModule::get_version()
{
	return "0.0.1";
}

AudioDriverSP
DummyAudioDriverModule::create_driver() const
{
	return AudioDriverSP( new DummyAudioDriver() );
}

MOJO_CAPI void*
mojo_module_factory( DynamicLibrarySP library )
{
	return new DummyAudioDriverModule( library );
}

MOJO_END_NAMESPACE
