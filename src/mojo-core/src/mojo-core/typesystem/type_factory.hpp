#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class TypeFactory
{
public:
	virtual TypeID type_id() const = 0;

	// create always returns a pointer to a newly allocated type.
	virtual std::any create() const = 0;

	// Should placement new be supported?
	//	virtual std::any create(void* address) const = 0;

	virtual std::type_info const& type_info() const = 0;

protected:
	virtual ~TypeFactory() {}
};

MOJO_DEFINE_POINTER_ALIASES( TypeFactory );

MOJO_END_NAMESPACE
