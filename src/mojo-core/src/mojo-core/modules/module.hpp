#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

/**
 * Base class for all modules
 */
class Module
{
public: // typedefs
	typedef void* ( *factory_func_t )( DynamicLibrarySP );

public: // Constructors
	Module( DynamicLibrarySP );

	virtual ~Module();

public: // Module Interface
	virtual std::string get_author() = 0;

	virtual std::string get_description() = 0;

	virtual std::string get_version() = 0;

	// virtual std::string get_license() = 0;

public: // library accessor
	DynamicLibrarySP const& library() const { return m_library; }

protected:
	// const?
	DynamicLibrarySP m_library;

private:
	M_DISALLOW_COPY_AND_ASSIGN( Module );
};

MOJO_DEFINE_ALL_ALIASES( Module )

MOJO_END_NAMESPACE
