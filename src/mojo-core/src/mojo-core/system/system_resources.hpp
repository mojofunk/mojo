#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

/// This class is used to get/set Resource limits and get resource usage.
/// on Linux this will be via get/setrlimit and getrusage API.
class SystemResources
{
public:
	struct Limits
	{
		uint64_t current_limit;
		uint64_t max_limit;
	};

	enum class Type
	{
		CORE_SIZE,
		CPU_TIME,
		FILE_SIZE,
		LOCKS,
		DATA,
		STACK,
		RSS,
		NPROC,
		OPEN_FILES,
		MEM_LOCK,
		VMEM,
		ADDRESS_SPACE,
		MSGQUEUE,
		NICE,
		REALTIME_PRIORITY,
		REALTIME_TIME,
		SIGPENDING,
		SBSIZE,
		SWAP,
		PTS
	};

	class ErrorCategory : public std::error_category
	{
	public:
		const char* name() const noexcept override final;
		std::string message( int error_value ) const noexcept override final;
	};

	static const std::error_category& error_category() noexcept;

	static result<void> get_limit( Type type, Limits& limits );

	static result<void> set_limit( Type type, const Limits& limits );

	enum class UsageOption
	{
		SELF,
		CHILDREN,
		THREAD,
	};

	struct Usage
	{
		struct TimeValues
		{
			int64_t seconds;
			int64_t microseconds;
		};

		TimeValues user_time;
		TimeValues system_time;
		int64_t max_rss;
		int64_t ixrss; /// integral shared memory size
		int64_t idrss; /// integral unshared data size
		int64_t isrss; /// integral unshared stack size
		int64_t soft_page_faults;
		int64_t hard_page_faults;
		int64_t swap_count;
		int64_t block_input_count;
		int64_t block_output_count;
		int64_t messages_sent;
		int64_t messages_recieved;
		int64_t signals_recieved;
		int64_t voluntary_context_switches;
		int64_t involuntary_context_switches;
	};

	static result<Usage> get_usage( UsageOption );

	/// @return The amount of physical memory in bytes
	static result<int64_t> physical_memory_size();
};

enum class SystemResourcesError
{
	SUCCESS = 0,
	BAD_ADDRESS,
	NO_PERMISSION,
	INVALID_VALUE,
	NO_PROCESS,
	UNSUPPORTED,
};

MOJO_API std::error_condition
    make_error_condition( SystemResourcesError ) noexcept;

inline std::error_code
make_error_code( SystemResourcesError error )
{
	return { static_cast<int>( error ), SystemResources::error_category() };
}

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::SystemResourcesError> : true_type
{
};
} // namespace std
