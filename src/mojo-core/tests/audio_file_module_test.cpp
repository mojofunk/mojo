﻿#include "mojo_test_includes.hpp"

void
write_test_audio_file( AudioFileSP const& af, float sample_value )
{
	unsigned int const sample_count = 4096U;
	unsigned int const buffer_size = sample_count * af->channels();

	A_LOG_DATA1( LOG::Test, buffer_size );

	std::unique_ptr<float[]> buffer( new float[buffer_size] );
	int64_t const total_samples = sample_count * 10;

	ASSERT_TRUE( buffer );

	for( int i = 0; i < sample_count; ++i )
	{
		buffer[i] = sample_value;
	}

	int64_t samples_written = 0;

	while( samples_written < total_samples )
	{
		samples_written += af->write_samples( buffer.get(), sample_count );

		ASSERT_TRUE( af->seek( samples_written ) == samples_written );
	}

	ASSERT_TRUE( samples_written == total_samples );
}

void
read_test_audio_file( AudioFileSP const& af, float expected_sample_value )
{
	unsigned int const sample_count = 4096U;
	unsigned int const buffer_size = sample_count * af->channels();

	A_LOG_DATA1( LOG::Test, buffer_size );

	std::unique_ptr<float[]> buffer( new float[buffer_size] );

	ASSERT_TRUE( buffer );

	// seek to start of audio data
	ASSERT_TRUE( af->seek( 0 ) == 0 );

	int64_t samples_read = 0;
	int64_t total_samples = af->samples();

	while( samples_read < total_samples )
	{
		samples_read += af->read_samples( buffer.get(), sample_count );

		for( int i = 0; i < sample_count; ++i )
		{
			ASSERT_TRUE( buffer[i] == expected_sample_value );
		}

		ASSERT_TRUE( af->seek( samples_read ) == samples_read );
	}

	ASSERT_TRUE( samples_read == total_samples );
}

void
test_formats( AudioFileFormatSPSet const& formats )
{
	ASSERT_TRUE( !formats.empty() );

	for( auto const& format : formats )
	{
		ASSERT_TRUE( format );

		A_LOG_DATA2( LOG::Test, format->name(), format->extension() );
	}
}

TEST( AudioFileModule, TestFormats )
{
	auto modules = ModuleManager::get_modules_of_type<AudioFileModule>();

	ASSERT_TRUE( !modules.empty() );

	for( auto const& module : modules )
	{
		ASSERT_TRUE( module );

		A_LOG_DATA3( LOG::Test,
		             module->get_author(),
		             module->get_description(),
		             module->get_version() );

		test_formats( module->get_readable_formats() );

		test_formats( module->get_writable_formats() );
	}
}

void
mojo_audio_file_test_write_read( fs::path const& file_path )
{
	auto modules = ModuleManager::get_modules_of_type<AudioFileModule>();

	ASSERT_TRUE( !modules.empty() );

	for( auto const& module : modules )
	{
		ASSERT_TRUE( module );

		A_LOG_DATA3( LOG::Test,
		             module->get_author(),
		             module->get_description(),
		             module->get_version() );

		auto const writable_formats = module->get_writable_formats();

		ASSERT_TRUE( !writable_formats.empty() );

		auto const format = *writable_formats.begin();

		auto audio_file_path = file_path;
		audio_file_path += format->extension();

		A_LOG_DATA1( LOG::Test, audio_file_path.generic_u8string() );

		auto audio_file = module->make_audio_file( audio_file_path );

		ASSERT_TRUE( audio_file );

		std::error_code ec;
		if( fs::remove( audio_file_path, ec ) )
		{
			A_LOG_MSG( LOG::Test,
			           A_FMT( "Removing previous test audio file {}",
			                  audio_file_path.generic_u8string() ) );
		}

		ASSERT_TRUE( !fs::exists( audio_file_path ) );

		auto result = audio_file->open( AudioFile::OpenMode::READ );

		ASSERT_FALSE( result );

		ASSERT_TRUE( result.error() == std::errc::no_such_file_or_directory );

		ASSERT_TRUE( audio_file->create( format, 48000, 2 ) );

		write_test_audio_file( audio_file, 0.5 );

		ASSERT_TRUE( audio_file->close() );

		result = audio_file->create( format, 48000, 2 );

		ASSERT_FALSE( result );

		ASSERT_TRUE( result.error() == std::errc::file_exists );

		ASSERT_TRUE( audio_file->open( AudioFile::OpenMode::READ ) );

		ASSERT_TRUE( format->extension() == audio_file->format()->extension() );

		ASSERT_TRUE( format->name() == audio_file->format()->name() );

		read_test_audio_file( audio_file, 0.5 );
	}
}

TEST( AudioFileModule, WriteRead )
{
	mojo_audio_file_test_write_read( fs::u8path( u8"WriteRead" ) );
}

TEST( AudioFileModule, I18NPaths )
{
	mojo_audio_file_test_write_read( fs::u8path( u8"要らない" ) );
}

TEST( AudioFileModule, MaxOpenFiles )
{
	// Don't bother checking above this limit...it is enough.
	std::size_t const max_file_limit = 4097;

	auto modules = ModuleManager::get_modules_of_type<AudioFileModule>();

	ASSERT_TRUE( !modules.empty() );

	for( auto const& module : modules )
	{
		ASSERT_TRUE( module );

		A_LOG_DATA3( LOG::Test,
		             module->get_author(),
		             module->get_description(),
		             module->get_version() );

		auto const writable_formats = module->get_writable_formats();

		ASSERT_TRUE( !writable_formats.empty() );

		auto const format = *writable_formats.begin();

		fs::path tmp_dir( u8"MaxOpenFiles" );

		std::error_code ec;

		// Remove tmp directory and files in case the test failed and didn't clean up
		fs::remove_all( tmp_dir, ec );

		fs::create_directory( tmp_dir, ec );

		ASSERT_FALSE( ec );

		auto const audio_file_path_template = tmp_dir / fs::u8path( "AudioFile-" );

		int count = 1;

		std::vector<AudioFileSP> audio_files;

		audio_files.reserve( max_file_limit );

		while( true )
		{
			auto audio_file_path = audio_file_path_template;

			audio_file_path += std::to_string( count );

			audio_file_path += format->extension();

			ASSERT_TRUE( !fs::exists( audio_file_path ) );

			auto audio_file = module->make_audio_file( audio_file_path );

			ASSERT_TRUE( audio_file );

			if( !audio_file->create( format, 48000, 2 ) )
			{
				break;
			}

			write_test_audio_file( audio_file, 0.5 );

			audio_files.push_back( audio_file );

			if( count == max_file_limit )
			{
				break;
			}
			count++;
		}

		for( auto const& audio_file : audio_files )
		{
			read_test_audio_file( audio_file, 0.5 );
		}

		A_LOG_DATA1( LOG::Test, count );

		EXPECT_TRUE( count == max_file_limit );

		// close files so we can remove them successfully
		audio_files.clear();

		ASSERT_TRUE( fs::remove_all( tmp_dir, ec ) );
	}
}
