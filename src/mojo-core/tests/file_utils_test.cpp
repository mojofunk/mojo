#include "mojo_test_includes.hpp"

TEST( FileUtils, FindFile )
{
	auto const data_paths =
	    get_special_paths( SpecialPathType::CommonAppDirectories );

	auto path = find_path_recursive( data_paths, "README" );

	A_LOG_DATA1( LOG::Test, path.u8string() );

	ASSERT_TRUE( !path.empty() );
	ASSERT_TRUE( fs::is_regular_file( path ) );
}

TEST( FileUtils, FindFiles )
{
	auto const data_paths =
	    get_special_paths( SpecialPathType::CommonAppDirectories );

	auto paths = find_paths_recursive( data_paths, "README" );

	for( auto const& p : paths )
	{
		A_LOG_DATA1( LOG::Test, p.u8string() );
	}

	ASSERT_TRUE( paths.size() );

	for( auto const& path : paths )
	{
		ASSERT_TRUE( fs::is_regular_file( path ) );
	}
}

TEST( FileUtils, FindFileMatching )
{
	auto const config_paths =
	    get_special_paths( SpecialPathType::UserHomeDirectory );

	auto path = find_path_matching( config_paths, [&]( const fs::path& path ) {
		try
		{
			if( fs::file_size( path ) <= 1000 * 1000 )
			{
				return true;
			}
		}
		catch( ... )
		{
		}
		return false;
	} );

	ASSERT_TRUE( !path.empty() );
	ASSERT_TRUE( fs::is_regular_file( path ) );
}

TEST( FileUtils, FindFilesMatching )
{
	auto const home_paths =
	    get_special_paths( SpecialPathType::UserHomeDirectory );

	EXPECT_TRUE( home_paths.size() == 1 );

	auto paths = find_paths_matching( home_paths, [&]( const fs::path& path ) {
		try
		{
			if( fs::file_size( path ) <= 1000 * 1000 )
			{
				return true;
			}
		}
		catch( ... )
		{
		}
		return false;
	} );

	ASSERT_TRUE( paths.size() );

	for( auto const& path : paths )
	{
		ASSERT_TRUE( fs::is_regular_file( path ) );
	}
}

TEST( FileUtils, FindDirectory )
{
	auto const data_paths =
	    get_special_paths( SpecialPathType::UserDataDirectory );

	auto path = find_path_matching_recursive(
	    data_paths,
	    [&]( const fs::path& ) { return true; },
	    fs::file_type::directory );

	A_LOG_DATA1( LOG::Test, path.u8string() );

	ASSERT_TRUE( !path.empty() );
	ASSERT_TRUE( fs::is_directory( path ) );
}

TEST( FileUtils, FindDirectories )
{
	auto const data_paths =
	    get_special_paths( SpecialPathType::CommonAppDirectories );

	auto paths = find_paths_matching_recursive(
	    data_paths,
	    [&]( const fs::path& ) { return true; },
	    fs::file_type::directory );

	A_LOG_DATA1( LOG::Test, paths.size() );

	ASSERT_TRUE( paths.size() );

	for( auto const& path : paths )
	{
		ASSERT_TRUE( fs::is_directory( path ) );
	}
}
