#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class AudioDriver
{
public: // ctors
	virtual ~AudioDriver();

public: // interface
	virtual AudioDeviceSPSet get_devices() const = 0;

protected: // ctors
	AudioDriver();
};

MOJO_DEFINE_ALL_ALIASES( AudioDriver );

MOJO_END_NAMESPACE