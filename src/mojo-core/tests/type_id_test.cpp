#include "mojo_test_includes.hpp"

TEST( TypeID, ConstructorCharConst )
{
	TypeID id_1( "string" );
}

TEST( TypeID, ConstructorStdString )
{
	std::string id_2_str( "float" );
	TypeID id_2( id_2_str );
}

TEST( TypeID, ConstructorCopy )
{
	TypeID id_1( "int32_t" );
	TypeID id_2( id_1 );
	ASSERT_TRUE( id_1 == id_2 );
}
