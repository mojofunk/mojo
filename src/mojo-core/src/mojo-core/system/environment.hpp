#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class MOJO_API Environment
{
public:
	/**
	 * @return true if variable with name is set in the environment.
	 */
	static bool has_variable( char const* name );

	/*
	 static std::string get_variable(char const* name);

	 static bool set_variable(char const* name, std::string const& new_value);

	 static bool unset_variable(char const* name);
	*/

private:
	Environment() = delete;
};

MOJO_END_NAMESPACE