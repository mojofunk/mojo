#include "mojo-sndfile-audio-file.hpp"

#include "mojo-sndfile-audio-file/sndfile_audio_file.cpp"
#include "mojo-sndfile-audio-file/sndfile_audio_file_format.cpp"
#include "mojo-sndfile-audio-file/sndfile_audio_file_module.cpp"
#include "mojo-sndfile-audio-file/sndfile_error.cpp"