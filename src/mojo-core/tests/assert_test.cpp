#include "mojo_test_includes.hpp"

// These tests only that check that the MOJO_ASSERT macro is usable.

TEST( Assert, i_can_use_mojo_assert_macro_with_no_message )
{
	MOJO_ASSERT( true );
}

TEST( Assert, i_can_use_mojo_assert_macro_with_message )
{
	MOJO_ASSERT( true, "This is expected to be true" );
}

TEST( Assert, i_can_use_mojo_assert_macro_with_failure )
{
	EXPECT_DEBUG_DEATH( MOJO_ASSERT( false, "failure expected" ), "" );
}
