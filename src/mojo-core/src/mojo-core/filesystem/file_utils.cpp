#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( FileUtils, "mojo::FileUtils" )
}

static fs::path
find_path_matching_internal( const std::vector<fs::path>& paths,
                             std::function<bool( const fs::path& )> matcher,
                             fs::file_type type,
                             bool recursive )
{
	A_LOG_CALL2( log::FileUtils, (uint32_t)type, recursive );

	for( auto const& dir_path : paths )
	{
		if( !fs::is_directory( dir_path ) )
		{
			A_LOG_MSG( log::FileUtils,
			           A_FMT( "Path not a directory : {}", dir_path.string() ) );
			continue;
		}

		if( recursive )
		{
			for( auto const& de : fs::recursive_directory_iterator(
			         dir_path, fs::directory_options::skip_permission_denied ) )
			{
				auto const& p = de.path();
				try
				{
					if( fs::status( p ).type() == type && matcher( p ) )
					{
						return p;
					}
				}
				catch( std::filesystem::filesystem_error const& exception )
				{
					A_LOG_MSG( log::FileUtils,
					           A_FMT( "Error accessing path {} : {}",
					                  p.u8string(),
					                  exception.what() ) );
				}
			}
		}
		else
		{
			for( auto const& de : fs::directory_iterator(
			         dir_path, fs::directory_options::skip_permission_denied ) )
			{
				auto const& p = de.path();
				try
				{
					if( fs::status( p ).type() == type && matcher( p ) )
					{
						return p;
					}
				}
				catch( std::filesystem::filesystem_error const& exception )
				{
					A_LOG_MSG( log::FileUtils,
					           A_FMT( "Error accessing path {} : {}",
					                  p.u8string(),
					                  exception.what() ) );
				}
			}
		}
	}
	return fs::path();
}

static std::vector<fs::path>
find_paths_matching_internal( const std::vector<fs::path>& paths,
                              std::function<bool( const fs::path& )> matcher,
                              fs::file_type type,
                              bool recursive )
{
	A_LOG_CALL2( log::FileUtils, (uint32_t)type, recursive );

	std::vector<fs::path> found_paths;

	for( auto const& dir_path : paths )
	{
		if( !fs::is_directory( dir_path ) )
		{
			A_LOG_MSG( log::FileUtils,
			           A_FMT( "Path not a directory : {}", dir_path.string() ) );
			continue;
		}

		if( recursive )
		{
			for( auto const& de : fs::recursive_directory_iterator(
			         dir_path, fs::directory_options::skip_permission_denied ) )
			{
				auto const& p = de.path();
				try
				{
					if( fs::status( p ).type() == type && matcher( p ) )
					{
						found_paths.emplace_back( p );
					}
				}
				catch( std::filesystem::filesystem_error const& exception )
				{
					A_LOG_MSG( log::FileUtils,
					           A_FMT( "Error accessing path {} : {}",
					                  p.u8string(),
					                  exception.what() ) );
				}
			}
		}
		else
		{
			for( auto const& de : fs::directory_iterator(
			         dir_path, fs::directory_options::skip_permission_denied ) )
			{
				auto const& p = de.path();
				try
				{
					if( fs::status( p ).type() == type && matcher( p ) )
					{
						found_paths.emplace_back( p );
					}
				}
				catch( std::filesystem::filesystem_error const& exception )
				{
					A_LOG_MSG( log::FileUtils,
					           A_FMT( "Error accessing path {} : {}",
					                  p.u8string(),
					                  exception.what() ) );
				}
			}
		}
	}
	A_LOG_DATA1( log::FileUtils, found_paths.size() );
	return found_paths;
}

static fs::path
find_path_internal( const std::vector<fs::path>& paths,
                    const std::string& filename,
                    fs::file_type type,
                    bool recursive )
{
	A_LOG_CALL3( log::FileUtils, filename, (uint32_t)type, recursive );

	return find_path_matching_internal(
	    paths,
	    [&]( const fs::path& path ) {
		    if( path.filename().string() == filename )
		    {
			    return true;
		    }
		    return false;
	    },
	    type,
	    recursive );
}

static std::vector<fs::path>
find_paths_internal( const std::vector<fs::path>& paths,
                     const std::string& filename,
                     fs::file_type type,
                     bool recursive )
{
	A_LOG_CALL3( log::FileUtils, filename, (uint32_t)type, recursive );

	return find_paths_matching_internal(
	    paths,
	    [&]( const fs::path& path ) {
		    if( path.filename().string() == filename )
		    {
			    return true;
		    }
		    return false;
	    },
	    type,
	    recursive );
}

fs::path
find_path( std::vector<fs::path> const& paths,
           std::string const& filename,
           fs::file_type const type )
{
	return find_path_internal( paths, filename, type, false );
}

fs::path
find_path_recursive( std::vector<fs::path> const& paths,
                     std::string const& filename,
                     fs::file_type const type )
{
	return find_path_internal( paths, filename, type, true );
}

std::vector<fs::path>
find_paths( std::vector<fs::path> const& paths,
            std::string const& filename,
            fs::file_type const type )
{
	return find_paths_internal( paths, filename, type, false );
}

std::vector<fs::path>
find_paths_recursive( std::vector<fs::path> const& paths,
                      std::string const& filename,
                      fs::file_type const type )
{
	return find_paths_internal( paths, filename, type, true );
}

fs::path
find_path_matching( std::vector<fs::path> const& paths,
                    std::function<bool( fs::path const& )> matcher,
                    fs::file_type const type )
{
	return find_path_matching_internal( paths, matcher, type, false );
}

fs::path
find_path_matching_recursive( std::vector<fs::path> const& paths,
                              std::function<bool( fs::path const& )> matcher,
                              fs::file_type const type )
{
	return find_path_matching_internal( paths, matcher, type, true );
}

std::vector<fs::path>
find_paths_matching( std::vector<fs::path> const& paths,
                     std::function<bool( fs::path const& )> matcher,
                     fs::file_type const type )
{
	return find_paths_matching_internal( paths, matcher, type, false );
}

std::vector<fs::path>
find_paths_matching_recursive( std::vector<fs::path> const& paths,
                               std::function<bool( fs::path const& )> matcher,
                               fs::file_type const type )
{
	return find_paths_matching_internal( paths, matcher, type, true );
}

MOJO_END_NAMESPACE
