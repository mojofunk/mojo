MOJO_BEGIN_NAMESPACE

A_DEFINE_CLASS_MEMBERS( DummyAudioDevice )

DummyAudioDevice::DummyAudioDevice() {}

DummyAudioDevice::~DummyAudioDevice() {}

std::string
DummyAudioDevice::get_name() const
{
	return "Dummy Device";
}

result<void>
DummyAudioDevice::open( uint32_t, uint32_t, double, uint32_t, Callback*, void* )
{
	mIsOpen = true;
	return success();
}

result<bool>
DummyAudioDevice::is_open() const
{
	return mIsOpen;
}

result<void>
DummyAudioDevice::start()
{
	mIsActive = true;
	return success();
}

result<bool>
DummyAudioDevice::is_active() const
{
	return mIsActive;
}

result<void>
DummyAudioDevice::stop()
{
	mIsActive = false;
	return success();
}

result<bool>
DummyAudioDevice::is_stopped() const
{
	return true;
}

result<void>
DummyAudioDevice::close()
{
	mIsOpen = false;
	return success();
}

result<void>
DummyAudioDevice::abort()
{
	mIsOpen = false;
	mIsActive = false;
	return success();
}

uint32_t
DummyAudioDevice::max_input_channels() const
{
	return 2;
}

uint32_t
DummyAudioDevice::max_output_channels() const
{
	return 2;
}

double
DummyAudioDevice::get_default_sample_rate() const
{
	return 0.0;
}

std::vector<double>
DummyAudioDevice::get_supported_sample_rates() const
{
	std::vector<double> supported_rates;
	return supported_rates;
}

double
DummyAudioDevice::get_input_latency() const
{
	return 0.0;
}

double
DummyAudioDevice::get_output_latency() const
{
	return 0.0;
}

double
DummyAudioDevice::get_current_sample_rate() const
{
	return 0.0;
}

double
DummyAudioDevice::get_cpu_load() const
{
	return 0.0;
}

MOJO_END_NAMESPACE