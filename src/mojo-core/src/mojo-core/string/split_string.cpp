#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

std::vector<std::string>
split_string( const std::string& s, char delim )
{
	std::stringstream ss( s );
	std::string item;
	std::vector<std::string> elems;
	while( std::getline( ss, item, delim ) )
	{
		elems.push_back( std::move( item ) );
	}
	return elems;
}

MOJO_END_NAMESPACE