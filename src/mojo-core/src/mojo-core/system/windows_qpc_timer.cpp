#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( QpcTimer, "mojo::QpcTimer" )
}

class QpcTimerPrivate
{
public:
	static double timer_rate_us;

	static bool is_monotonic()
	{
		int64_t last_timer_val = QpcTimer::get_microseconds();
		if( last_timer_val < 0 ) return false;

		for( int i = 0; i < 100000; ++i )
		{
			int64_t timer_val = QpcTimer::get_microseconds();
			if( timer_val < 0 ) return false;
			// try and test for non-syncronized TSC(AMD K8/etc)
			if( timer_val < last_timer_val ) return false;
			last_timer_val = timer_val;
		}
		return true;
	}
};

double QpcTimerPrivate::timer_rate_us = 0.0;

result<bool>
QpcTimer::is_monotonic()
{
	A_LOG_CALL( log::QpcTimer );

	if( !QpcTimerPrivate::timer_rate_us )
	{
		return failure( QpcTimerError::NOT_INITIALIZED );
	}

	return QpcTimerPrivate::is_monotonic();
}

result<void>
QpcTimer::initialize()
{
	A_LOG_CALL( log::QpcTimer );

	LARGE_INTEGER freq;
	if( !QueryPerformanceFrequency( &freq ) || freq.QuadPart < 1 )
	{
		// This may only fail on systems < Windows XP
		return failure( QpcTimerError::UNKNOWN );
	}

	QpcTimerPrivate::timer_rate_us = 1000000.0 / freq.QuadPart;

	A_LOG_DATA1( log::QpcTimer, QpcTimerPrivate::timer_rate_us );

	return success();
}

int64_t
QpcTimer::get_microseconds()
{
	LARGE_INTEGER current_val;

	// MS docs say this will always succeed for systems >= XP but it may
	// not return a monotonic value with non-invariant TSC's etc
	if( QueryPerformanceCounter( &current_val ) == 0 )
	{
		failure( QpcTimerError::UNKNOWN );
	}

	return ( int64_t )( current_val.QuadPart * QpcTimerPrivate::timer_rate_us );
}

const char*
QpcTimerErrorCategory::name() const noexcept
{
	return "QpcTimerError";
}

std::string
QpcTimerErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<QpcTimerError>( error_value ) )
	{
	case QpcTimerError::SUCCESS:
		return "QpcTimer: Operation Successful";
	case QpcTimerError::NOT_INITIALIZED:
		return "QpcTimer: Not Initialized";
	default:
		return "QpcTimer: Unknown error";
	}
}

const std::error_category&
qpc_timer_error_category() noexcept
{
	static QpcTimerErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( QpcTimerError error ) noexcept
{
	return std::error_condition( static_cast<int>( error ),
	                             qpc_timer_error_category() );
}

MOJO_END_NAMESPACE