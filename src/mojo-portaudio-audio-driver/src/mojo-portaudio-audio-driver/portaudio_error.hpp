MOJO_BEGIN_NAMESPACE

// I don't think it is necessary to enumerate all errors as some of them are API
// specific and can be merged into a more generic error_code and matched with a
// catchall condition.
enum class PortaudioError
{
	DEVICE_UNVAILABLE = paDeviceUnavailable,
	BAD_IO_DEVICE_COMBINATION = paBadIODeviceCombination,
	INVALID_CHANNEL_COUNT = paInvalidChannelCount,
	INVALID_SAMPLE_RATE = paInvalidSampleRate,
	BUFFER_TOO_BIG = paBufferTooBig,
	BUFFER_TOO_SMALL = paBufferTooSmall,
	BUFFER_OUTPUT_UNDERFLOWED = paOutputUnderflowed,
	BUFFER_INPUT_OVERFLOWED = paInputOverflowed
};

class PortaudioErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override;
	std::string message( int portaudio_error_value ) const noexcept override;

	bool
	equivalent( int portaudio_error_value,
	            const std::error_condition& condition ) const noexcept override;
};

// export needed??
const std::error_category&
portaudio_error_category() noexcept;

std::error_code
make_error_code( PortaudioError portaudio_error ) noexcept;

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::PortaudioError> : true_type
{
};
} // namespace std