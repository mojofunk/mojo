#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

fs::path
juce_string_to_path( juce::String const& str )
{
	return fs::u8path( str.toRawUTF8() );
}

fs::path
juce_get_special_path( juce::File::SpecialLocationType const type )
{
	auto const juce_file = juce::File::getSpecialLocation( type );
	return juce_string_to_path( juce_file.getFullPathName() );
}

fs::path
get_special_path( SpecialPathType const special_path_type )
{
	auto const paths = get_special_paths( special_path_type );

	if( !paths.empty() )
	{
		return paths.front();
	}

	return {};
}

std::vector<fs::path>
get_special_paths( SpecialPathType const special_path_type )
{
	switch( special_path_type )
	{
	case UserHomeDirectory:
		return { juce_get_special_path( juce::File::userHomeDirectory ) };
	case UserTempDirectory:
		// TODO This should be XDG_CACHE_HOME on linux
		return { juce_get_special_path( juce::File::userApplicationDataDirectory ) };
	case UserDataDirectory:
		// This isn't per XDG spec on linux with JUCE 5.3.2
		return { juce_get_special_path( juce::File::userApplicationDataDirectory ) };
	case UserConfigDirectory:
		// This isn't implemented for linux JUCE 5.3.2
		return { juce_get_special_path( juce::File::userApplicationDataDirectory ) };
	case UserDesktopDirectory:
		return { juce_get_special_path( juce::File::userDesktopDirectory ) };
	case UserDocumentsDirectory:
		return { juce_get_special_path( juce::File::userDocumentsDirectory ) };
	case UserMusicDirectory:
		return { juce_get_special_path( juce::File::userMusicDirectory ) };
	case UserMoviesDirectory:
		return { juce_get_special_path( juce::File::userMoviesDirectory ) };
	case UserPicturesDirectory:
		return { juce_get_special_path( juce::File::userPicturesDirectory ) };
	case CommonTempDirectory:
		return { juce_get_special_path( juce::File::tempDirectory ) };
	case CommonDataDirectories:
		return { juce_get_special_path(
			   juce::File::commonApplicationDataDirectory ) };
	case CommonConfigDirectories:
		return { juce_get_special_path(
			   juce::File::commonApplicationDataDirectory ) };
	case CommonAppDirectories:
		return { juce_get_special_path( juce::File::globalApplicationsDirectory ) };
	case ModuleFile:
		return { juce_get_special_path( juce::File::currentApplicationFile ) };
	case ExecutableFile:
		return { juce_get_special_path( juce::File::hostApplicationPath ) };
	}
	return {};
}

MOJO_END_NAMESPACE