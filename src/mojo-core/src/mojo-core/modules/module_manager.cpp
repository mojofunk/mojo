#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( ModuleManager, "mojo::ModuleManager" )
}

class ModuleManagerPrivate
{
	ModuleManagerPrivate()
	{
		set_directory_paths( get_default_directory_paths() );
	}

public:
	M_SINGLETON( ModuleManagerPrivate );

	std::vector<fs::path> get_default_directory_paths() const
	{
		std::vector<fs::path> default_paths;

		auto build_path = fs::current_path().parent_path();

		default_paths.push_back( build_path );

		// TODO install location

		return default_paths;
	}

	void set_directory_paths( std::vector<fs::path> const& directory_paths )
	{
		A_LOG_CALL( log::ModuleManager );

		{
			std::lock_guard<std::mutex> guard( m_mutex );
			m_directory_paths = directory_paths;
		}
		refresh();
	}

	std::vector<fs::path> get_directory_paths() const
	{
		std::lock_guard<std::mutex> guard( m_mutex );
		return m_directory_paths;
	}

	void refresh()
	{
		A_LOG_CALL( log::ModuleManager );

		std::lock_guard<std::mutex> guard( m_mutex );

		auto const module_paths =
		    find_paths_matching_recursive( m_directory_paths, is_module_path );

		for( auto const& module_path : module_paths )
		{
			auto it = m_path_to_module_map.find( module_path );

			if( it == m_path_to_module_map.end() )
			{
				auto module_sp = open_module( module_path );
				// still need to check the module is valid before inserting
				if( module_sp )
				{
					m_path_to_module_map[module_path] = module_sp;
					A_LOG_MSG( log::ModuleManager,
					           A_FMT( "New mojo::Module found at path : {}",
					                  module_sp->library()->get_path().u8string() ) );
				}
				else
				{
					A_LOG_MSG( log::ModuleManager,
					           A_FMT( "file at path : {} is not a valid mojo::Module",
					                  module_path.u8string() ) );
				}
			}
		}
	}

	std::vector<ModuleSP> get_modules() const
	{
		std::vector<ModuleSP> modules;

		std::lock_guard<std::mutex> guard( m_mutex );

		modules.reserve( m_path_to_module_map.size() );

		for( auto const& path_and_module : m_path_to_module_map )
		{
			modules.push_back( path_and_module.second );
		}
		return modules;
	}

private:
	ModuleSP open_module( fs::path const& module_path )
	{
		Module::factory_func_t factory = 0;

		DynamicLibrarySP dll = std::make_shared<DynamicLibrary>( module_path );

		if( !dll || !dll->open() )
		{
			return {};
		}

		factory = (Module::factory_func_t)dll->resolve( "mojo_module_factory" );

		if( factory == NULL )
		{
			return {};
		}

		mojo::Module* p = static_cast<mojo::Module*>( factory( dll ) );

		return mojo::ModuleSP( p );
	}

	static bool is_module_path( fs::path const& path )
	{
		if( DynamicLibrary::is_library( path ) &&
		    path.filename().u8string().find( "mojo" ) != std::string::npos &&
		    !fs::is_symlink( path ) )
		{
			return true;
		}
		return false;
	}

private:
	std::map<fs::path, ModuleSP> m_path_to_module_map;
	std::vector<fs::path> m_directory_paths;
	mutable std::mutex m_mutex;
};

// Singleton interface
ModuleManagerPrivate*
ModuleManagerPrivate::get_instance()
{
	return Singleton<ModuleManagerPrivate>::get();
}

std::vector<fs::path>
ModuleManager::get_default_directory_paths()
{
	return ModuleManagerPrivate::get_instance()->get_default_directory_paths();
}

void
ModuleManager::set_directory_paths( std::vector<fs::path> const& paths )
{
	ModuleManagerPrivate::get_instance()->set_directory_paths( paths );
}

std::vector<fs::path>
ModuleManager::get_directory_paths()
{
	return ModuleManagerPrivate::get_instance()->get_directory_paths();
}

void
ModuleManager::refresh()
{
	ModuleManagerPrivate::get_instance()->refresh();
}

ModuleSPVec
ModuleManager::get_modules()
{
	return ModuleManagerPrivate::get_instance()->get_modules();
}

MOJO_END_NAMESPACE
