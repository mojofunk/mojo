#include "mojo_test_includes.hpp"

TEST( PropertyID, ConstructorCharConst )
{
	PropertyID id_1( "id_1" );
}

TEST( PropertyID, ConstructorStdString )
{
	std::string id_2_str( "id_2" );
	PropertyID id_2( id_2_str );
}

TEST( PropertyID, ConstructorCopy )
{
	PropertyID id_1( "id_1" );
	PropertyID id_2( id_1 );
	ASSERT_TRUE( id_1 == id_2 );
}
