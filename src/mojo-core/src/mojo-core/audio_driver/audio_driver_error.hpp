#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

enum class AudioDriverError
{
	SUCCESS = 0,
	DEVICE_NOT_AVAILABLE,
	DEVICE_NOT_CONNECTED,
	DEVICE_IO_NOT_SUPPORTED,
	CHANNEL_COUNT_NOT_SUPPORTED,
	SAMPLE_RATE_NOT_SUPPORTED,
	BUFFER_SIZE_NOT_SUPPORTED,
	BUFFER_COUNT_NOT_SUPPORTED,
	BUFFER_UNDERRUN,
	BUFFER_OVERRUN
};

class MOJO_API AudioDriverErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override;
	std::string message( int error_value ) const noexcept override;
};

MOJO_API const std::error_category&
audio_driver_error_category() noexcept;

MOJO_API std::error_condition make_error_condition( AudioDriverError ) noexcept;

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_condition_enum<mojo::AudioDriverError> : true_type
{
};
} // namespace std