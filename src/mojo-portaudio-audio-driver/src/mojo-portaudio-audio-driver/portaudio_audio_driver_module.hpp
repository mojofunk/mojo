MOJO_BEGIN_NAMESPACE

class PortaudioAudioDriverModule : public AudioDriverModule
{
public: // constructors
	PortaudioAudioDriverModule( DynamicLibrarySP );
	~PortaudioAudioDriverModule();

public: // Module interface
	std::string get_author();

	std::string get_description();

	std::string get_version();

public: // AudioDriverModule interface
	AudioDriverSP create_driver() const;
};

MOJO_END_NAMESPACE