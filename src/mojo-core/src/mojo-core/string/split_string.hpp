#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

std::vector<std::string>
split_string( const std::string& s, char delim );

MOJO_END_NAMESPACE