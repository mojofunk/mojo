#include "mojo_test_includes.hpp"

bool
loop_until( std::function<void()> iterate_function,
            std::vector<std::function<bool()>> conditions,
            std::chrono::seconds timeout = std::chrono::seconds( 5 ) )
{
	auto const start = std::chrono::system_clock::now();

	while( true )
	{

		if( iterate_function )
		{
			iterate_function();
		}

		std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

		bool all_conditions_complete = true;
		std::for_each( conditions.begin(),
		               conditions.end(),
		               [&]( std::function<bool()> condition ) {
			               bool condition_complete = condition();
			               all_conditions_complete &= condition_complete;
		               } );

		auto const elapsed = std::chrono::system_clock::now() - start;

		if( all_conditions_complete )
		{
			return true;
		}
		if( elapsed > timeout )
		{
			break;
		}
	}
	return false;
}

TEST( ThreadPool, i_can_create_a_thread_pool_and_it_runs_a_thread_function )
{
	std::size_t const thread_count = 6;

	std::atomic<std::size_t> threads_finished{ 0 };
	std::atomic<bool> quit{ false };

	ThreadPool::ThreadFunction thread_function = [&]() {
		while( !quit )
		{
			std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );
		}
		threads_finished++;
	};

	ThreadPool pool( "ThreadPoolTestPool", thread_count, thread_function, {} );

	// don't wait for the threads to start
	pool.start();

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return pool.is_running() == true; } } ) );

	quit = true;

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return threads_finished == thread_count; } } ) );

	pool.stop();

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return pool.is_running() == false; } } ) );
}

TEST( ThreadPool,
      i_can_create_a_thread_pool_and_thread_started_callback_is_called )
{
	std::size_t const thread_count = 2;

	std::atomic<std::size_t> priority_set{ 0 };

	ThreadPool::ThreadFunction thread_function = [&]() {};

	ThreadPool::ThreadStartedCallback on_thread_started = [&]() {
		std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
		priority_set++;
	};

	ThreadPool pool(
	    "ThreadPoolTestPool", thread_count, thread_function, on_thread_started );

	pool.start();

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return priority_set == thread_count; } } ) );

	pool.stop();

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return pool.is_running() == false; } } ) );
}

TEST( ThreadPool, i_can_restart_a_thread_pool )
{
	std::size_t const thread_count = 12;

	std::atomic<std::size_t> threads_finished{ 0 };

	ThreadPool::ThreadFunction thread_function = [&]() { threads_finished++; };

	ThreadPool pool( "ThreadPoolTestPool", thread_count, thread_function, {} );

	pool.start( ThreadPool::StartOption::WAIT );

	ASSERT_TRUE( pool.is_running() );

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return threads_finished == thread_count; } } ) );

	pool.stop();

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return pool.is_running() == false; } } ) );

	pool.start( ThreadPool::StartOption::WAIT );

	ASSERT_TRUE( pool.is_running() );
	EXPECT_EQ( thread_count, pool.thread_count() );

	ASSERT_TRUE( loop_until(
	    {}, { [&]() { return threads_finished == thread_count * 2; } } ) );

	pool.stop();

	ASSERT_TRUE(
	    loop_until( {}, { [&]() { return pool.is_running() == false; } } ) );
}
