#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class MOJO_API QpcTimer
{
public:
	/**
	 * Initialize the QPC timer, must be called before QPC::get_microseconds will
	 * return a valid value.
	 *
	 * @return true if QPC timer is usable, use is_monotonic() to try to check
	 * if it is monotonic.
	 */
	static result<void> initialize();

	/**
	 * This should always return true for systems > XP as those versions of windows
	 * have there own tests to check timer validity and will select an appropriate
	 * timer source. This check is not conclusive and there are probably conditions
	 * under which this check will return true but the timer is not monotonic.
	 *
	 * @return true if QueryPerformanceCounter is usable as a timer source
	 */
	static result<bool> is_monotonic();

	/**
	 * If initialize returns true then get_microseconds will always return a
	 * positive value. If QPC is not supported(OS < XP) then -1 is returned but the
	 * MS docs say that this won't occur for systems >= XP.
	 *
	 * @return The value of the performance counter converted to microseconds.
	 */
	static int64_t get_microseconds();
};

enum class QpcTimerError
{
	SUCCESS = 0,
	NOT_INITIALIZED,
	UNKNOWN,
};

class MOJO_API QpcTimerErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override final;
	std::string message( int error_value ) const noexcept override final;
};

MOJO_API const std::error_category&
qpc_timer_error_category() noexcept;

MOJO_API std::error_condition make_error_condition( QpcTimerError ) noexcept;

inline std::error_code
make_error_code( QpcTimerError err )
{
	return { static_cast<int>( err ), qpc_timer_error_category() };
}

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::QpcTimerError> : true_type
{
};
} // namespace std