set(MOJO_CORE_JUCE_TARGET mojo-core-juce-${MOJO_VERSION_MAJOR})

add_library(${MOJO_CORE_JUCE_TARGET} INTERFACE)

target_include_directories(${MOJO_CORE_JUCE_TARGET}
                           INTERFACE ${JUCE_ZLIB_INCLUDE_PATH})

target_compile_definitions(
  ${MOJO_CORE_JUCE_TARGET}
  INTERFACE JUCE_STANDALONE_APPLICATION=1
            JUCE_INCLUDE_ZLIB_CODE=${JUCE_INCLUDE_ZLIB_CODE}
            JUCE_USE_CURL=${JUCE_USE_CURL})

target_link_libraries(${MOJO_CORE_JUCE_TARGET} INTERFACE juce::juce_core
                                                         ${ZLIB_LIBRARIES})
