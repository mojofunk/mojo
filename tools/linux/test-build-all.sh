#! /bin/bash

# TODO get a list of configs and iterate through them with a build
# directory for each config

: ${TEST_INSTALL:=true}

./configure.sh && ./build.sh && $TEST_INSTALL || exit
./configure-debug-static.sh && ./build.sh && $TEST_INSTALL || exit
./configure-debug-static-tests.sh && ./build.sh && $TEST_INSTALL || exit
./configure-debug-static-examples.sh && ./build.sh && $TEST_INSTALL || exit
