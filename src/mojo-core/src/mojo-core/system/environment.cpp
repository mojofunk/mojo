#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

namespace log
{
A_DEFINE_LOG_CATEGORY( Environment, "mojo::Environment" )
}

bool
Environment::has_variable( char const* variable_name )
{
	A_LOG_CALL1( log::Environment, variable_name );

#if MOJO_WINDOWS
	size_t requiredSize = 0;
	(void)getenv_s( &requiredSize, 0, 0, variable_name );
	return requiredSize != 0;
#else
	return ::getenv( variable_name ) != nullptr;
#endif
}

MOJO_END_NAMESPACE
