MOJO_BEGIN_NAMESPACE

A_DEFINE_CLASS_MEMBERS( DummyAudioDriver )

DummyAudioDriver::DummyAudioDriver() {}

DummyAudioDriver::~DummyAudioDriver() {}

AudioDeviceSPSet
DummyAudioDriver::get_devices() const
{
	AudioDeviceSPSet devices;

	AudioDeviceSP device( new DummyAudioDevice() );

	devices.insert( device );
	return devices;
}

MOJO_END_NAMESPACE