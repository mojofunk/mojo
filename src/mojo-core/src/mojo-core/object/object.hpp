#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

class Object : std::enable_shared_from_this<Object>
{
public:
protected:
	virtual ~Object() {}
};

MOJO_DEFINE_ALL_ALIASES( Object );

MOJO_END_NAMESPACE
