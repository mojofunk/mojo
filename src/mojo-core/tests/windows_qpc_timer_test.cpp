#include "mojo_test_includes.hpp"

TEST( QpcTimer, i_can_set_check_that_timer_is_monotonic )
{
	ASSERT_TRUE( QpcTimer::initialize() );

	// performs basically the same test
	auto is_monotonic_result = QpcTimer::is_monotonic();
	ASSERT_TRUE( is_monotonic_result );
	EXPECT_TRUE( is_monotonic_result.value() );

	int64_t last_timer_val = QpcTimer::get_microseconds();
	EXPECT_TRUE( last_timer_val >= 0 );

	auto min_interval = std::numeric_limits<int64_t>::max();
	auto max_interval = std::numeric_limits<int64_t>::min();

	for( int i = 0; i < 10000; ++i )
	{

		int64_t timer_val = QpcTimer::get_microseconds();

		EXPECT_TRUE( timer_val >= 0 );

		// try and test for non-syncronized TSC(AMD K8/etc)
		EXPECT_TRUE( timer_val >= last_timer_val );

		min_interval = std::min( min_interval, timer_val - last_timer_val );
		// We may get swapped out so a max interval is not so informative
		max_interval = std::max( max_interval, timer_val - last_timer_val );
		last_timer_val = timer_val;
	}

	A_LOG_DATA2( LOG::Test, min_interval, max_interval );
}
