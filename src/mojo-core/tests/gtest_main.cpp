#include "mojo_test_includes.hpp"

namespace LOG
{
A_DEFINE_LOG_CATEGORY( Test, "mojo::Test" )
}

int
main( int argc, char** argv )
{
	adt::TestUtils test_utils;

	if( Environment::has_variable( "MOJO_TEST_LOGGING" ) )
	{
		adt::Log::set_enabled();
		auto trace_event_sink = std::make_shared<adt::TraceEventSink>();
		std::string trace_file_name = std::string( argv[0] ) + ".trace";
		auto log_file = std::make_shared<adt::StdFile>( trace_file_name );
		trace_event_sink->set_output_file( log_file );
		trace_event_sink->set_enabled( true );
		adt::Log::set_sink( trace_event_sink );
		adt::Log::set_all_categories_enabled( true );
		adt::Log::set_all_types_enabled( true );
	}

	int exit_success( 1 );

	::testing::InitGoogleTest( &argc, argv );

	{
		exit_success = RUN_ALL_TESTS();
	}

	// Give the logging threads a chance to write for very short tests
	std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

	return exit_success;
}
