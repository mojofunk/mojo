#include "mojo_test_includes.hpp"

namespace
{

class DoNothingCallbackOwner : public mojo::CallbackOwner
{
public:
	void RemoveCallback( const CallbackHandle* ) override {}
};

} // namespace

TEST(
    CallbackList,
    i_can_add_callback_handles_from_two_callback_lists_with_different_types_to_a_container )
{
	CallbackList<> void_callback_list;
	CallbackList<int, std::string> int_string_callback_list;
	CallbackHandles callback_handles;

	DoNothingCallbackOwner owner;

	auto handle1 = void_callback_list.Add( owner, []() {} );
	auto handle2 =
	    int_string_callback_list.Add( owner, []( int, const std::string& ) {} );

	callback_handles.push_back( std::move( handle1 ) );
	callback_handles.push_back( std::move( handle2 ) );

	EXPECT_EQ( size_t{ 2 }, callback_handles.size() );
}

TEST(
    CallbackList,
    when_a_callback_is_added_to_a_callback_list_it_is_invoked_when_the_callback_list_is_invoked )
{
	CallbackList<int, float> callback_list;

	DoNothingCallbackOwner owner;
	int expected_int = 42;
	float expected_float = 3.142f;
	bool callback_invoked = false;

	auto handle = callback_list.Add(
	    owner, [&]( const int& integer_value, const float& float_value ) {
		    callback_invoked = true;
		    EXPECT_EQ( expected_int, integer_value );
		    EXPECT_EQ( expected_float, float_value );
	    } );

	callback_list.Invoke( expected_int, expected_float );
	EXPECT_TRUE( callback_invoked );
}

TEST(
    CallbackList,
    when_a_callback_handle_is_destroyed_the_callback_is_not_invoked_by_the_callback_list )
{
	CallbackList<> callback_list;

	DoNothingCallbackOwner owner;
	bool callback_invoked = false;

	auto handle = callback_list.Add( owner, [&]() { callback_invoked = true; } );

	callback_list.Invoke();
	EXPECT_TRUE( callback_invoked );
	callback_invoked = false;

	handle.reset();
	callback_list.Invoke();
	EXPECT_FALSE( callback_invoked );
}

TEST(
    CallbackList,
    when_a_callback_list_is_destroyed_the_callback_owner_is_notified_to_remove_callback_handle )
{
	struct TestCallbackOwner : public mojo::CallbackOwner
	{
		void RemoveCallback( const CallbackHandle* handle ) override
		{
			auto iter = std::find_if(
			    handles.begin(), handles.end(), [handle]( const auto& handle_ptr ) {
				    return handle_ptr.get() == handle;
			    } );

			if( iter != handles.end() )
			{
				handles.erase( iter );
			}
		}

		CallbackHandles handles;
	};

	TestCallbackOwner owner;

	{
		CallbackList<> callback_list;

		auto handle = callback_list.Add( owner, []() {} );
		owner.handles.push_back( std::move( handle ) );

		EXPECT_EQ( size_t{ 1 }, owner.handles.size() );
	}

	EXPECT_TRUE( owner.handles.empty() );
}

TEST(
    CallbackList,
    when_a_caller_id_used_when_invoking_a_callback_list_then_callbacks_added_with_the_same_caller_id_are_not_invoked )
{
	CallbackList<> callback_list;

	DoNothingCallbackOwner owner;
	bool callback_invoked = false;
	CallerId caller_id;

	auto handle = callback_list.Add(
	    owner, [&]() { callback_invoked = true; }, &caller_id );

	callback_list.Invoke( &caller_id );
	EXPECT_FALSE( callback_invoked );
}

TEST(
    CallbackList,
    i_can_add_a_callback_to_the_callback_list_while_the_callback_list_is_invoking_callbacks )
{
	/// @todo Not sure if this functionality is necessary
}

TEST(
    CallbackList,
    i_can_remove_a_callback_from_the_callback_list_while_the_callback_list_is_invoking_callbacks )
{
	/// @todo Not sure if this functionality is necessary
}