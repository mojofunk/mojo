#include "mojo_test_includes.hpp"

static std::vector<SpecialPathType>
get_all_special_path_types()
{
	std::vector<SpecialPathType> special_paths;

	special_paths.push_back( SpecialPathType::UserHomeDirectory );
	special_paths.push_back( SpecialPathType::UserTempDirectory );
	special_paths.push_back( SpecialPathType::UserDataDirectory );
	special_paths.push_back( SpecialPathType::UserConfigDirectory );
	special_paths.push_back( SpecialPathType::UserDesktopDirectory );
	special_paths.push_back( SpecialPathType::UserDocumentsDirectory );
	special_paths.push_back( SpecialPathType::UserMusicDirectory );
	special_paths.push_back( SpecialPathType::UserMoviesDirectory );
	special_paths.push_back( SpecialPathType::UserPicturesDirectory );
	special_paths.push_back( SpecialPathType::CommonTempDirectory );
	special_paths.push_back( SpecialPathType::CommonDataDirectories );
	special_paths.push_back( SpecialPathType::CommonConfigDirectories );
	special_paths.push_back( SpecialPathType::CommonAppDirectories );
	special_paths.push_back( SpecialPathType::ModuleFile );
	special_paths.push_back( SpecialPathType::ExecutableFile );

	return special_paths;
}

TEST( SpecialPaths, GetPaths )
{
	auto const special_paths = get_all_special_path_types();

	for( auto const& special_path : special_paths )
	{
		auto paths = get_special_paths( special_path );

		ASSERT_TRUE( paths.size() );

		for( auto const& path : paths )
		{
			A_LOG_DATA2( LOG::Test, special_path, path.u8string() );
		}
	}
}

TEST( SpecialPaths, FindPaths )
{
	auto const special_paths = get_all_special_path_types();

	for( auto const& special_path : special_paths )
	{
		auto const paths = get_special_paths( special_path );

		EXPECT_TRUE( paths.size() );

		for( auto const& path : paths )
		{

			ASSERT_TRUE( !path.empty() );
			ASSERT_TRUE( path.is_absolute() );

			if( !fs::is_directory( path ) )
			{
				continue;
			}

			auto count = 0;
			auto paths2 = paths;

			for( auto& it : fs::directory_iterator(
			         path, fs::directory_options::skip_permission_denied ) )
			{

				auto const filename = it.path().filename().string();
				fs::path fpath;

				if( fs::is_directory( it.path() ) )
				{
					fpath = find_path( paths, filename, fs::file_type::directory );

					paths2 = find_paths( paths, filename, fs::file_type::directory );
				}
				else
				{
					fpath = find_path( paths, filename );
					paths2 = find_paths( paths, filename );
				}

				A_LOG_DATA3( LOG::Test, filename, it.path().string(), fpath.string() );

				ASSERT_TRUE( !fpath.empty() );

				ASSERT_TRUE( paths2.size() );

				if( ++count == 10 )
				{
					break;
				}
			}
		}
	}
}