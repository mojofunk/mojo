#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

MOJO_API fs::path
find_path( std::vector<fs::path> const& paths,
           std::string const& filename,
           fs::file_type const type = fs::file_type::regular );

MOJO_API fs::path
find_path_recursive( std::vector<fs::path> const& paths,
                     std::string const& filename,
                     fs::file_type const type = fs::file_type::regular );

MOJO_API std::vector<fs::path>
find_paths( std::vector<fs::path> const& paths,
            std::string const& filename,
            fs::file_type const type = fs::file_type::regular );

MOJO_API std::vector<fs::path>
find_paths_recursive( std::vector<fs::path> const& paths,
                      std::string const& filename,
                      fs::file_type const type = fs::file_type::regular );

MOJO_API fs::path
find_path_matching( std::vector<fs::path> const& paths,
                    std::function<bool( fs::path const& )> matcher,
                    fs::file_type const type = fs::file_type::regular );

MOJO_API fs::path
find_path_matching_recursive(
    std::vector<fs::path> const& paths,
    std::function<bool( fs::path const& )> matcher,
    fs::file_type const type = fs::file_type::regular );

MOJO_API std::vector<fs::path>
find_paths_matching( std::vector<fs::path> const& paths,
                     std::function<bool( fs::path const& )> matcher,
                     fs::file_type const type = fs::file_type::regular );

MOJO_API std::vector<fs::path>
find_paths_matching_recursive(
    std::vector<fs::path> const& paths,
    std::function<bool( fs::path const& )> matcher,
    fs::file_type const type = fs::file_type::regular );

MOJO_END_NAMESPACE