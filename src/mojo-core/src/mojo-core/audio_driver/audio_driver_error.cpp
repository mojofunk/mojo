#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

const char*
AudioDriverErrorCategory::name() const noexcept
{
	return "AudioDriverError";
}

std::string
AudioDriverErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<AudioDriverError>( error_value ) )
	{
	case AudioDriverError::DEVICE_NOT_AVAILABLE:
		return "Device not available";
	case AudioDriverError::DEVICE_NOT_CONNECTED:
		return "Device not connected";
	case AudioDriverError::DEVICE_IO_NOT_SUPPORTED:
		return "Device I/O configuration not supported";
	case AudioDriverError::CHANNEL_COUNT_NOT_SUPPORTED:
		return "Channel count no supported";
	case AudioDriverError::SAMPLE_RATE_NOT_SUPPORTED:
		return "Sample rate not supported";
	case AudioDriverError::BUFFER_SIZE_NOT_SUPPORTED:
		return "Buffer size not supported";
	case AudioDriverError::BUFFER_COUNT_NOT_SUPPORTED:
		return "Buffer count not supported";
	case AudioDriverError::BUFFER_UNDERRUN:
		return "Buffer underrun";
	case AudioDriverError::BUFFER_OVERRUN:
		return "Buffer overrun";
	default:
		return "Unknown AudioDriver error";
	}
}

const std::error_category&
audio_driver_error_category() noexcept
{
	static AudioDriverErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( AudioDriverError ade ) noexcept
{
	return std::error_condition( static_cast<int>( ade ),
	                             audio_driver_error_category() );
}

MOJO_END_NAMESPACE