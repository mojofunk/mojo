#pragma once

#include "mojo-core.hpp"

namespace mojo
{

struct CallerId;
class CallbackHandle;

class CallbackOwner
{
public:
	virtual ~CallbackOwner() = default;

	virtual void RemoveCallback( const CallbackHandle* handle ) = 0;
};

class CallbackHandle
{
public:
	virtual ~CallbackHandle() {}
};

using CallbackHandles = std::vector<std::unique_ptr<CallbackHandle>>;

template <typename... Args>
class CallbackList final
{
	M_DISALLOW_COPY_AND_ASSIGN( CallbackList );

public:
	using CallbackType = std::function<void( Args... )>;

public:
	CallbackList() = default;

	~CallbackList() { RemoveAllCallbacks(); }

public:
	std::unique_ptr<CallbackHandle> Add( CallbackOwner& owner,
	                                     CallbackType callback,
	                                     const CallerId* caller_id = nullptr )
	{
		auto handle = std::make_unique<Handle>(
		    owner, *this, std::move( callback ), caller_id );
		mHandles.push_back( handle.get() );
		return handle;
	}

	void Invoke( Args... args, const CallerId* caller_id = nullptr )
	{
		for( auto handle : mHandles )
		{
			if( caller_id == nullptr || handle->caller_id != caller_id )
			{
				handle->callback( args... );
			}
		}
	}

	// @todo private friend of Handle?
	bool Remove( CallbackHandle* handle )
	{
		auto iter = std::find( mHandles.begin(), mHandles.end(), handle );

		if( iter == mHandles.end() )
		{
			return false;
		}

		mHandles.erase( iter );
		return true;
	}

private:
	void RemoveAllCallbacks()
	{
		while( !mHandles.empty() )
		{
			auto handle = mHandles.back();
			MOJO_ASSERT( handle != nullptr );

			const auto size = mHandles.size();
			handle->callback_owner.RemoveCallback( handle );
			const auto new_size = mHandles.size();

			MOJO_ASSERT( new_size < size );
		}
	}

private:
	struct Handle : public CallbackHandle
	{
		M_DISALLOW_COPY_AND_ASSIGN( Handle );

		Handle( CallbackOwner& callback_owner,
		        CallbackList& callback_list,
		        CallbackType callback,
		        const CallerId* caller_id )
		    : callback_owner( callback_owner )
		    , callback_list( callback_list )
		    , callback( std::move( callback ) )
		    , caller_id( caller_id )
		{
		}

		virtual ~Handle() { callback_list.Remove( this ); }

		CallbackOwner& callback_owner;
		CallbackList& callback_list;
		CallbackType callback;
		const CallerId* const caller_id;
	};

	std::vector<Handle*> mHandles;
};

} // namespace mojo