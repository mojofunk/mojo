#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

/**
 * A template parameter for the container type could be added to test
 * performance of set vs unordered_set
 */
template <class StringType>
class StringPool
{
public:
	using InternedString = const char*;

public:
	template <typename... Args>
	typename std::enable_if<std::is_constructible<std::string, Args...>::value,
	                        InternedString>::type
	emplace_string( Args&&... args )
	{
		return m_interned.emplace( std::forward<Args>( args )... ).first->c_str();
	}

private:
	std::unordered_set<StringType> m_interned;
};

MOJO_END_NAMESPACE