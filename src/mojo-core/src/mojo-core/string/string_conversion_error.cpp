#include "mojo-core/build/source_includes.hpp"

MOJO_BEGIN_NAMESPACE

const char*
StringConversionErrorCategory::name() const noexcept
{
	return "StringConversionError";
}

std::string
StringConversionErrorCategory::message( int error_value ) const noexcept
{
	switch( static_cast<StringConversionError>( error_value ) )
	{
	case StringConversionError::SUCCESS:
		return "Successful string conversion";
	case StringConversionError::EMPTY_STRING:
		return "Empty String";
	case StringConversionError::ILLEGAL_CHAR:
		return "Illegal Character";
	case StringConversionError::TOO_LONG:
		return "String too long";
	default:
		return "Unknown String Conversion error";
	}
}

const std::error_category&
string_conversion_error_category() noexcept
{
	static StringConversionErrorCategory category;
	return category;
}

std::error_condition
make_error_condition( StringConversionError error ) noexcept
{
	return std::error_condition( static_cast<int>( error ),
	                             string_conversion_error_category() );
}

MOJO_END_NAMESPACE
