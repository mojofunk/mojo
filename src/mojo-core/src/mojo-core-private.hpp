#ifndef MOJO_CORE_PRIVATE_H
#define MOJO_CORE_PRIVATE_H

#include "mojo-core.hpp"

#include "mojo-core/build/header_includes_p.hpp"

#include "mojo-core/debug/debug_print.hpp"

#include "mojo-core/system/at_exit_manager_p.hpp"

#include "mojo-core/macros/singleton_p.hpp"

#endif // MOJO_CORE_PRIVATE_H