#pragma once

#include "mojo-core.hpp"

MOJO_BEGIN_NAMESPACE

template <typename T>
const char* const
printf_format( const T& )
{
	// Force a compilation error
	return T::PRINTF_FORMAT_SPECIFIER_UNDEFINED;
}

template <>
inline const char* const
printf_format( const int16_t& )
{
	return "%" PRIi16;
}

template <>
inline const char* const
printf_format( const int32_t& )
{
	return "%" PRIi32;
}

template <>
inline const char* const
printf_format( const int64_t& )
{
	return "%" PRIi64;
}

template <>
inline const char* const
printf_format( const uint16_t& )
{
	return "%" PRIu16;
}

template <>
inline const char* const
printf_format( const uint32_t& )
{
	return "%" PRIu32;
}

template <>
inline const char* const
printf_format( const uint64_t& )
{
	return "%" PRIu64;
}

template <>
inline const char* const
printf_format( const float& )
{
	return "%.9g"; // FLT_DECIMAL_DIG
}

template <>
inline const char* const
printf_format( const double& )
{
	return "%.17g"; // DBL_DECIMAL_DIG
}

MOJO_END_NAMESPACE
