MOJO_BEGIN_NAMESPACE

enum class DummyAudioDriverError
{
	BUFFER_UNDERRUN = 1,
	BUFFER_OVERRUN
};

class DummyAudioDriverErrorCategory : public AudioDriverErrorCategory
{
public:
	const char* name() const noexcept override;
	std::string message( int dummy_error_value ) const noexcept override;

	bool
	equivalent( int dummy_error_value,
	            const std::error_condition& condition ) const noexcept override;
};

// export needed??
const std::error_category&
dummy_audio_driver_error_category() noexcept;

std::error_code
make_error_code( DummyAudioDriverError dummy_error ) noexcept;

MOJO_END_NAMESPACE

// Specialise the template in namespace std
namespace std
{
template <>
struct is_error_code_enum<mojo::DummyAudioDriverError> : true_type
{
};
} // namespace std