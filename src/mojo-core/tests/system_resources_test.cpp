#include "mojo_test_includes.hpp"

TEST( SystemResources, get_max_open_files )
{
	SystemResources::Limits limits;
	ASSERT_TRUE(
	    SystemResources::get_limit( SystemResources::Type::OPEN_FILES, limits ) );
	A_LOG_DATA2( LOG::Test, limits.current_limit, limits.max_limit );
}

TEST( SystemResources, set_max_open_files )
{
	SystemResources::Limits limits;
	ASSERT_TRUE(
	    SystemResources::get_limit( SystemResources::Type::OPEN_FILES, limits ) );

	ASSERT_NE( limits.current_limit, limits.max_limit );

	if( limits.current_limit != limits.max_limit )
	{
		limits.current_limit++;
		ASSERT_TRUE(
		    SystemResources::set_limit( SystemResources::Type::OPEN_FILES, limits ) );
	}

	SystemResources::Limits set_limits;

	ASSERT_TRUE( SystemResources::get_limit( SystemResources::Type::OPEN_FILES,
	                                         set_limits ) );
	ASSERT_EQ( limits.current_limit, set_limits.current_limit );
}

TEST( SystemResources, mem_lock_resource_limit_test )
{
	SystemResources::Limits limits;

#if MOJO_LINUX
	ASSERT_TRUE(
	    SystemResources::get_limit( SystemResources::Type::MEM_LOCK, limits ) );
#else
	ASSERT_FALSE(
	    SystemResources::get_limit( SystemResources::Type::MEM_LOCK, limits ) );
#endif

	A_LOG_DATA2( LOG::Test, limits.current_limit, limits.max_limit );
}

TEST( SystemResources, physical_memory_size )
{
	auto memory_size = SystemResources::physical_memory_size();

	ASSERT_TRUE( memory_size );
	ASSERT_NE( 0, memory_size.value() );

	A_LOG_DATA1( LOG::Test, memory_size.value() );
}
