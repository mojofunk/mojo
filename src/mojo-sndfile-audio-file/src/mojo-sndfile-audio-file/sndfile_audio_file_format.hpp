MOJO_BEGIN_NAMESPACE

class SndfileAudioFileFormat : public AudioFileFormat
{
public: // constructors
	// @param format The major and minor format
	SndfileAudioFileFormat( int sndfile_format );

public: // AudioFileFormat interface
	std::string name() const override;

	std::string extension() const override;

public: // member functions
	int format() const;

private: // member data
	int const m_format;
};

MOJO_DEFINE_ALL_ALIASES( SndfileAudioFileFormat );

MOJO_END_NAMESPACE