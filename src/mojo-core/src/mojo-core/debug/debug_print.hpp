#pragma once

#include "mojo-core.hpp"

#ifdef MOJO_ENABLE_DEBUG_PRINT
#include <cstdio>
#define MOJO_DEBUG( ... ) printf( __VA_ARGS__ )
#else
#define MOJO_DEBUG( ... )
#endif

//#define MOJO_ENABLE_INTERNAL_LOGGING
