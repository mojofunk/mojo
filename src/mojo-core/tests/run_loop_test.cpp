#include "mojo_test_includes.hpp"

class TestRunLoop : public mojo::RunLoop
{
public:
	void on_iteration() override
	{
#if 0
		int sleep_time = get_random_sleep_time_us();
		std::this_thread::sleep_for(std::chrono::microseconds(sleep_time));
		// schedule another call
#endif
		schedule_iteration();
	}
};

TEST( RunLoop, SingleThread )
{
	TestRunLoop run_loop;

	ASSERT_FALSE( run_loop.running() );

	std::thread run_loop_thread( &TestRunLoop::run, &run_loop );

	run_loop.wait_for_iteration();

	ASSERT_TRUE( run_loop.running() );

	run_loop.schedule_iteration();

	std::this_thread::sleep_for( std::chrono::seconds( 5 ) );

	ASSERT_TRUE( run_loop.running() );

	run_loop.quit();

	run_loop_thread.join();

	ASSERT_FALSE( run_loop.running() );

	// Test that we can run it again in another thread.
	run_loop_thread = std::thread( &TestRunLoop::run, &run_loop );

	run_loop.schedule_iteration();

	std::this_thread::sleep_for( std::chrono::seconds( 5 ) );

	ASSERT_TRUE( run_loop.running() );

	run_loop.quit();

	run_loop_thread.join();
}
