MOJO_BEGIN_NAMESPACE

class PortaudioAudioDriver : public AudioDriver
{
public: // constructors
	PortaudioAudioDriver();
	~PortaudioAudioDriver();

public: // AudioDriver interface
	AudioDeviceSPSet get_devices() const;

private: // methods
	bool initialize();
	bool terminate();

	static void discover_devices( AudioDeviceSPSet& devices );

private: // member data
	bool m_initialized;

private:
	A_DECLARE_CLASS_MEMBERS( PortaudioAudioDriver )
};

MOJO_END_NAMESPACE