MOJO_BEGIN_NAMESPACE

enum class SndfileError
{
	UNREGOGNISED_FORMAT = SF_ERR_UNRECOGNISED_FORMAT,
	SYSTEM = SF_ERR_SYSTEM,
	MALFORMED_FILE = SF_ERR_MALFORMED_FILE,
	UNSUPPORTED_ENCODING = SF_ERR_UNSUPPORTED_ENCODING
};

class SndfileErrorCategory : public std::error_category
{
public:
	const char* name() const noexcept override;
	std::string message( int sndfile_error_value ) const noexcept override;

	bool
	equivalent( int sndfile_error_value,
	            const std::error_condition& condition ) const noexcept override;
};

// export needed??
const std::error_category&
sndfile_error_category() noexcept;

std::error_code
make_error_code( SndfileError sndfile_error ) noexcept;

MOJO_END_NAMESPACE

namespace std
{
template <>
struct is_error_code_enum<mojo::SndfileError> : true_type
{
};
} // namespace std